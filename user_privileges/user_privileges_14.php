<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H6';

$current_user_parent_role_seq='H1::H6';

$current_user_profiles=array(6,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>1,'2'=>1,'4'=>1,'6'=>1,'8'=>1,'9'=>1,'10'=>1,'13'=>0,'14'=>1,'15'=>1,'16'=>1,'18'=>1,'19'=>1,'20'=>1,'21'=>1,'22'=>1,'23'=>1,'24'=>1,'25'=>0,'26'=>1,'32'=>1,'33'=>1,'34'=>1,'37'=>1,'38'=>1,'39'=>1,'40'=>1,'41'=>1,'42'=>1,'45'=>1,'47'=>1,'48'=>1,'49'=>1,'51'=>1,'53'=>1,'54'=>0,'55'=>0,'57'=>0,'58'=>0,'59'=>0,'60'=>0,'62'=>0,'63'=>0,'65'=>0,'66'=>0,'67'=>0,'68'=>0,'70'=>0,'71'=>0,'72'=>0,'73'=>0,'28'=>1,'3'=>0,);

$profileActionPermission=array(13=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,10=>0,),25=>array(0=>1,1=>1,2=>1,4=>1,6=>0,13=>0,),54=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),55=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),59=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),60=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),62=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),63=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),65=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),66=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),67=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),68=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),70=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),71=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),72=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),73=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),);

$current_user_groups=array(20,);

$subordinate_roles=array('H7',);

$parent_roles=array('H1',);

$subordinate_roles_users=array('H7'=>array(),);

$user_info=array('user_name'=>'mm','is_admin'=>'off','user_password'=>'$1$mm000000$p.StWn7xgrquFKAixvi3m1','confirm_password'=>'$1$mm000000$p.StWn7xgrquFKAixvi3m1','first_name'=>'','last_name'=>'test','roleid'=>'H6','email1'=>'abc@test.com','status'=>'Inactive','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'12','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'R6D5TlHR5V5Hu96b','time_zone'=>'Pacific/Midway','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>'.','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'softed','language'=>'en_us','reminder_interval'=>'','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'medium','defaulteventstatus'=>'Select an Option','defaultactivitytype'=>'Select an Option','hidecompletedevents'=>'0','is_owner'=>'0','currency_name'=>'USA, Dollars','currency_code'=>'USD','currency_symbol'=>'&#36;','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'14');
?>