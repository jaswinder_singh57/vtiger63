<?php

function processTrack($trackno) {
    
	$req['RequestOption'] = '15';
	$tref['CustomerContext'] = '';
    	$req['TransactionReference'] = $tref;
    	$request['Request'] = $req;
    	$request['InquiryNumber'] = $trackno;
        $request['TrackingOption'] = '02';

    	return $request;
}



function get_data ($remote_filename,$endpointUrl) {

        global $today;
        global $DEBUG;

        if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);

        $retValue = curl_exec($ch);
        $status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
        if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);


}


function send_alert ($remote_file,$reason) {

        global $DEBUG;
        global $crmpath;
        global $email_server;
        global $email_it;
        global $email_from;

        require_once($crmpath."modules/Emails/class.phpmailer.php");


        $msg = "Dear Team,\n";
        $msg .= "\n";
        $msg .= "File Import Alert:-\n";
        $msg .= "\nFilename $remote_file can not be processed due to following error\n";
        $msg .= "\n$reason\n";
        $msg .= "\n\nThanks\nImportMailer\n";

        $mail = new PHPMailer(true);
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $email_server;
        $mail->Username = $email_from;
        $mail->From = $email_from;
        $mail->FromName = $email_from;
        $mail->AddReplyTo($email_from);
        $mail->WordWrap = 50;
        $mail->Subject = 'ImportAlert';
        $mail->addAddress($email_it);
        $mail->Body = $msg; 

        if(!$mail->send()) {
           echo 'Message could not be sent.';
           echo 'Mailer Error: ' . $mail->ErrorInfo;
           return;
        }
        echo "Mail Sent";
        return;

}



function write_log ($log) {
        global $shiplogfile;
        $fp = fopen($shiplogfile,'a+');
        fwrite($fp,$log."\n");
        fclose($fp);
}


require_once("conn.php");
require_once("import_config.php");


$today = date('m-d-y');

$remote_filename = "SO_Script";
echo "starting";
$csql = "SELECT vtiger_crmentity.crmid,vtiger_salesordercf.$tracking_field,vtiger_salesordercf.$shipping_status_field,vtiger_salesorder.carrier,vtiger_soshipads.ship_code FROM  vtiger_crmentity INNER JOIN vtiger_salesorder ON vtiger_salesorder.salesorderid=vtiger_crmentity.crmid INNER JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_crmentity.crmid INNER JOIN vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_crmentity.crmid INNER JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_crmentity.crmid  WHERE  vtiger_crmentity.deleted='0' and vtiger_salesordercf.$shipping_status_field not in ('Delivered','Cancelled') and vtiger_salesorder.carrier in ('USPS','UPS') ";
$nresult = mysql_query($csql, $link);
while($nrow = mysql_fetch_array($nresult)) {

	$order_id = $nrow["crmid"];
	$tracking_number = $nrow["$tracking_field"];
	$shipping_status = $nrow["$shipping_status_field"];
	$carrier = $nrow["carrier"];
	$destination = $nrow["ship_code"];
	$destination = substr($destination, 0, 5);

	if ($carrier == 'UPS') {



		$mode = array ( 
			'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         		'trace' => 1
    		);
		$client = new SoapClient($UPS_wsdl , $mode);
		$client->__setLocation($UPS_endpointurl);
		$usernameToken['Username'] = $UPS_userid;
    		$usernameToken['Password'] = $UPS_passwd;
   	 	$serviceAccessLicense['AccessLicenseNumber'] = $UPS_access;
    		$upss['UsernameToken'] = $usernameToken;
    		$upss['ServiceAccessToken'] = $serviceAccessLicense;
	
		$header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    		$client->__setSoapHeaders($header);

		$resp = $client->__soapCall($UPS_operation ,array(processTrack($tracking_number)));	
		$status = $resp->Response->ResponseStatus->Description;
		$response = $client->__getLastResponse();
			
		$response = str_replace("trk:","",$response);
		$response = str_replace("common:","",$response);
		$response = str_replace("soapenv:Body","Body",$response);
		$response = str_replace("soapenv:Header","Header",$response);
		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
	
		$status = strtoupper($status);	
		if ($status == 'SUCCESS' or $status == 'TRUE'){
		
			$trackresponse = $array['Body']['TrackResponse'];
			$final_status = $trackresponse['Shipment']['Package']['Activity']['0']['Status']['Description'];
			$fstatus = strtoupper($final_status);
			$pos = strpos($fstatus, "DELIVER");
			if ($pos === false) {

                		$query = "update vtiger_salesordercf set $shipping_status_field='$final_status' where salesorderid='$order_id'";                
		                $result = mysql_query($query, $link);
		        } else {
				$deliverydate = $trackresponse['Shipment']['Package']['Activity']['0']['Date'];
		                $timestamp = strtotime($deliverydate);
                		$deliverydate = date("Y-m-d", $timestamp);

		                $final_status = 'Delivered';
				$shipping_status = $final_status;
                		$query = "update vtiger_salesordercf set $shipping_status_field='$final_status', $delivery_date_field= '$deliverydate' where salesorderid='$order_id'";  
		                $result = mysql_query($query, $link);
			}
		        if ($shipping_status == '') {

				$estimate_delivery = $trackresponse['Shipment']['DeliveryDetail']['Date'];
		                $timestamp = strtotime($estimate_delivery);
				$ship_final = date("Y-m-d", $timestamp);

                		$query = "update vtiger_salesordercf set $scheduled_delivery_date_field='$ship_final' where salesorderid='$order_id'";
                		$result = mysql_query($query, $link);
			}	
				
		} else {
	               	send_alert($remote_filename,"Unable to fetch response from UPS $tracking_number ");
			$final_status = $status;	
			$query = "update vtiger_salesordercf set $shipping_status_field='$final_status' where salesorderid='$order_id'";
                        $result = mysql_query($query, $link);

		}

	} else {	

	$url = "http://production.shippingapis.com/ShippingApi.dll?API=TrackV2&XML=".urlencode("<TrackFieldRequest USERID=\"$usps_username\"><TrackID ID=\"$tracking_number\"></TrackID></TrackFieldRequest>");

	list($status,$response) = get_data($remote_filename,$url);	
	if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to fetch status $tracking_number  ");}
                send_alert($remote_filename,"Unable to fetch status for id $tracking_number ");
		continue;
        }

	$xml = simplexml_load_string($response);
	$json = json_encode($xml);
	$array = json_decode($json,TRUE);
	$final_status = $array["TrackInfo"]["TrackSummary"]["Event"];

	$fstatus = strtoupper($final_status);
	$pos = strpos($fstatus, "DELIVER");
	if ($pos === false) {

		$query = "update vtiger_salesordercf set $shipping_status_field='$final_status' where salesorderid='$order_id'";		
		$result = mysql_query($query, $link);
	} else {
		$deliverydate = $array["TrackInfo"]["TrackSummary"]["EventDate"];
		$timestamp = strtotime($deliverydate);
                $deliverydate = date("Y-m-d", $timestamp);

		$final_status = 'Delivered';
		$query = "update vtiger_salesordercf set $shipping_status_field='$final_status', $delivery_date_field= '$deliverydate' where salesorderid='$order_id'";		
		$result = mysql_query($query, $link);
        }
	

	if ($shipping_status == '') {

		$narray = $array["TrackInfo"]["TrackDetail"];	
		foreach ($narray as $detail) {

		        if (strcasecmp($detail["Event"],"Picked Up") == 0){
        	        $origion_zip = $detail["EventZIPCode"];
			$origion_zip = substr($origion_zip, 0, 5);
		        }

        		if (strcasecmp($detail["Event"],"Pre-Shipment Info Sent to USPS") == 0){
                	$ship_date = $detail["EventDate"];
		        }
		}
		$url = "http://production.shippingapis.com/ShippingAPI.dll?API=StandardB&XML=".urlencode("<StandardBRequest USERID=\"$usps_username\"><OriginZip>$origion_zip</OriginZip><DestinationZip>$destination</DestinationZip></StandardBRequest>");	

		list($status,$response) = get_data($remote_filename,$url);      
	        if ($status != 1 ){  
        	        if ($DEBUG)  { write_log("$today $remote_filename :: unable to fetch estimate days $tracking_number  ");}
	               	send_alert($remote_filename,"Unable to fetch estimate days $tracking_number ");
	                continue;
        	}

		
	        $xml = simplexml_load_string($response);
        	$json = json_encode($xml);
	        $array = json_decode($json,TRUE);
		$days = $array["Days"];

		$timestamp = strtotime($ship_date);
		$addtime = $days * 86400;	
		$timestamp = $timestamp + $addtime;
		$ship_final = date("Y-m-d", $timestamp);

		$query = "update vtiger_salesordercf set $scheduled_delivery_date_field='$ship_final' where salesorderid='$order_id'";
		$result = mysql_query($query, $link);
		
	}


	}
}

?>
