<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'PatientNotes';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
if ($moduleInstance) {
        echo "Module already present - choose a different name.";
} else {
        $moduleInstance = new Vtiger_Module();
        $moduleInstance->name = $MODULENAME;
        $moduleInstance->parent= 'Tools';
        $moduleInstance->save();

        // Schema Setup
        $moduleInstance->initTables();

        // Field Setup
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);

        $blockcf = new Vtiger_Block();
        $blockcf->label = 'LBL_CUSTOM_INFORMATION';
        $moduleInstance->addBlock($blockcf);

	$field1 = new Vtiger_Field();
	$field1->name = 'usertaskkey';
	$field1->table = $module->basetable;
	$field1->column = 'usertaskkey';
	$field1->label = 'UserTaskKey';
	$field1->columntype = 'VARCHAR(255)';
	$field1->uitype = 2;
	$field1->typeofdata = 'V~M';
	$block->addField($field1);
	$moduleInstance->setEntityIdentifier($field1);

	
	$field2 = new Vtiger_Field();
	$field2->name = 'subject';
	$field2->column = 'subject';
	$field2->label= 'Subject';
	$field2->columntype = 'VARCHAR(255)';
	$field2->uitype = 2;
	$field2->typeofdata = 'V~M';
	$block->addField($field2);


        $field3  = new Vtiger_Field();
        $field3->name = 'customer';
        $field3->label= 'Customer';
        $field3->uitype= 10;
        $field3->column = $field3->name;
        $field3->columntype = 'VARCHAR(255)';
        $field3->typeofdata = 'V~O';
        $block->addField($field3);
	$field3->setRelatedModules(Array('Contacts'));

        $field16  = new Vtiger_Field();
        $field16->name = 'notedescription';
        $field16->label= 'Description';
        $field16->uitype= 19;
        $field16->column = $field16->name;
        $field16->columntype = 'VARCHAR(255)';
        $field16->typeofdata = 'V~M';
        $block->addField($field16);


        $field4  = new Vtiger_Field();
        $field4->name = 'notecreatedate';
        $field4->label= 'CreateDate ';
        $field4->uitype= 1;
        $field4->column = $field4->name;
        $field4->columntype = 'VARCHAR(50)';
        $field4->typeofdata = 'V~O';
        $block->addField($field4);


        $field5  = new Vtiger_Field();
        $field5->name = 'needdate';
        $field5->label= 'NeedDate';
        $field5->uitype= 1;
        $field5->column = $field5->name;
        $field5->columntype = 'VARCHAR(50)';
        $field5->typeofdata = 'V~O';
        $block->addField($field5);

        $field6  = new Vtiger_Field();
        $field6->name = 'closeddate';
        $field6->label= 'ClosedDate';
        $field6->uitype= 1;
        $field6->column = $field6->name;
        $field6->columntype = 'VARCHAR(50)';
        $field6->typeofdata = 'V~O';
        $block->addField($field6);

        $field7  = new Vtiger_Field();
        $field7->name = 'notetype';
        $field7->label= 'NoteType';
        $field7->uitype= 15;
        $field7->column = $field7->name;
        $field7->columntype = 'VARCHAR(50)';
        $field7->typeofdata = 'V~M';
        $block->addField($field7);
        $field7->setPicklistValues(Array('PatientNote','PractitionerNote','FinancialNote'));


        $field8  = new Vtiger_Field();
        $field8->name = 'notestatus';
        $field8->label= 'NoteStatus';
        $field8->uitype= 15;
        $field8->column = $field8->name;
        $field8->columntype = 'VARCHAR(50)';
        $field8->typeofdata = 'V~M';
        $block->addField($field8);
        $field8->setPicklistValues(Array('Open','Closed'));

        $field9  = new Vtiger_Field();
        $field9->name = 'noteseverity';
        $field9->label= 'NoteSeverity"';
        $field9->uitype= 1;
        $field9->column = $field9->name;
        $field9->columntype = 'VARCHAR(30)';
        $field9->typeofdata = 'V~O';
        $block->addField($field9);

        $field10  = new Vtiger_Field();
        $field10->name = 'userdefined1';
        $field10->label= 'UserDefined1';
        $field10->uitype= 1;
        $field10->column = $field10->name;
        $field10->columntype = 'VARCHAR(255)';
        $field10->typeofdata = 'V~O';
        $block->addField($field10);

        $field11 = new Vtiger_Field();
        $field11->name = 'userdefined2';
        $field11->label= 'UserDefined2';
        $field11->uitype= 1;
        $field11->column = $field11->name;
        $field11->columntype = 'VARCHAR(255)';
        $field11->typeofdata = 'V~O';
        $block->addField($field11);

        $field12 = new Vtiger_Field();
        $field12->name = 'userdefined3';
        $field12->label= 'UserDefined3';
        $field12->uitype= 1;
        $field12->column = $field12->name;
        $field12->columntype = 'VARCHAR(255)';
        $field12->typeofdata = 'V~O';
        $block->addField($field12);

        $field13 = new Vtiger_Field();
        $field13->name = 'userdefined4';
        $field13->label= 'UserDefined4';
        $field13->uitype= 1;
        $field13->column = $field13->name;
        $field13->columntype = 'VARCHAR(255)';
        $field13->typeofdata = 'V~O';
        $block->addField($field13);

        $field14 = new Vtiger_Field();
        $field14->name = 'userdefined5';
        $field14->label= 'UserDefined5';
        $field14->uitype= 1;
        $field14->column = $field14->name;
        $field14->columntype = 'VARCHAR(255)';
        $field14->typeofdata = 'V~O';
        $block->addField($field14);



        $field15  = new Vtiger_Field();
        $field15->name = 'reportdt';
        $field15->label= 'ReportDt';
        $field15->uitype= 1;
        $field15->column = $field15->name;
        $field15->columntype = 'VARCHAR(255)';
        $field15->typeofdata = 'V~O';
        $block->addField($field15);



        // Recommended common fields every Entity module should have (linked to core table)
        $mfield1 = new Vtiger_Field();
        $mfield1->name = 'assigned_user_id';
        $mfield1->label = 'Assigned To';
        $mfield1->table = 'vtiger_crmentity';
        $mfield1->column = 'smownerid';
        $mfield1->uitype = 53;
        $mfield1->typeofdata = 'V~M';
        $block->addField($mfield1);

        $mfield2 = new Vtiger_Field();
        $mfield2->name = 'CreatedTime';
        $mfield2->label= 'Created Time';
        $mfield2->table = 'vtiger_crmentity';
        $mfield2->column = 'createdtime';
        $mfield2->uitype = 70;
        $mfield2->typeofdata = 'T~O';
        $mfield2->displaytype= 2;
        $block->addField($mfield2);

        $mfield3 = new Vtiger_Field();
        $mfield3->name = 'ModifiedTime';
        $mfield3->label= 'Modified Time';
        $mfield3->table = 'vtiger_crmentity';
        $mfield3->column = 'modifiedtime';
        $mfield3->uitype = 70;
        $mfield3->typeofdata = 'T~O';
        $mfield3->displaytype= 2;
        $block->addField($mfield3);

        // Filter Setup
        $filter1 = new Vtiger_Filter();
        $filter1->name = 'All';
        $filter1->isdefault = true;
        $moduleInstance->addFilter($filter1);
        $filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($field7, 3)->addField($field4, 4)->addField($field5, 5)->addField($mfield2, 6);

        // Sharing Access Setup
        $moduleInstance->setDefaultSharing('Public');

        // Webservice Setup
        $moduleInstance->initWebservice();

        mkdir('modules/'.$MODULENAME);
        echo "OK\n";
}
