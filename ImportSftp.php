<?php

function sftp_get_file ($remote_filename,$local_filename) {


	global $fetch_sftp_domain;
	global $fetch_sftp_password;
		
	System("sshpass -p '$fetch_sftp_password' sftp -o StrictHostKeyChecking=no  -oBatchMode=no -b - $fetch_sftp_domain << !
   get $remote_filename $local_filename
   bye
!
");
	if (file_exists($local_filename)){
		return "Success";
	} else {

		return "FileDownloadFailed";
	}

}


function unzip_file ($filename,$location) {

	$zip = new ZipArchive;
	if ($zip->open("$filename") === TRUE) {
		$zip->extractTo("$location");
		$zip->close();
		return "Success";
	} else {
		return "UnableToUnzip";
	}
}

function send_alert ($remote_file,$reason) {

	global $DEBUG;
	global $crmpath;
	global $email_server;
	global $email_it;
	global $email_from;

	return;	
	require_once($crmpath."modules/Emails/class.phpmailer.php");


	$msg = "Dear Team,\n";
	$msg .= "\n";
	$msg .= "File Import Alert:-\n";
	$msg .= "\nFilename $remote_file can not be processed due to following error\n";
	$msg .= "\n$reason\n";
	$msg .= "\n\nThanks\nImportMailer\n";
	
	$mail = new PHPMailer(true);
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $email_server;
	$mail->Username = $email_from;
	$mail->From = $email_from;
	$mail->FromName = $email_from;
	$mail->AddReplyTo($email_from);
	$mail->WordWrap = 50;
	$mail->Subject = 'ImportAlert';
	$mail->addAddress($email_it);
	$mail->Body = $msg; 

	if(!$mail->send()) {
	   echo 'Message could not be sent.';
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	   return;
	}
	echo "Mail Sent";
	return;
	
}


function write_import_status($remote_filename,$total,$new,$merged,$error,$status) {

	global $link;
	global $today;	
	global $DEBUG;	

        $query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res=mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}

        $query = "select id from vtiger_crmentity_seq";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res=mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        $row = mysql_fetch_row($res);
        $crm_id = $row[0];

	$query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($crm_id,'1','1','ImportReport',NULL,'1',now(),now())";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        $query = "UPDATE vtiger_crmentity SET label='$remote_filename' WHERE crmid='$crm_id'";
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);

        $query = "insert into vtiger_importreport (importreportid,importedfile,totalrecords,mergedrecords,newrecords,errorrecords,importstatus) values($crm_id,'$remote_filename','$total','$merged','$new','$error','$status')";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}

        $query = "insert into vtiger_importreportcf (importreportid) values($crm_id)";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}

	return "Success";	


}


function process_sales_order () {

	global $today;
	global $extractpath;
	global $link;
	global $so_template;
	global $logfile;
	global $DEBUG;
	global $webapiurl;
	global $username;
	global $secretkey;

	$file_suffix = "SalesOrder";
	$remote_filename = $file_suffix."_".$today.".zip";
	$local_filename = $extractpath."/".$remote_filename;
	$file_csv = $extractpath."/".$file_suffix.".csv";
	$file_csv = '/home/gurpreet/PROJECT/VtigerCustomization/SalesOrderImport/SampleSalesOrder.csv';

/*
	if ($DEBUG)  { write_log("$today $remote_filename Getting File");} 
	$response = sftp_get_file($remote_filename,$local_filename);	
	if ($DEBUG)  { write_log("$today $remote_filename sftp $response ");} 

	if ($response != "Success"){
		send_alert($remote_filename,$response);
		return "$response";
	}
	
	$response = unzip_file($local_filename,$extractpath);
        
	
	if ($DEBUG)  { write_log("$today $remote_filename unzip $response ");} 
        if ($response != "Success"){
		send_alert($remote_filename,$response);
                return "$response";
        }
*/
	$so_template = 'Purchase Order #=purchaseorder&Invoice Amount=s_h_amount&Patient Address 1=ship_street&Patient City=ship_city&Patient State=ship_state&Patient ZipCode=ship_code&Patient ID=cf_1159&Product No=productcode';
	$mappingarr = explode('&',$so_template);
	
	if ($DEBUG)  { write_log("$today $remote_filename :: Template Found   ");} 
	$field_arr = array();
	$header_arr = array();	
	foreach ($mappingarr as $value) {
		$fields = explode('=',$value);	
		$header = $fields[0];
		$fieldname = $fields[1];
		$header_arr["$header"] = $fieldname;
	 	if ($fieldname == 's_h_amount') {
			$field_arr["hdnS_H_Amount"] = 'hdnS_H_Amount';
			$header_arr["$header"] = 'hdnS_H_Amount';
			continue;
                }

		$fquery = "select tablename ,uitype from vtiger_field where columnname='$fieldname' and tabid='22' limit 1";
		if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}     
		$fresult = mysql_query($fquery,$link);
		$qstatus = mysql_affected_rows($link);
		if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");} 
		if(!mysql_num_rows($fresult)){
			$fquery = "select tablename ,uitype from vtiger_field where columnname='$fieldname' and tabid='4' limit 1";
        	        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
                	$fresult = mysql_query($fquery,$link);
	                $qstatus = mysql_affected_rows($link);
        	        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
                	if(!mysql_num_rows($fresult)){
				$fquery = "select tablename ,uitype from vtiger_field where columnname='$fieldname' and tabid='14' limit 1";
	                        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        	                $fresult = mysql_query($fquery,$link);
                	        $qstatus = mysql_affected_rows($link);
                        	if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
				if(!mysql_num_rows($fresult)){
					if ($DEBUG)  { write_log("$today $remote_filename :: Template Error  field Not found $fquery ");} 
			                send_alert($remote_filename,"TemplateError Field $fieldname Not found $fquery");
      	 			        return "NoTemplate field Not Found";
				} else {
				        $frow = mysql_fetch_array($fresult);
			                $field_arr["$fieldname"] = $frow["uitype"];
 				}
			} else {
				$frow = mysql_fetch_array($fresult);
			        $field_arr["$fieldname"] = $frow["uitype"];
			}		
		} else {	
			$frow = mysql_fetch_array($fresult);
			$field_arr["$fieldname"] = $frow["uitype"];
		}
	}

/*	
	$fquery = "select columnname ,uitype from vtiger_field where fieldlabel='Import Date' and tabid='6' limit 1";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
		if ($DEBUG)  { write_log("$today $remote_filename :: Import Date field not found $fquery");}
                send_alert($remote_filename,"Import Date Field Not Found ");
 		return "Import Date field Not Found";
	}
        $frow = mysql_fetch_array($fresult);
        $import_field_name = $frow["columnname"];

*/
	//login to server
	$loginurl = $webapiurl."?operation=getchallenge&username=$username";
	list($status,$response) = get_data($remote_filename,$loginurl);
	if ($status != 1 ){
		if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
	}
	$loginresponse = json_decode($response,true);
	if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
	}
	$challengeToken = $loginresponse['result']['token'];

	$generatedKey = md5($challengeToken.$secretkey);
	$params = array('operation'=>'login', 'username'=>$username,'accessKey'=>$generatedKey);

        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }

	$jsonResponse = json_decode($response,true);
	$sessionid = $jsonResponse['result']['sessionName'];
	$userid = $jsonResponse['result']['userId'];


	$fp = fopen("$file_csv", "r");
	if(!$fp) {
		if ($DEBUG)  { write_log("$today $remote_filename :: File Open Fail $file_csv  ");}
		send_alert($file_csv,"FailedToOpenFile");
		return "FailedToOpenFile $file_csv";
	}
	if ($DEBUG)  { write_log("$today $remote_filename :: File Opened $file_csv  ");}
	$firstloop = 1;	
	$header_index = array();
	$total=0;
	$new = 0;
	$error = 0;
	$merged = 0;
	while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
		if($firstloop){
			$firstloop = 0;	
			foreach ($header_arr as $key => $value) {
				foreach ($data as $d => $v) {
					if ("$key" == "$v"){
						$header_index["$key"] = $d;
						if ($DEBUG)  { write_log("$today $remote_filename :: $key = $d ");}
					}
					if ($v == "Qty Shipped") {
						$header_index["Qty Shipped"] = $d;
					}
				}	
			}

			continue;
		}
		$total++;
		$err = 0;

                $order_id = $header_index["Purchase Order #"];
                $order_id = $data["$order_id"];
                $order_id = trim($order_id);
                $table_field = $header_arr["Purchase Order #"];

		

		$aquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_salesorder as b on a.crmid=b.salesorderid where b.$table_field='$order_id' and a.deleted='0'";
		if ($DEBUG)  { write_log("$today $remote_filename :: Search order  $order_id === $aquery  ");}
		$aresult = mysql_query($aquery,$link);
		$qstatus = mysql_affected_rows($link);
		if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");} 
		if ($qstatus < 0) {$err = 1;}
		$modulename = 'SalesOrder';
		if (!mysql_num_rows($aresult)){
			if ($DEBUG)  { write_log("$today $remote_filename :: Sales Order $order_id Not Found Adding New   ");}
			//insert block

			$patient_id = $header_index["Patient ID"];
			$patient_id = $data["$patient_id"];
	                $patient_id = trim($patient_id);
        	        $table_field = $header_arr["Patient ID"];

			$dquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_contactscf as b on a.crmid=b.contactid where b.$table_field='$patient_id' and a.deleted='0'";
			if ($DEBUG)  { write_log("$today $remote_filename :: Search patient id  $patient_id === $dquery  ");}			
			$dresult = mysql_query($dquery,$link);
			if (!mysql_num_rows($dresult)){
				if ($DEBUG)  { write_log("$today $remote_filename :: patient id  $patient_id Not Found  ");}	
		                send_alert($file_csv,"Order number $order_id patient id  $patient_id Not Found");
				continue;

			}

		    	$row = mysql_fetch_row($dresult);
                        $contactid = $row[0];

                        $productcode = $header_index["Product No"];
                        $productcode = $data["$productcode"];
                        $productcode = trim($productcode);
                        $table_field = $header_arr["Product No"];
				
			$dquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_products as b on a.crmid=b.productid where b.$table_field='$productcode' and a.deleted='0'";
                        if ($DEBUG)  { write_log("$today $remote_filename :: Search product code  $productcode === $dquery  ");}
                        $dresult = mysql_query($dquery,$link);
                        if (!mysql_num_rows($dresult)){
                                if ($DEBUG)  { write_log("$today $remote_filename :: Product code  $productcode Not Found  ");}
                                send_alert($file_csv,"Order number $order_id product code  $productcode  Not Found");
                                continue;
                        }

                        $row = mysql_fetch_row($dresult);
                        $product_id = $row[0];
			$quantity = $header_index["Qty Shipped"];
                        $quantity = $data["$quantity"];
                        $quantity = trim($quantity);
 
				
			$contactdata = array('subject'=>$order_id,'vtiger_purchaseorder'=>$order_id,'sostatus'=>'Created','assigned_user_id'=>$userid); 
			foreach ($header_index as $key => $value) {
				if ($key == "Patient ID" or $key == "Product No" or $key == "Purchase Order #" or $key == "Qty Shipped"){
                                        continue;
                                }

				$data_value = $data["$value"];
				$crm_field = $header_arr["$key"];
				$field_type = $field_arr["$crm_field"];
				if ($field_type == '5' or $field_type == '23') {
                                        if ($data_value == ''){
                                                $data_value = '0000-00-00';
                                        } else {
                                                $new_value = format_date($data_value);
                                                if ($new_value == '1969-12-31') {
                                                        $data_value = '0000-00-00';
                                                } else {
                                                        $data_value = $new_value;
                                                }
                                        }
				}
				
				$set_field = $crm_field."='".mysql_real_escape_string($data_value)."'";
				$contactdata["$crm_field"] = mysql_real_escape_string($data_value);
			}

			$contactdata["bill_street"] = $contactdata["ship_street"]; 
			$contactdata["bill_city"] = $contactdata["ship_city"]; 
			$contactdata["bill_state"] = $contactdata["ship_state"]; 
			$contactdata["bill_code"] = $contactdata["ship_code"]; 

			$lineitem = array('productid'=>"14x$product_id",'sequence_no'=>'1','listprice'=>'0','quantity'=>"$quantity");
			$contactdata["contact_id"] = "12x$contactid";	
			$contactdata["LineItems"] = array($lineitem);	
			$contactdata["productid"] = "14x$product_id";
			$contactdata["hdnS_H_Amount"] = str_replace('$','',$contactdata["hdnS_H_Amount"]);

			$objectjson=json_encode($contactdata);
			$params = array("sessionName"=>$sessionid, "operation"=>'create', "element"=>$objectjson, "elementType"=>$modulename);		
			list($status,$response) = post_data($remote_filename,$webapiurl,$params);
			if (!$status){
				$error++;
				continue;
			}	
				
			$jsonresponse = json_decode($response,true);	
			
			if ($jsonresponse["success"] == 1){
				$new++;

			} else {
				$error++;				
			}
				
		} else {
			//merge block
			$row = mysql_fetch_row($aresult);
                        $crm_id = $row[0];
			if ($DEBUG)  { write_log("$today $remote_filename :: Record Found CRMID $crm_id ");}

                        $productcode = $header_index["Product No"];
                        $productcode = $data["$productcode"];
                        $productcode = trim($productcode);
                        $table_field = $header_arr["Product No"];

                        $dquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_products as b on a.crmid=b.productid where b.$table_field='$productcode' and a.deleted='0'";
                        if ($DEBUG)  { write_log("$today $remote_filename :: Search product code  $productcode === $dquery  ");}
                        $dresult = mysql_query($dquery,$link);
                        if (!mysql_num_rows($dresult)){
                                if ($DEBUG)  { write_log("$today $remote_filename :: Product code  $productcode Not Found  ");}
                                send_alert($file_csv,"Order number $order_id product code  $productcode  Not Found");
                                continue;
                        }

                        $row = mysql_fetch_row($dresult);
                        $product_id = $row[0];
                        $quantity = $header_index["Qty Shipped"];
                        $quantity = $data["$quantity"];
                        $quantity = trim($quantity);

	
			$retrieveid = "6x".$crm_id;
			$url = $webapiurl."?sessionName=$sessionid&operation=retrieve&id=$retrieveid";

		        list($status,$response) = get_data($remote_filename,$url);
		        if ($status != 1 ){
		                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id  ");}
		               	$error++;
				continue;
        		}
			$ret_response = json_decode($response,true);
		        if ($ret_response["success"] != 1) {
		                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id");}
				$error++;
				continue;
        		}
			
			$retrievedobject = $ret_response['result'];
	
			$sequence = count($retrievedobject["LineItems"]);			
			$sequence++;
                        $lineitem = array('productid'=>"14x$product_id",'sequence_no'=>"$sequence",'listprice'=>'0','quantity'=>"$quantity");
			array_push ($retrievedobject["LineItems"],$lineitem);

			$retrievedobject["productid"] = "14x$product_id";	
                        $objectjson=json_encode($retrievedobject);
                        $params = array("sessionName"=>$sessionid, "operation"=>'update', "element"=>$objectjson, "elementType"=>$modulename);
                        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
                        if (!$status){
                                $error++;
                                continue;
                        }

                        $jsonresponse = json_decode($response,true);

                        if ($jsonresponse["success"] == 1){
                                $merged++;

                        } else {
                                $error++;
                        }

		}	

	}
	fclose($fp);
	if ($DEBUG)  { write_log("$today $remote_filename :: Import Total = $total , New = $new , Merged = $merged , Error = $error ");}
	$url = $webapiurl."?operation=logout&sessionName=$sessionid";		
	list($status,$response) = get_data($remote_filename,$url);
	
/*
	$status = "Imported";
	write_import_status($remote_filename,$total,$new,$merged,$error,$status);

        if ($DEBUG)  { write_log("$today $remote_filename Uploading File");}
        $response = sftp_put_file($remote_filename,$local_filename);
        if ($DEBUG)  { write_log("$today $remote_filename sftp $response ");}



        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }

	return "Success";
*/			
}



function write_log ($log) {
	global $logfile;
	$fp = fopen($logfile,'a+');
	fwrite($fp,$log."\n");
	fclose($fp);
}

function post_data ($remote_filename,$endpointUrl,$params) {

	global $today;
	global $DEBUG;
	global $username;
	global $password;

	if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
//	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	$retValue = curl_exec($ch);
	$status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
	if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);	
	

}


function get_data ($remote_filename,$endpointUrl) {

        global $today;
        global $DEBUG;
	global $username;
	global $password;

        if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
//	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");	
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $retValue = curl_exec($ch);
        $status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
        if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);


}

function format_date ($old_date) {

	$newDate = date("Y-m-d", strtotime($old_date));	
	return ($newDate);

}

function get_gmt() {
	$newDate = gmdate("Y-m-d", time());
	return ($newDate);
}

function update_required ($modified_time,$lastupdate_datetime) {

	global $DEBUG;
	global $today;
	if ($lastupdate_datetime == '' or $lastupdate_datetime == null){
		return 0;
	}
	$modified_timestamp = strtotime($modified_time);
	$lastupdate_datetime_timestamp = strtotime($lastupdate_datetime);
	if ($DEBUG)  { write_log("$today  ::  Modified $modified_time $modified_timestamp csv $lastupdate_datetime $lastupdate_datetime_timestamp ");}
	$status = 0;
	if ($modified_timestamp >= $lastupdate_datetime_timestamp) {
		$status = 1;
	}
	return $status;	

}

include('import_config.php');
include("conn.php");
ini_set('include_path', "$basepath");

error_reporting(E_ALL & ~E_WARNING);
echo "Starting\n";

$today = date('m-d-y');
$extractpath = $basepath."".$today;
if (!file_exists($extractpath)) {
	mkdir($extractpath,0777,true);
}

//SalesOrder Filie Processing
$response = process_sales_order();

echo "Finished\n";
?>
