<?php

include_once 'modules/Vtiger/CRMEntity.php';

class Insurance extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_insurance';
        var $table_index= 'insuranceid';

        var $customFieldTable = Array('vtiger_insurancecf', 'insuranceid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_insurance', 'vtiger_insurancecf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_insurance' => 'insuranceid',
                'vtiger_insurancecf'=>'insuranceid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'PayorKey' => Array('insurance', 'payorkey'),
                'PayorName' => Array('insurance', 'payorname'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'PayorKey' => 'payorkey',
                'PayorName' => 'payorname',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'payorkey';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'PayorKey' => Array('insurance', 'payorkey'),
                'PayorName' => Array('insurance', 'payorname'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'PayorKey' => 'payorkey',
                'PayorName' => 'payorname',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('payorkey','payorname');

        // For Alphabetical search
        var $def_basicsearch_col = 'payorkey';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'payorkey';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('payorkey','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
