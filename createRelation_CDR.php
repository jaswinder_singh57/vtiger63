<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;


        $cdr = Vtiger_Module::getInstance('CDR');


        $contacts = Vtiger_Module::getInstance('Contacts');
        $accounts = Vtiger_Module::getInstance('Accounts');
        $insurance = Vtiger_Module::getInstance('Insurance');
        $vendors = Vtiger_Module::getInstance('Vendors');
        $leads = Vtiger_Module::getInstance('Leads');
        $relationLabel = 'CDR';
        $contacts->setRelatedList(
                $cdr, $relationLabel, Array('ADD','SELECT'),'get_dependents_list'
        );
        $accounts->setRelatedList(
                $cdr, $relationLabel, Array('ADD','SELECT'),'get_dependents_list'
        );

        $insurance->setRelatedList(
                $cdr, $relationLabel, Array(''),'get_dependents_list'
        );

        $vendors->setRelatedList(
                $cdr, $relationLabel, Array(''),'get_dependents_list'
        );
        $leads->setRelatedList(
                $cdr, $relationLabel, Array(''),'get_dependents_list'
        );
?>
