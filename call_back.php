<html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="callback/jquery-ui.css">
  <script src="callback/jquery-1.10.2.js"></script>
  <script src="callback/jquery-ui.js"></script>
  <script src="callback/jquery.timepicker.js"></script>
  <link rel="stylesheet" href="callback/jquery.timepicker.css">


  <script>
  $(function() {
    $( "#ddpicker" ).datepicker({
    'minDate' : 'today',
    beforeShowDay: function (date) {
        var day = date.getDay();
        return [(day != 0) && (day != 6)];
    }
})
  });
 $(function() {
   $('#timepicker1').timepicker({
    'minTime': '9:00am',
    'maxTime': '4:30pm'
})
 });

  </script>
</head>

<body>
<div class="listViewEntriesDiv contents-bottomscroll" style="margin-top:50px;margin-left:50px;">
<?php

include('conn.php');

$modulename = $_REQUEST['module'];
$customer = $_REQUEST["selected_ids"];
$userid =$_REQUEST['userid'];

?>

<script>
function validateForm() {
	var callsubject = document.forms["callbackform"]["callsubject"].value;
	var calldate = document.forms["callbackform"]["ddpicker"].value;
	var calltime = document.forms["callbackform"]["timepicker1"].value;
	var callcampaign = document.forms["callbackform"]["callcampaign"].value;
	var customer = '<?php echo $customer;?>';
	if (callsubject == "") {
		alert("Please Give a call subject");
		return false;
	}
	if (callcampaign == "select") {
		alert("Please select  a campaign");
		return false;
	}
    	if (calldate == "" ) {
        	alert("Please Select CallBack Date");
	        return false;
    	}
	if (calltime == ""){
        	alert("Please Select CallBack Date");
	        return false;
	}
	var currentdate = Math.floor(Date.now() / 1000);
	var seldatetime = calldate+" "+calltime;
	seldatetime = seldatetime.replace("am", " am");
	seldatetime = seldatetime.replace("pm", " pm");
	var finaltime = Math.floor(Date.parse(seldatetime)/1000);
	if (finaltime < currentdate) {
		alert ("Please Select a Future Call Back Time");
		return false;
	}
	return true;

}


</script>

<form action=check_callbacks.php name='callbackform' align='center' method="post" enctype='multipart/form-data' onsubmit="return validateForm()">
<table cellpadding=2 cellspacing=2 aligh="center" >

<tr><td>
<label>CallBack Subject : </label>
</td>
<td>
<input type='text' id='callsubject' name='callsubject'></input>
</td>
</tr>

<tr><td>
<label>Select Campaign : </label>
</td>
<td>
<?php

$query = "select vicidialcampaign as campaign_name from vtiger_dialermapping group by vicidialcampaign";
$result=mysql_query($query,$link);
?>
<select name="callcampaign" id="callcampaign" style="height: 25px;">
<option value="select">Select</option>
<?php
while($row = mysql_fetch_array($result)) {
	print "<option value='".$row['campaign_name']."'>".$row['campaign_name']."</option>";	
}
?>
</td></tr>
<tr>
<td><label>CB To Extension : </label>
</td>
<td>

<input type='checkbox' name='agent_cb'  id='agent_cb' value="" checked>
</td>
</tr>
<tr>
<td>
<label>CallBack Date : </label>
</td>
<td>
<input type="text" id="ddpicker" name="ddpicker">
</td>
</tr>
<tr>
<td>
<label>CallBack Time : </label>
</td>
<td>
<input type="text" id="timepicker1" name="timepicker1">
<?php
print "<input type=\"hidden\" id=\"customer\" name=\"customer\" value=".$customer."></input>";
print "<input type=\"hidden\" id=\"module\" name=\"module\" value=".$modulename."></input>";
print "<input type=\"hidden\" id=\"userid\" name=\"userid\" value=".$userid."></input>";
?>
</td>
</tr>
<tr><td>
<input type="submit" name="callback" style="background-color:#909999;color:#FFFFFF"  id="callback" value="Set CallBack">
</td></tr>
</table>
</form>
</div>

</body>
</head>
