<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'CallBackManager';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
if ($moduleInstance || file_exists('modules/'.$MODULENAME)) {
        echo "Module already present - choose a different name.";
} else {
        $moduleInstance = new Vtiger_Module();
        $moduleInstance->name = $MODULENAME;
        $moduleInstance->parent= 'Tools';
        $moduleInstance->save();

        // Schema Setup
        $moduleInstance->initTables();

        // Field Setup
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);

        $blockcf = new Vtiger_Block();
        $blockcf->label = 'LBL_CUSTOM_INFORMATION';
        $moduleInstance->addBlock($blockcf);

	$field1 = new Vtiger_Field();
	$field1->name = 'callsubject';
	$field1->table = $module->basetable;
	$field1->column = 'callsubject';
	$field1->label = 'Call Subject';
	$field1->columntype = 'VARCHAR(255)';
	$field1->uitype = 2;
	$field1->typeofdata = 'V~M';
	$block->addField($field1);
	$moduleInstance->setEntityIdentifier($field1);

	
	$field2 = new Vtiger_Field();
	$field2->name = 'customer';
	$field2->label= 'Customer';
	$field2->table = $module->basetable;
	$field2->column = 'customer';
	$field2->columntype = 'VARCHAR(100)';
	$field2->uitype = 10;
	$field2->typeofdata = 'V~O';
	$block->addField($field2);
	$field2->setRelatedModules(Array('Contacts'));

        $field3  = new Vtiger_Field();
        $field3->name = 'callbackdatetime';
        $field3->label= 'CallBack DateTime';
        $field3->uitype= 70;
        $field3->column = $field3->name;
        $field3->columntype = 'VARCHAR(100)';
        $field3->typeofdata = 'V~M';
        $block->addField($field3);


        $field5 = new Vtiger_Field();
        $field5->name = 'dialerrecord';
        $field5->label= 'DialerRecord';
        $field5->table = $module->basetable;
        $field5->column = 'dialerrecord';
        $field5->columntype = 'VARCHAR(100)';
        $field5->uitype = 10;
        $field5->typeofdata = 'V~O';
        $block->addField($field5);
        $field5->setRelatedModules(Array('DialerManager'));


        $field6  = new Vtiger_Field();
        $field6->name = 'callstatus';
        $field6->label= 'Call Status';
        $field6->uitype= 2;
        $field6->column = $field6->name;
        $field6->columntype = 'VARCHAR(255)';
        $field6->typeofdata = 'V~O';
        $block->addField($field6);


        $field7 = new Vtiger_Field();
        $field7->name = 'callbackrecord';
        $field7->label= 'CallbackRecord';
        $field7->table = $module->basetable;
        $field7->column = 'callbackrecord';
        $field7->columntype = 'VARCHAR(100)';
        $field7->uitype = 10;
        $field7->typeofdata = 'V~O';
        $block->addField($field7);
        $field7->setRelatedModules(Array('CallBackManager'));

        $field8  = new Vtiger_Field();
        $field8->name = 'campaignname';
        $field8->label= 'Campaign Name ';
        $field8->uitype= 2;
        $field8->column = $field8->name;
        $field8->columntype = 'VARCHAR(100)';
        $field8->typeofdata = 'V~M';
        $block->addField($field8);



        // Recommended common fields every Entity module should have (linked to core table)
        $mfield1 = new Vtiger_Field();
        $mfield1->name = 'assigned_user_id';
        $mfield1->label = 'Assigned To';
        $mfield1->table = 'vtiger_crmentity';
        $mfield1->column = 'smownerid';
        $mfield1->uitype = 53;
        $mfield1->typeofdata = 'V~M';
        $block->addField($mfield1);

        $mfield2 = new Vtiger_Field();
        $mfield2->name = 'CreatedTime';
        $mfield2->label= 'Created Time';
        $mfield2->table = 'vtiger_crmentity';
        $mfield2->column = 'createdtime';
        $mfield2->uitype = 70;
        $mfield2->typeofdata = 'T~O';
        $mfield2->displaytype= 2;
        $block->addField($mfield2);

        $mfield3 = new Vtiger_Field();
        $mfield3->name = 'ModifiedTime';
        $mfield3->label= 'Modified Time';
        $mfield3->table = 'vtiger_crmentity';
        $mfield3->column = 'modifiedtime';
        $mfield3->uitype = 70;
        $mfield3->typeofdata = 'T~O';
        $mfield3->displaytype= 2;
        $block->addField($mfield3);

	echo "i am here";
        // Filter Setup
        $filter1 = new Vtiger_Filter();
        $filter1->name = 'All';
        $filter1->isdefault = true;
        $moduleInstance->addFilter($filter1);
        $filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($mfield1, 3)->addField($mfield2, 4);
	echo "i am here2";

        // Sharing Access Setup
        $moduleInstance->setDefaultSharing('Public');

        // Webservice Setup
        $moduleInstance->initWebservice();
	echo "i am here3";

        mkdir('modules/'.$MODULENAME);
	echo "i am here4";
        echo "OK\n";
        $moduleInstance = Vtiger_Module::getInstance('Contacts');
        $accountsModule = Vtiger_Module::getInstance('CallBackManager');
        $relationLabel = 'CallBackManager';
        $moduleInstance->setRelatedList(
                $accountsModule, $relationLabel, Array('ADD'),'get_dependents_list'
        );
	
	echo "Relation Done";
}
