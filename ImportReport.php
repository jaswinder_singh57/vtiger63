<?php

include_once 'modules/Vtiger/CRMEntity.php';

class ImportReport extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_importreport';
        var $table_index= 'importreportid';

        var $customFieldTable = Array('vtiger_importreportcf', 'importreportid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_importreport', 'vtiger_importreportcf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_importreport' => 'importreportid',
                'vtiger_importreportcf'=>'importreportid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'ImportedFile' => Array('importreport', 'vtigerfield'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'ImportedFile' => 'importedfile',
                'Assigned To' => 'assigned_user_id',
        );

        // Make the field link to detail view
        var $list_link_field = 'ImportedFile';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'ImportedFile' => Array('importreport', 'vtigerfield'),
                'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'ImportedFile' => 'importedfile',
                'Assigned To' => 'assigned_user_id',
        );

        // For Popup window record selection
        var $popup_fields = Array ('importedfile');

        // For Alphabetical search
        var $def_basicsearch_col = 'importedfile';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'importedfile';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('importedfile','assigned_user_id');

        var $default_order_by = 'importedfile';
        var $default_sort_order='ASC';
}
