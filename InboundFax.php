<?php

include_once 'modules/Vtiger/CRMEntity.php';

class InboundFax extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_inboundfax';
        var $table_index= 'inboundfaxid';

        var $customFieldTable = Array('vtiger_inboundfaxcf', 'inboundfaxid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_inboundfax', 'vtiger_inboundfaxcf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_inboundfax' => 'inboundfaxid',
                'vtiger_inboundfaxcf'=>'inboundfaxid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Title' => Array('inboundfax', 'faxtitle'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Title' => 'faxtitle',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'faxtitle';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Title' => Array('inboundfax', 'faxtitle'),
                'FileLink' => Array('inboundfax', 'filelink'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Title' => 'faxtitle',
                'FileLink' => 'filelink',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('faxtitle');

        // For Alphabetical search
        var $def_basicsearch_col = 'faxtitle';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'faxtitle';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('faxtitle','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
