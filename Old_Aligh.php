<?php


function write_log ($log) {
	global $logfile;
	$fp = fopen($logfile,'a+');
	fwrite($fp,$log."\n");
	fclose($fp);
}


include('import_config.php');
include("conn.php");

echo "Starting\n";
$today = date('m-d-y');

$query = "select a.crmid,b.$column_c_field,b.$pt_desc_field from vtiger_crmentity as a inner join vtiger_patientnotes as b on a.crmid=b.patientnotesid where a.deleted='0'";
$result = mysql_query($query,$link);
while ($row = mysql_fetch_assoc($result)){
	$crmid = $row['crmid'];
	$column_c_data = $row["$column_c_field"];
        if (array_key_exists("$column_c_data",$ptnotes_align_arr)) {
	        if ($DEBUG)  { write_log("$today $remote_filename :: Align Mapping For $column_c_data Is available  crmid $crmid ");}
                $align_array = $ptnotes_align_arr["$column_c_data"];
                $raw_data = $row["$pt_desc_field"];
                foreach($align_array as $search_str => $newline) {
                	$break = '';
                        while($newline > 0) {
                        	$break = $break."\n";
                                $newline--;
                        }
                        $replace_str = $break."".$search_str;
                        $raw_data = str_replace("$search_str","$replace_str",$raw_data);
                }
          	$contactdata["$pt_desc_field"] = $raw_data;
       	} else {
                if ($DEBUG)  { write_log("$today $remote_filename :: Align Mapping For $column_c_data not found  $crmid ");}
        }
}


echo "Finished\n";
?>
