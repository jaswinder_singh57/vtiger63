<?php /* Smarty version Smarty-3.1.7, created on 2016-08-19 17:36:40
         compiled from "/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/InboundSMS/dashboards/InboundMessages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:93208599357b743a84a2fc9-34301708%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64e747813a9b7379a917315fef7b3e59e705108a' => 
    array (
      0 => '/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/InboundSMS/dashboards/InboundMessages.tpl',
      1 => 1471628124,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '93208599357b743a84a2fc9-34301708',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'MODULE_NAME' => 0,
    'STYLES' => 0,
    'cssModel' => 0,
    'SCRIPTS' => 0,
    'jsModel' => 0,
    'WIDGET' => 0,
    'SPANSIZE' => 0,
    'DATA' => 0,
    'LINE_ITEM_DETAIL' => 0,
    'LISTLINK' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_57b743a855f2e',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57b743a855f2e')) {function content_57b743a855f2e($_smarty_tpl) {?>
<script type="text/javascript">
//	Vtiger_Barchat_Widget_Js('Vtiger_Ticketsbystatus_Widget_Js',{},{});
</script>
<div class="dashboardWidgetHeader">
<!--	<?php echo $_smarty_tpl->getSubTemplate (vtemplate_path("dashboards/WidgetHeader.tpl",$_smarty_tpl->tpl_vars['MODULE_NAME']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('SETTING_EXIST'=>false), 0);?>
-->
<?php  $_smarty_tpl->tpl_vars['cssModel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cssModel']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['STYLES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cssModel']->key => $_smarty_tpl->tpl_vars['cssModel']->value){
$_smarty_tpl->tpl_vars['cssModel']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['cssModel']->key;
?>
        <link rel="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getRel();?>
" href="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getHref();?>
" type="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getType();?>
" media="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getMedia();?>
" />
<?php } ?>
<?php  $_smarty_tpl->tpl_vars['jsModel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['jsModel']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['SCRIPTS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['jsModel']->key => $_smarty_tpl->tpl_vars['jsModel']->value){
$_smarty_tpl->tpl_vars['jsModel']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['jsModel']->key;
?>
        <script type="<?php echo $_smarty_tpl->tpl_vars['jsModel']->value->getType();?>
" src="<?php echo $_smarty_tpl->tpl_vars['jsModel']->value->getSrc();?>
"></script>
<?php } ?>

<table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
                <tr>
                        <td class="span5">
                                <div class="dashboardTitle textOverflowEllipsis" title="<?php echo vtranslate($_smarty_tpl->tpl_vars['WIDGET']->value->getTitle(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
" style="width: 15em;"><b>&nbsp;&nbsp;<?php echo vtranslate($_smarty_tpl->tpl_vars['WIDGET']->value->getTitle(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
</b></div>
                        </td>
                        <td class="refresh span2" align="right">
                                <span style="position:relative;">&nbsp;</span>
                        </td>
                        <td class="widgeticons span5" align="right">
                                <div class="box pull-right">
<?php if (!$_smarty_tpl->tpl_vars['WIDGET']->value->isDefault()){?>
        <a name="dclose" class="widget" data-url="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getDeleteUrl();?>
">
                <i class="icon-remove" hspace="2" border="0" align="absmiddle" title="<?php echo vtranslate('LBL_REMOVE');?>
" alt="<?php echo vtranslate('LBL_REMOVE');?>
"></i>
        </a>
<?php }?>

                                </div>
                        </td>
                </tr>
        </tbody>
</table>

</div>
<div class="dashboardWidgetContent">
	<?php $_smarty_tpl->tpl_vars["SPANSIZE"] = new Smarty_variable(12/3, null, 0);?>
	<div class="row-fluid" style="padding:5px">
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><strong>STATUS</strong></div>
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><strong>SENDER</strong></div>
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><strong>MESSAGE</strong></div>
        </div>
    <?php  $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->_loop = false;
 $_smarty_tpl->tpl_vars['INDEX'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['DATA']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->key => $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value){
$_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->_loop = true;
 $_smarty_tpl->tpl_vars['INDEX']->value = $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->key;
?>
        <div class="row-fluid" style="padding:5px">
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["link"];?>
"><?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["status"];?>
</a></div>
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["clink"];?>
"><?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["custname"];?>
</a></div>
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["message"];?>
</div>
        </div>
 <?php } ?>		
	<div class="row-fluid" style="padding:5px">
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['LISTLINK']->value;?>
">More >> </a></div>
        </div>

</div>
<?php }} ?>