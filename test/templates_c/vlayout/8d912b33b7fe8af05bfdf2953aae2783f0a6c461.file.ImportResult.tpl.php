<?php /* Smarty version Smarty-3.1.7, created on 2016-02-11 09:46:08
         compiled from "/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/Calendar/ImportResult.tpl" */ ?>
<?php /*%%SmartyHeaderCode:128455846156bc58600b4af7-77472391%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d912b33b7fe8af05bfdf2953aae2783f0a6c461' => 
    array (
      0 => '/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/Calendar/ImportResult.tpl',
      1 => 1398442051,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '128455846156bc58600b4af7-77472391',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LEFTPANELHIDE' => 0,
    'MODULE' => 0,
    'FOR_MODULE' => 0,
    'ERROR_MESSAGE' => 0,
    'SUCCESS_EVENTS' => 0,
    'SKIPPED_EVENTS' => 0,
    'SUCCESS_TASKS' => 0,
    'SKIPPED_TASKS' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_56bc5860287da',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56bc5860287da')) {function content_56bc5860287da($_smarty_tpl) {?>
<div id="toggleButton" class="toggleButton" title="<?php echo vtranslate('LBL_LEFT_PANEL_SHOW_HIDE','Vtiger');?>
"><i id="tButtonImage" class="<?php if ($_smarty_tpl->tpl_vars['LEFTPANELHIDE']->value!='1'){?>icon-chevron-left<?php }else{ ?>icon-chevron-right<?php }?>"></i></div>&nbsp<div style="padding-left: 15px;"><input type="hidden" name="module" value="<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
" /><table style=" width:90%;margin-left: 5%" cellpadding="5" class="searchUIBasic well"><tr><td class="font-x-large" align="left" colspan="2"><strong><?php echo vtranslate('LBL_IMPORT',$_smarty_tpl->tpl_vars['MODULE']->value);?>
 <?php echo vtranslate($_smarty_tpl->tpl_vars['FOR_MODULE']->value,$_smarty_tpl->tpl_vars['MODULE']->value);?>
 - <?php echo vtranslate('LBL_RESULT',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></td></tr><?php if ($_smarty_tpl->tpl_vars['ERROR_MESSAGE']->value!=''){?><tr><td class="style1" align="left" colspan="2"><?php echo $_smarty_tpl->tpl_vars['ERROR_MESSAGE']->value;?>
</td></tr><?php }?><tr><td valign="top"><table cellpadding="5" cellspacing="0" align="center" width="100%" class="dvtSelectedCell thickBorder importContents"><tr><td><?php echo vtranslate('LBL_TOTAL_EVENTS_IMPORTED',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td width="10%">:</td><td width="30%"><?php echo $_smarty_tpl->tpl_vars['SUCCESS_EVENTS']->value;?>
</td></tr><tr><td><?php echo vtranslate('LBL_TOTAL_EVENTS_SKIPPED',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td width="10%">:</td><td width="30%"><?php echo $_smarty_tpl->tpl_vars['SKIPPED_EVENTS']->value;?>
</td></tr><tr><td><?php echo vtranslate('LBL_TOTAL_TASKS_IMPORTED',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td width="10%">:</td><td width="30%"><?php echo $_smarty_tpl->tpl_vars['SUCCESS_TASKS']->value;?>
</td></tr><tr><td><?php echo vtranslate('LBL_TOTAL_TASKS_SKIPPED',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td width="10%">:</td><td width="30%"><?php echo $_smarty_tpl->tpl_vars['SKIPPED_TASKS']->value;?>
</td></tr></table></td></tr><tr><td align="right" colspan="2"><button name="next" class="create btn"onclick="location.href='index.php?module=<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
&view=List'" ><strong><?php echo vtranslate('LBL_FINISH',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></button></td><td align="right" colspan="2"><button name="next" class="delete btn"onclick="location.href='index.php?module=<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
&view=Import&mode=undoImport'"><strong><?php echo vtranslate('LBL_UNDO_LAST_IMPORT',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></button></td></tr></table><?php }} ?>