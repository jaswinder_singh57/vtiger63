<?php /* Smarty version Smarty-3.1.7, created on 2016-04-06 01:46:42
         compiled from "/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/SQLReports/ReportHeaderSQL.tpl" */ ?>
<?php /*%%SmartyHeaderCode:171137318357046a820db257-48285105%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c08bd44818134deeb7543239039b019e7567590a' => 
    array (
      0 => '/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/SQLReports/ReportHeaderSQL.tpl',
      1 => 1459907141,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '171137318357046a820db257-48285105',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LEFTPANELHIDE' => 0,
    'DATE_FILTERS' => 0,
    'REPORT_MODEL' => 0,
    'MODULE' => 0,
    'COUNT' => 0,
    'DETAILVIEW_LINKS' => 0,
    'DETAILVIEW_LINK' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_57046a8212e30',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57046a8212e30')) {function content_57046a8212e30($_smarty_tpl) {?>
<div id="toggleButton" class="toggleButton" title="Left Panel Show/Hide"><i id="tButtonImage" class="<?php if ($_smarty_tpl->tpl_vars['LEFTPANELHIDE']->value!='1'){?>icon-chevron-left<?php }else{ ?>icon-chevron-right<?php }?>"></i></div><div class="reportsDetailHeader row-fluid"><input type="hidden" name="date_filters" data-value='<?php echo ZEND_JSON::encode($_smarty_tpl->tpl_vars['DATE_FILTERS']->value);?>
' /><div class="reportHeader row-fluid span12"><div class='span4' style="position:relative;left:10px"><?php if ($_smarty_tpl->tpl_vars['REPORT_MODEL']->value->isEditable()==true){?><button onclick='window.location.href="<?php echo $_smarty_tpl->tpl_vars['REPORT_MODEL']->value->getEditViewUrl();?>
"' type="button" class="cursorPointer btn"><strong><?php echo vtranslate('LBL_CUSTOMIZE',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong>&nbsp;<i class="icon-pencil"></i></button>&nbsp;<?php }?></div><div class='span4 textAlignCenter'><h3><?php echo $_smarty_tpl->tpl_vars['REPORT_MODEL']->value->getName();?>
</h3><div id="noOfRecords"><?php echo vtranslate('LBL_NO_OF_RECORDS',$_smarty_tpl->tpl_vars['MODULE']->value);?>
 <span id="countValue"><?php echo $_smarty_tpl->tpl_vars['COUNT']->value;?>
</span></div></div><div class='span4'><span class="pull-right" style='margin-right:50px;'><?php  $_smarty_tpl->tpl_vars['DETAILVIEW_LINK'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DETAILVIEW_LINKS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->key => $_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->value){
$_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->_loop = true;
?><img class="cursorPointer alignBottom" onclick='window.location.href="<?php echo $_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->value->getUrl();?>
"' src="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->value->get('linkicon');?>
<?php $_tmp1=ob_get_clean();?><?php echo vimage_path($_tmp1);?>
" alt="<?php echo vtranslate($_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE']->value);?>
" title="<?php echo vtranslate($_smarty_tpl->tpl_vars['DETAILVIEW_LINK']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE']->value);?>
" />&nbsp;<?php } ?></span></div></div></div><div id="reportContentsDiv"><?php }} ?>