<?php /* Smarty version Smarty-3.1.7, created on 2016-02-05 10:41:12
         compiled from "/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/Calendar/Import.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19409371856b47c48559151-49000984%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e72d80d558e310b36d222a2cc3317d2ec7506c2' => 
    array (
      0 => '/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/Calendar/Import.tpl',
      1 => 1380782177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19409371856b47c48559151-49000984',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'MODULE' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_56b47c485c583',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56b47c485c583')) {function content_56b47c485c583($_smarty_tpl) {?>
<div id="importRecordsContainer" class='modelContainer'><div class="modal-header"><button data-dismiss="modal" class="close" title="<?php echo vtranslate('LBL_CLOSE');?>
">x</button><h3 id="importRecordHeader"><?php echo vtranslate('LBL_IMPORT_RECORDS',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</h3></div><form method="POST" action="index.php" enctype="multipart/form-data" id="ical_import" name="ical_import"><input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['MODULE']->value;?>
" name="module"><div name='importRecordsContent'><input type="hidden" value="Import" name="view"><input type="hidden" value="importResult" name="mode"><div class="modal-body tabbable"><div class="tab-content massEditContent"><table class="massEditTable table table-bordered"><tr><td class="fieldLabel alignMiddle"><?php echo vtranslate('LBL_IMPORT_RECORDS',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</td><td class="fieldValue"><input type="file" data-validation-engine="validate[required]" id="import_file" name="import_file" class="small"></td></tr></table></div></div></div><div class="modal-footer"><div class=" pull-right cancelLinkContainer"><a class="cancelLink" type="reset" data-dismiss="modal"><?php echo vtranslate('LBL_CANCEL',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</a></div><button class="btn btn-success" type="submit" name="saveButton"><strong><?php echo vtranslate('LBL_IMPORT',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</strong></button></div></form></div><?php }} ?>