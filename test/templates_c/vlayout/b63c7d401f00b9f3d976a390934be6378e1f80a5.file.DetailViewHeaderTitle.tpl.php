<?php /* Smarty version Smarty-3.1.7, created on 2016-02-05 07:13:14
         compiled from "/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/PBXManager/DetailViewHeaderTitle.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26002450556b44b8aa9a626-85687316%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b63c7d401f00b9f3d976a390934be6378e1f80a5' => 
    array (
      0 => '/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/PBXManager/DetailViewHeaderTitle.tpl',
      1 => 1451987414,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26002450556b44b8aa9a626-85687316',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'RECORD' => 0,
    'MODULE_INSTANCE' => 0,
    'IMAGE_DETAILS' => 0,
    'IMAGE_INFO' => 0,
    'MODULE_MODEL' => 0,
    'NAME_FIELD' => 0,
    'FIELD_MODEL' => 0,
    'RECORDID' => 0,
    'MODULE' => 0,
    'ENTITY_NAMES' => 0,
    'CALLER_INFO' => 0,
    'MODULEMODEL' => 0,
    'FIELDMODEL' => 0,
    'CALLERNAME' => 0,
    'FIELD_NAME' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_56b44b8acc960',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56b44b8acc960')) {function content_56b44b8acc960($_smarty_tpl) {?>
<span class="span2"><div style="position:relative;display:inline;"><?php if ($_smarty_tpl->tpl_vars['RECORD']->value->get('customer')&&$_smarty_tpl->tpl_vars['RECORD']->value->get('customertype')=='Contacts'){?><?php $_smarty_tpl->tpl_vars['MODULE_INSTANCE'] = new Smarty_variable(Vtiger_Record_Model::getInstanceById($_smarty_tpl->tpl_vars['RECORD']->value->get('customer'),$_smarty_tpl->tpl_vars['RECORD']->value->get('customertype')), null, 0);?><?php $_smarty_tpl->tpl_vars['IMAGE_DETAILS'] = new Smarty_variable($_smarty_tpl->tpl_vars['MODULE_INSTANCE']->value->getImageDetails(), null, 0);?><?php if ($_smarty_tpl->tpl_vars['IMAGE_DETAILS']->value){?><?php  $_smarty_tpl->tpl_vars['IMAGE_INFO'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['IMAGE_INFO']->_loop = false;
 $_smarty_tpl->tpl_vars['ITER'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['IMAGE_DETAILS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['IMAGE_INFO']->key => $_smarty_tpl->tpl_vars['IMAGE_INFO']->value){
$_smarty_tpl->tpl_vars['IMAGE_INFO']->_loop = true;
 $_smarty_tpl->tpl_vars['ITER']->value = $_smarty_tpl->tpl_vars['IMAGE_INFO']->key;
?><?php if (!empty($_smarty_tpl->tpl_vars['IMAGE_INFO']->value['path'])){?><img src="<?php echo $_smarty_tpl->tpl_vars['IMAGE_INFO']->value['path'];?>
_<?php echo $_smarty_tpl->tpl_vars['IMAGE_INFO']->value['orgname'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['IMAGE_INFO']->value['orgname'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['IMAGE_INFO']->value['orgname'];?>
" ><?php }else{ ?><img src="<?php echo vimage_path('summary_Contact.png');?>
" class="summaryImg"/><?php }?><?php } ?><?php }else{ ?><img src="<?php echo vimage_path('summary_Contact.png');?>
" class="summaryImg"/><?php }?><?php }else{ ?><img src="<?php echo vimage_path('summary_Contact.png');?>
" class="summaryImg"/><?php }?><?php if ($_smarty_tpl->tpl_vars['RECORD']->value->get('direction')=='inbound'){?><img src="modules/PBXManager/resources/images/Incoming.png" style="position:absolute;bottom:4px;right:0;"></div><?php }elseif($_smarty_tpl->tpl_vars['RECORD']->value->get('direction')=='outbound'){?><img src="modules/PBXManager/resources/images/Outgoing.png" style="position:absolute;bottom:4px;right:0;"></div><?php }else{ ?></div><?php }?></span><span class="span10 margin0px"><span class="row-fluid"><span class="recordLabel pushDown" title="<?php echo $_smarty_tpl->tpl_vars['RECORD']->value->getName();?>
"><?php $_smarty_tpl->tpl_vars['NAME_FIELD'] = new Smarty_variable($_smarty_tpl->tpl_vars['MODULE_MODEL']->value->getNameFields(), null, 0);?><?php $_smarty_tpl->tpl_vars['FIELD_MODEL'] = new Smarty_variable($_smarty_tpl->tpl_vars['MODULE_MODEL']->value->getField($_smarty_tpl->tpl_vars['NAME_FIELD']->value), null, 0);?><?php if ($_smarty_tpl->tpl_vars['FIELD_MODEL']->value->getPermissions()){?><?php $_smarty_tpl->tpl_vars['RECORDID'] = new Smarty_variable($_smarty_tpl->tpl_vars['RECORD']->value->get("customer"), null, 0);?><?php if ($_smarty_tpl->tpl_vars['RECORDID']->value){?><?php $_smarty_tpl->tpl_vars['MODULE'] = new Smarty_variable($_smarty_tpl->tpl_vars['RECORD']->value->get('customertype'), null, 0);?><?php $_smarty_tpl->tpl_vars['ENTITY_NAMES'] = new Smarty_variable(getEntityName($_smarty_tpl->tpl_vars['MODULE']->value,array($_smarty_tpl->tpl_vars['RECORDID']->value)), null, 0);?><?php $_smarty_tpl->tpl_vars['CALLERNAME'] = new Smarty_variable($_smarty_tpl->tpl_vars['ENTITY_NAMES']->value[$_smarty_tpl->tpl_vars['RECORDID']->value], null, 0);?><?php }else{ ?><?php $_smarty_tpl->tpl_vars['CALLERNAME'] = new Smarty_variable($_smarty_tpl->tpl_vars['RECORD']->value->get("customernumber"), null, 0);?><?php }?><?php $_smarty_tpl->tpl_vars['CALLER_INFO'] = new Smarty_variable(PBXManager_Record_Model::lookUpRelatedWithNumber($_smarty_tpl->tpl_vars['RECORD']->value->get('customernumber')), null, 0);?><?php if ($_smarty_tpl->tpl_vars['CALLER_INFO']->value['id']){?><?php $_smarty_tpl->tpl_vars['MODULEMODEL'] = new Smarty_variable(Vtiger_Module_Model::getInstance($_smarty_tpl->tpl_vars['RECORD']->value->get('customertype')), null, 0);?><?php $_smarty_tpl->tpl_vars['FIELDMODEL'] = new Smarty_variable(Vtiger_Field_Model::getInstance($_smarty_tpl->tpl_vars['CALLER_INFO']->value['fieldname'],$_smarty_tpl->tpl_vars['MODULEMODEL']->value), null, 0);?><?php $_smarty_tpl->tpl_vars['FIELD_NAME'] = new Smarty_variable($_smarty_tpl->tpl_vars['FIELDMODEL']->value->get('label'), null, 0);?><?php }?><?php if ($_smarty_tpl->tpl_vars['RECORD']->value->get('direction')=='inbound'){?>&nbsp;<strong><span class="<?php echo $_smarty_tpl->tpl_vars['NAME_FIELD']->value;?>
"><?php echo vtranslate('LBL_CALL_FROM',$_smarty_tpl->tpl_vars['MODULE_MODEL']->value->get('name'));?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['CALLERNAME']->value;?>
</span><br/></strong><?php }else{ ?>&nbsp;<strong><span class="<?php echo $_smarty_tpl->tpl_vars['NAME_FIELD']->value;?>
"><?php echo vtranslate('LBL_CALL_TO',$_smarty_tpl->tpl_vars['MODULE_MODEL']->value->get('name'));?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['CALLERNAME']->value;?>
</span><br/></strong><?php }?><?php if ($_smarty_tpl->tpl_vars['FIELD_NAME']->value){?>&nbsp;<?php echo $_smarty_tpl->tpl_vars['FIELD_NAME']->value;?>
:&nbsp;<span class="title_label muted"><?php echo $_smarty_tpl->tpl_vars['RECORD']->value->get('customernumber');?>
</span><?php }?><?php }?></span></span></span>
<?php }} ?>