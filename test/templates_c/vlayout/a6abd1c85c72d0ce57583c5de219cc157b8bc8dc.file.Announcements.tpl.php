<?php /* Smarty version Smarty-3.1.7, created on 2018-01-19 12:07:26
         compiled from "/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/Announcements/dashboards/Announcements.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3309810115a61d70546e834-80423836%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6abd1c85c72d0ce57583c5de219cc157b8bc8dc' => 
    array (
      0 => '/var/www/html/vtiger63/includes/runtime/../../layouts/vlayout/modules/Announcements/dashboards/Announcements.tpl',
      1 => 1516363628,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3309810115a61d70546e834-80423836',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_5a61d7057f629',
  'variables' => 
  array (
    'STYLES' => 0,
    'cssModel' => 0,
    'SCRIPTS' => 0,
    'jsModel' => 0,
    'WIDGET' => 0,
    'MODULE_NAME' => 0,
    'SPANSIZE' => 0,
    'DATA' => 0,
    'LINE_ITEM_DETAIL' => 0,
    'LISTLINK' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a61d7057f629')) {function content_5a61d7057f629($_smarty_tpl) {?>
<div class="dashboardWidgetHeader">
<?php  $_smarty_tpl->tpl_vars['cssModel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cssModel']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['STYLES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cssModel']->key => $_smarty_tpl->tpl_vars['cssModel']->value){
$_smarty_tpl->tpl_vars['cssModel']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['cssModel']->key;
?>
        <link rel="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getRel();?>
" href="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getHref();?>
" type="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getType();?>
" media="<?php echo $_smarty_tpl->tpl_vars['cssModel']->value->getMedia();?>
" />
<?php } ?>
<?php  $_smarty_tpl->tpl_vars['jsModel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['jsModel']->_loop = false;
 $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['SCRIPTS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['jsModel']->key => $_smarty_tpl->tpl_vars['jsModel']->value){
$_smarty_tpl->tpl_vars['jsModel']->_loop = true;
 $_smarty_tpl->tpl_vars['index']->value = $_smarty_tpl->tpl_vars['jsModel']->key;
?>
        <script type="<?php echo $_smarty_tpl->tpl_vars['jsModel']->value->getType();?>
" src="<?php echo $_smarty_tpl->tpl_vars['jsModel']->value->getSrc();?>
"></script>
<?php } ?>

<table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
                <tr>
                        <td class="span5">
                                <div class="dashboardTitle textOverflowEllipsis" title="<?php echo vtranslate($_smarty_tpl->tpl_vars['WIDGET']->value->getTitle(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
" style="width: 15em;"><b>&nbsp;&nbsp;<?php echo vtranslate($_smarty_tpl->tpl_vars['WIDGET']->value->getTitle(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
</b></div>
                        </td>
                        <td class="refresh span2" align="right">
                                <span style="position:relative;">&nbsp;</span>
                        </td>
                        <td class="widgeticons span5" align="right">
                                <div class="box pull-right">
<?php if (!$_smarty_tpl->tpl_vars['WIDGET']->value->isDefault()){?>
        <a name="dclose" class="widget" data-url="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getDeleteUrl();?>
">
                <i class="icon-remove" hspace="2" border="0" align="absmiddle" title="<?php echo vtranslate('LBL_REMOVE');?>
" alt="<?php echo vtranslate('LBL_REMOVE');?>
"></i>
        </a>
<?php }?>

                                </div>
                        </td>
                </tr>
        </tbody>
</table>

</div>
<div class="dashboardWidgetContent">
	<?php $_smarty_tpl->tpl_vars["SPANSIZE"] = new Smarty_variable(12/4, null, 0);?>
	<div class="row-fluid" style="padding:5px">
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><strong>Title</strong></div>
                <div class="span6"><strong>Message</strong></div>
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><strong>AckStatus</strong></div>
        </div>
    <?php  $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->_loop = false;
 $_smarty_tpl->tpl_vars['INDEX'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['DATA']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->key => $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value){
$_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->_loop = true;
 $_smarty_tpl->tpl_vars['INDEX']->value = $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->key;
?>
        <div class="row-fluid" style="padding:5px">
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value['entitylink'];?>
"><?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["annoucementtitle"];?>
</a></div>
                <div class="span6"><?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["description"];?>
</div>
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LINE_ITEM_DETAIL']->value["ackstatus"];?>
</div>
        </div>
 <?php } ?>		
	<div class="row-fluid" style="padding:5px">
                <div class="span<?php echo $_smarty_tpl->tpl_vars['SPANSIZE']->value;?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['LISTLINK']->value;?>
">More >> </a></div>
        </div>

</div>
<?php }} ?>