<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'InboundFax';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
if ($moduleInstance || file_exists('modules/'.$MODULENAME)) {
        echo "Module already present - choose a different name.";
} else {
        $moduleInstance = new Vtiger_Module();
        $moduleInstance->name = $MODULENAME;
        $moduleInstance->parent= 'Tools';
        $moduleInstance->save();

        // Schema Setup
        $moduleInstance->initTables();

        // Field Setup
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);

        $blockcf = new Vtiger_Block();
        $blockcf->label = 'LBL_CUSTOM_INFORMATION';
        $moduleInstance->addBlock($blockcf);

	$field1 = new Vtiger_Field();
	$field1->name = 'faxtitle';
	$field1->table = $module->basetable;
	$field1->column = 'faxtitle';
	$field1->label = 'Title';
	$field1->columntype = 'VARCHAR(100)';
	$field1->uitype = 2;
	$field1->typeofdata = 'V~M';
	$block->addField($field1);
	$moduleInstance->setEntityIdentifier($field1);

	
	$field2 = new Vtiger_Field();
	$field2->name = 'faxstatus';
	$field2->label= 'Status';
	$field2->column = 'faxstatus';
	$field2->columntype = 'VARCHAR(100)';
	$field2->uitype = 2;
	$field2->typeofdata = 'V~M';
	$block->addField($field2);

        $field3  = new Vtiger_Field();
        $field3->name = 'patient';
        $field3->label= 'Patient';
        $field3->uitype= 10;
        $field3->column = $field3->name;
        $field3->columntype = 'VARCHAR(100)';
        $field3->typeofdata = 'V~O';
        $block->addField($field3);
	$field3->setRelatedModules(Array('Contacts'));

        $field4  = new Vtiger_Field();
        $field4->name = 'doctor';
        $field4->label= 'Doctor';
        $field4->uitype= 10;
        $field4->column = $field4->name;
        $field4->columntype = 'VARCHAR(100)';
        $field4->typeofdata = 'V~O';
        $block->addField($field4);
	$field4->setRelatedModules(Array('Accounts'));

        $field5  = new Vtiger_Field();
        $field5->name = 'vendor';
        $field5->label= 'Vendor';
        $field5->uitype= 10;
        $field5->column = $field5->name;
        $field5->columntype = 'VARCHAR(100)';
        $field5->typeofdata = 'V~O';
        $block->addField($field5);
	$field5->setRelatedModules(Array('Vendors'));


        $field6  = new Vtiger_Field();
        $field6->name = 'filelink';
        $field6->label= 'FileLink';
        $field6->uitype= 10;
        $field6->column = $field6->name;
        $field6->columntype = 'VARCHAR(255)';
        $field6->typeofdata = 'V~O';
        $block->addField($field6);
	$field6->setRelatedModules(Array('Documents'));

        $field7  = new Vtiger_Field();
        $field7->name = 'caller';
        $field7->label= 'Caller';
        $field7->uitype= 11;
        $field7->column = $field7->name;
        $field7->columntype = 'VARCHAR(100)';
        $field7->typeofdata = 'V~O';
        $block->addField($field7);

        $field8  = new Vtiger_Field();
        $field8->name = 'called';
        $field8->label= 'Called';
        $field8->uitype= 11;
        $field8->column = $field8->name;
        $field8->columntype = 'VARCHAR(100)';
        $field8->typeofdata = 'V~O';
        $block->addField($field8);

        $field9  = new Vtiger_Field();
        $field9->name = 'pages';
        $field9->label= 'Pages';
        $field9->uitype= 1;
        $field9->column = $field9->name;
        $field9->columntype = 'VARCHAR(100)';
        $field9->typeofdata = 'V~O';
        $block->addField($field9);


        // Recommended common fields every Entity module should have (linked to core table)
        $mfield1 = new Vtiger_Field();
        $mfield1->name = 'assigned_user_id';
        $mfield1->label = 'Assigned To';
        $mfield1->table = 'vtiger_crmentity';
        $mfield1->column = 'smownerid';
        $mfield1->uitype = 53;
        $mfield1->typeofdata = 'V~M';
        $block->addField($mfield1);

        $mfield2 = new Vtiger_Field();
        $mfield2->name = 'CreatedTime';
        $mfield2->label= 'Created Time';
        $mfield2->table = 'vtiger_crmentity';
        $mfield2->column = 'createdtime';
        $mfield2->uitype = 70;
        $mfield2->typeofdata = 'T~O';
        $mfield2->displaytype= 2;
        $block->addField($mfield2);

        $mfield3 = new Vtiger_Field();
        $mfield3->name = 'ModifiedTime';
        $mfield3->label= 'Modified Time';
        $mfield3->table = 'vtiger_crmentity';
        $mfield3->column = 'modifiedtime';
        $mfield3->uitype = 70;
        $mfield3->typeofdata = 'T~O';
        $mfield3->displaytype= 2;
        $block->addField($mfield3);

        // Filter Setup
        $filter1 = new Vtiger_Filter();
        $filter1->name = 'All';
        $filter1->isdefault = true;
        $moduleInstance->addFilter($filter1);
        $filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($field4, 3)->addField($field5, 4)->addField($field6, 5)->addField($field7, 6)->addField($field9, 7)->addField($mfield2, 8);

        // Sharing Access Setup
        $moduleInstance->setDefaultSharing('Public');

        // Webservice Setup
        $moduleInstance->initWebservice();

        mkdir('modules/'.$MODULENAME);
        echo "OK\n";
        $contacts = Vtiger_Module::getInstance('Contacts');
        $accounts = Vtiger_Module::getInstance('Accounts');
        $vendors = Vtiger_Module::getInstance('Vendors');
        $inboundfax = Vtiger_Module::getInstance('InboundFax');
        $relationLabel = 'InboundFax';
        $contacts->setRelatedList(
                $inboundfax, $relationLabel, Array(''),'get_dependents_list'
        );
        $accounts->setRelatedList(
                $inboundfax, $relationLabel, Array(''),'get_dependents_list'
        );
        $vendors->setRelatedList(
                $inboundfax, $relationLabel, Array(''),'get_dependents_list'
        );
	
	echo "Relation Done";
}
