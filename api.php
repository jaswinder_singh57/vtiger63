<?php
include_once 'include/Webservices/ModuleTypes.php';
include_once 'include/Webservices/Utils.php';
include_once 'include/Webservices/Create.php';
include_once 'include/Webservices/DescribeObject.php';
include_once 'modules/Users/Users.php';
include_once 'include/utils/CommonUtils.php';
include_once 'includes/runtime/LanguageHandler.php';
include_once 'includes/Loader.php';
include_once 'modules/Users/models/Record.php';


include_once('config.php');
require_once('include/logging.php');
require_once('data/Tracker.php');
require_once('include/utils/utils.php');
require_once('include/utils/UserInfoUtil.php');
require_once("include/Zend/Json.php");

//include_once 'vtlib/Vtiger/Functions.php';

$current_user = CRMEntity::getInstance('Users');
$current_user->retrieveCurrentUserInfoFromFile(1);

try {
        $typeInformation = vtws_listtypes(array(), $current_user);
        foreach ($typeInformation['types'] as $name) {
                echo sprintf("%s\n", $name);
        }
        foreach ($typeInformation['information'] as $name => $information) {
                echo sprintf("Name: %s, Label: %s, SingluarLabel: %s, IsEntity: %s \n",
                        $name, $information['label'], $information['singular'], ($information['isEntity']? "yes": "no"));
        }
} catch (WebServiceException $ex) {
        echo $ex->getMessage();
}
	

?>
