<?php
function date_difference ($date1timestamp, $date2timestamp) {
	$all = round(($date1timestamp - $date2timestamp) / 60);
	$d = floor ($all / 1440);
	$h = floor (($all - $d * 1440) / 60);
	$m = $all - ($d * 1440) - ($h * 60);
	return array('hours'=>$h, 'mins'=>$m);
}

function sftp_get_file ($remote_filename,$local_filename) {


	global $fetch_sftp_domain;
	global $fetch_sftp_password;
		
	System("sshpass -p '$fetch_sftp_password' sftp -o StrictHostKeyChecking=no  -oBatchMode=no -b - $fetch_sftp_domain << !
   get $remote_filename $local_filename
   bye
!
");
	if (file_exists($local_filename)){
		return "Success";
	} else {

		return "FileDownloadFailed";
	}

}


function unzip_file ($filename,$location) {

	$zip = new ZipArchive;
	if ($zip->open("$filename") === TRUE) {
		$zip->extractTo("$location");
		$zip->close();
		return "Success";
	} else {
		return "UnableToUnzip";
	}
}

function send_alert ($remote_file,$reason) {

	global $DEBUG;
	global $crmpath;
	global $email_server;
	global $email_it;
	global $email_from;
	
	require_once($crmpath."modules/Emails/class.phpmailer.php");


	$msg = "Dear Team,\n";
	$msg .= "\n";
	$msg .= "File Import Alert:-\n";
	$msg .= "\nFilename $remote_file can not be processed due to following error\n";
	$msg .= "\n$reason\n";
	$msg .= "\n\nThanks\nImportMailer\n";
	
	$mail = new PHPMailer(true);
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $email_server;
	$mail->Username = $email_from;
	$mail->From = $email_from;
	$mail->FromName = $email_from;
	$mail->AddReplyTo($email_from);
	$mail->WordWrap = 50;
	$mail->Subject = 'ImportAlert';
	$mail->addAddress($email_it);
	$mail->Body = $msg; 

	if(!$mail->send()) {
	   echo 'Message could not be sent.';
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	   return;
	}
	echo "Mail Sent";
	return;
	
}


function write_import_status($remote_filename,$total,$new,$merged,$error,$status) {

	global $link;
	global $today;	
	global $DEBUG;	

        $query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res=mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}

        $query = "select id from vtiger_crmentity_seq";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res=mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        $row = mysql_fetch_row($res);
        $crm_id = $row[0];

	$query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($crm_id,'1','1','ImportReport',NULL,'1',now(),now())";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        $query = "UPDATE vtiger_crmentity SET label='$remote_filename' WHERE crmid='$crm_id'";
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);

        $query = "insert into vtiger_importreport (importreportid,importedfile,totalrecords,mergedrecords,newrecords,errorrecords,importstatus) values($crm_id,'$remote_filename','$total','$merged','$new','$error','$status')";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}

        $query = "insert into vtiger_importreportcf (importreportid) values($crm_id)";
        if ($DEBUG)  { write_log("$today $remote_filename :: $query  ");}
        $res = mysql_query($query,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}

	return "Success";	


}


function Insurance_import () {

	global $today;
	global $extractpath;
	global $link;
	global $insurance_template;
	global $logfile;
	global $DEBUG;
	global $webapiurl;
	global $username;
	global $secretkey;
	global $first_import_insurance;

	$file_suffix = "Payor";
	$remote_filename = $file_suffix."_".$today.".zip";
	$local_filename = $extractpath."/".$remote_filename;
	$file_csv = $extractpath."/".$file_suffix.".csv";
/*
	if ($DEBUG)  { write_log("$today $remote_filename Getting File");} 
	$response = sftp_get_file($remote_filename,$local_filename);	
	if ($DEBUG)  { write_log("$today $remote_filename sftp $response ");} 

	if ($response != "Success"){
		send_alert($remote_filename,$response);
		return "$response";
	}
	
	$response = unzip_file($local_filename,$extractpath);
        
	
	if ($DEBUG)  { write_log("$today $remote_filename unzip $response ");} 
        if ($response != "Success"){
		send_alert($remote_filename,$response);
                return "$response";
        }
*/

        $fquery = "select tabid from vtiger_tab where name ='Insurance'";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: RetrieveId field not found $fquery");}
                send_alert($remote_filename,"RetrieveId Not Found in vtiger_ws_entity ");
                return "RetrieveId Not Found in vtiger_ws_entity";
        }
        $frow = mysql_fetch_array($fresult);
        $tabid = $frow["tabid"];



	$query = "select content from vtiger_import_maps where name='$insurance_template' and deleted='0' limit 1";		
	if ($DEBUG)  { write_log("$today $remote_filename :: $query ");} 
	$result = mysql_query($query,$link);
	$qstatus = mysql_affected_rows($link);
	if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");} 
	if (!mysql_num_rows($result)){
		if ($DEBUG)  { write_log("$today $remote_filename :: No Template $insurance_template ");} 
		send_alert($remote_filename,"NoTemplate $insurance_template");
		return "NoTemplate $insurance_template";
	}	
	$row = mysql_fetch_array($result);
	$mappingarr = explode('&',$row["content"]);
	
	if ($DEBUG)  { write_log("$today $remote_filename :: Template Found $insurance_template  ");} 
	$field_arr = array();
	$header_arr = array();	
	foreach ($mappingarr as $value) {
		$fields = explode('=',$value);	
		$header = $fields[0];
		$fieldname = $fields[1];
		$header_arr["$header"] = $fieldname;
		$fquery = "select tablename ,uitype from vtiger_field where columnname='$fieldname' and tabid='$tabid' limit 1";
		if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}     
		$fresult = mysql_query($fquery,$link);
		$qstatus = mysql_affected_rows($link);
		if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");} 
		if(!mysql_num_rows($fresult)){
			if ($DEBUG)  { write_log("$today $remote_filename :: Template Error $insurance_template field Not found $query ");} 
	                send_alert($remote_filename,"TemplateError $insurance_template Field Not found $fquery");
      	 	        return "NoTemplate $insurance_template field Not Found";
		}	
		$frow = mysql_fetch_array($fresult);
		$tablename = $frow["tablename"];
		$field_arr["$fieldname"] = $frow["uitype"];
	}
	
	$fquery = "select columnname ,uitype from vtiger_field where fieldlabel='ReportDt' and tablename='vtiger_insurance' limit 1";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
		if ($DEBUG)  { write_log("$today $remote_filename :: Import Date field not found $fquery");}
                send_alert($remote_filename,"Import Date Field Not Found ");
 		return "Import Date field Not Found";
	}
        $frow = mysql_fetch_array($fresult);
        $import_field_name = $frow["columnname"];

	$fquery = "select id from vtiger_ws_entity where name ='Insurance'";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: RetrieveId field not found $fquery");}
                send_alert($remote_filename,"RetrieveId Not Found in vtiger_ws_entity ");
                return "RetrieveId Not Found in vtiger_ws_entity";
        }
        $frow = mysql_fetch_array($fresult);
        $retrieve_id_prefix = $frow["id"];
	

	//login to server
	$loginurl = $webapiurl."?operation=getchallenge&username=$username";
	list($status,$response) = get_data($remote_filename,$loginurl);
	if ($status != 1 ){
		if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
	}
	$loginresponse = json_decode($response,true);
	if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
	}
	$challengeToken = $loginresponse['result']['token'];

	$generatedKey = md5($challengeToken.$secretkey);
	$params = array('operation'=>'login', 'username'=>$username,'accessKey'=>$generatedKey);

        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }

	$jsonResponse = json_decode($response,true);
	$sessionid = $jsonResponse['result']['sessionName'];
	$userid = $jsonResponse['result']['userId'];


	$fp = fopen("$file_csv", "r");
	if(!$fp) {
		if ($DEBUG)  { write_log("$today $remote_filename :: File Open Fail $file_csv  ");}
		send_alert($file_csv,"FailedToOpenFile");
		return "FailedToOpenFile $file_csv";
	}
	if ($DEBUG)  { write_log("$today $remote_filename :: File Opened $file_csv  ");}
	$firstloop = 1;	
	$header_index = array();
	$total=0;
	$new = 0;
	$error = 0;
	$merged = 0;
	while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
		if($firstloop){
			$firstloop = 0;	
			foreach ($header_arr as $key => $value) {
				foreach ($data as $d => $v) {
					if ("$v" == "ReportDt"){
						$header_index["ReportDt"] = $d; 
					}	
					if ("$key" == "$v"){
						$header_index["$key"] = $d;
						if ($DEBUG)  { write_log("$today $remote_filename :: $key = $d ");}
					}	
				}	
			}

			continue;
		}
		$total++;
		$err = 0;

                $payor_id = $header_index["PayorKey"];
                $payor_id = $data["$payor_id"];
                $payor_id = trim($payor_id);
                $payorname = $header_index["PayorName"];
                $table_field = $header_arr["PayorKey"];
		$last_update = $header_index["ReportDt"];
		$last_update = $data["$last_update"];
	
		$lastupdate_datetime = $last_update;
		if ($first_import_insurance) {
	        $status = check_import_required($lastupdate_datetime);
                if($status){
                        if ($DEBUG)  { write_log("$today $remote_filename :: Skiping record $payor_id Last Update  older 15 days $lastupdate_datetime  ");}
                        continue;
                }
		}

		$aquery = "select a.crmid,b.$import_field_name from vtiger_crmentity as a inner join vtiger_insurance as b on a.crmid=b.insuranceid where b.$table_field='$payor_id' and a.deleted='0'";


		if ($DEBUG)  { write_log("$today $remote_filename :: Search Payor $payorname $payor_id === $aquery  ");}
		$aresult = mysql_query($aquery,$link);
		$qstatus = mysql_affected_rows($link);
		if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");} 
                if ($qstatus < 0) {
                        $err = 1;
                        $reason = mysql_error($link);
                        write_log("$today $remote_filename :: Query Error $reason ");
			continue;
                }
		$modulename = 'Insurance';
		if (!mysql_num_rows($aresult)){
			if ($DEBUG)  { write_log("$today $remote_filename :: Payor Not Found Adding New $payorname  ");}

			$contactdata = array('payorname'=>$payorname,'assigned_user_id'=>$userid); 
			foreach ($header_index as $key => $value) {
				$data_value = $data["$value"];
				$crm_field = $header_arr["$key"];
				$field_type = $field_arr["$crm_field"];
				if ($field_type == '5' or $field_type == '23') {
                                        if ($data_value == ''){
                                                $data_value = '0000-00-00';
                                        } else {
                                                $new_value = format_date($data_value);
                                                if ($new_value == '1969-12-31') {
                                                        $data_value = '0000-00-00';
                                                } else {
                                                        $data_value = $new_value;
                                                }
                                        }
				}
				
				$set_field = $crm_field."='".mysql_real_escape_string($data_value)."'";
				$contactdata["$crm_field"] = mysql_real_escape_string($data_value);
			}
			if ($lastupdate_datetime == '') {
				$contactdata["$import_field_name"] = get_gmt();
			} else {
				$contactdata["$import_field_name"] = $lastupdate_datetime;
			}
			$objectjson=json_encode($contactdata);
			$params = array("sessionName"=>$sessionid, "operation"=>'create', "element"=>$objectjson, "elementType"=>$modulename);		
			list($status,$response) = post_data($remote_filename,$webapiurl,$params);
			if (!$status){
				$error++;
				continue;
			}	
				
			$jsonresponse = json_decode($response,true);	
			
			if ($jsonresponse["success"] == 1){
				$new++;

			} else {
				$error++;				
			}
				
		} else {
			//merge block
			$row = mysql_fetch_row($aresult);
                        $crm_id = $row[0];
                        $modified_time = $row[1];
			if ($DEBUG)  { write_log("$today $remote_filename :: Record Found CRMID $crm_id  $modified_time");}
	
			$status = update_required($modified_time,$lastupdate_datetime);
			if($status){
		                if ($DEBUG)  { write_log("$today $remote_filename :: Skiping record $crm_id Modifed $modified_time csv $lastupdate_datetime  ");}
				continue;
			}
			$retrieveid = $retrieve_id_prefix."x".$crm_id;
			$url = $webapiurl."?sessionName=$sessionid&operation=retrieve&id=$retrieveid";

		        list($status,$response) = get_data($remote_filename,$url);
		        if ($status != 1 ){
		                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id  ");}
		               	$error++;
				continue;
        		}
			$ret_response = json_decode($response,true);
		        if ($ret_response["success"] != 1) {
		                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id");}
				$error++;
				continue;
        		}
			
			$retrievedobject = $ret_response['result'];
		
			foreach ($header_index as $key => $value) {
                                if ($key == "ReportDt"){
                                        continue;
                                }
                                $data_value = $data["$value"];
                                $crm_field = $header_arr["$key"];
                                $field_type = $field_arr["$crm_field"];
                                if ($field_type == '5' or $field_type == '23') {
                                        if ($data_value == ''){
                                                $data_value = '0000-00-00';
                                        } else {
                                                $new_value = format_date($data_value);
                                                if ($new_value == '1969-12-31') {
                                                        $data_value = '0000-00-00';
                                                } else {
                                                        $data_value = $new_value;
                                                }
                                        }
                                }

                                $retrievedobject["$crm_field"] = mysql_real_escape_string($data_value);
                        }

			if ($lastupdate_datetime == '') {
                        	$retrievedobject["$import_field_name"] = get_gmt();
			} else {
                        	$retrievedobject["$import_field_name"] = $lastupdate_datetime;
			}
                        $objectjson=json_encode($retrievedobject);
                        $params = array("sessionName"=>$sessionid, "operation"=>'update', "element"=>$objectjson, "elementType"=>$modulename);
                        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
                        if (!$status){
                                $error++;
                                continue;
                        }

                        $jsonresponse = json_decode($response,true);

                        if ($jsonresponse["success"] == 1){
                                $merged++;

                        } else {
                                $error++;
                        }

		}	

	}
	fclose($fp);
	if ($DEBUG)  { write_log("$today $remote_filename :: Import Total = $total , New = $new , Merged = $merged , Error = $error ");}
	$url = $webapiurl."?operation=logout&sessionName=$sessionid";		
	list($status,$response) = get_data($remote_filename,$url);
	
	$status = "Imported";
	write_import_status($remote_filename,$total,$new,$merged,$error,$status);


        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }

	return "Success";
			
}


function process_ptpayor (){

        global $today;
        global $extractpath;
        global $link;
        global $ptpayor_template;
        global $logfile;
        global $DEBUG;
        global $webapiurl;
        global $username;
        global $secretkey;
	global $first_import_patient_insurance;
	global $patient_insurnace_mapping;
	global $crm_link;
	global $crm_link_field;

        $file_suffix = "PtPayor";
        $remote_filename = $file_suffix."_".$today.".zip";
        $local_filename = $extractpath."/".$remote_filename;
        $file_csv = $extractpath."/".$file_suffix.".csv";
/*
        if ($DEBUG)  { write_log("$today $remote_filename Getting File");}
        $response = sftp_get_file($remote_filename,$local_filename);
        if ($DEBUG)  { write_log("$today $remote_filename sftp $response ");}

        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }
        $response = unzip_file($local_filename,$extractpath);


        if ($DEBUG)  { write_log("$today $remote_filename unzip $response ");}
        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }
*/
        $mappingarr = explode('&',$patient_insurnace_mapping);

        if ($DEBUG)  { write_log("$today $remote_filename :: Template Found $ptpayor_template  ");}
        $field_arr = array();
        $header_arr = array();

        foreach ($mappingarr as $value) {
                $fields = explode('=',$value);
                $header = $fields[0];
                $fieldname = $fields[1];
                $header_arr["$header"] = $fieldname;
                $fquery = "select tablename,uitype from vtiger_field where columnname='$fieldname' and tabid='4'";
                if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
                $fresult = mysql_query($fquery,$link);
                $qstatus = mysql_affected_rows($link);
                if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
                if(!mysql_num_rows($fresult)){
                        if ($DEBUG)  { write_log("$today $remote_filename :: Template Error $ptpayor_template field Not found $query ");}
                        send_alert($remote_filename,"TemplateError $ptpayor_template Field Not found $fquery");
                        return "NoTemplate $ptpayor_template field Not Found";
                }
                $frow = mysql_fetch_array($fresult);
                $tablename = $frow["tablename"];
                $field_arr["$fieldname"] = $frow["uitype"];
		if ($header == 'StartDt') {
			$insurance_start_date_field = $fieldname;
		}

        }

        $fquery = "select columnname ,uitype from vtiger_field where fieldlabel='ReportDt' and tabid='4' limit 1";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: ReprotDt field not found $fquery");}
                send_alert($remote_filename,"Import Date Field Not Found ");
                return "Import Date field Not Found";
        }
        $frow = mysql_fetch_array($fresult);
        $import_field_name = $frow["columnname"];
	

        //login to server
        $loginurl = $webapiurl."?operation=getchallenge&username=$username";
        list($status,$response) = get_data($remote_filename,$loginurl);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $challengeToken = $loginresponse['result']['token'];

        $generatedKey = md5($challengeToken.$secretkey);
        $params = array('operation'=>'login', 'username'=>$username,'accessKey'=>$generatedKey);

        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }

        $jsonResponse = json_decode($response,true);
        $sessionid = $jsonResponse['result']['sessionName'];
        $userid = $jsonResponse['result']['userId'];


        $fp = fopen("$file_csv", "r");
        if(!$fp) {
                if ($DEBUG)  { write_log("$today $remote_filename :: File Open Fail $file_csv  ");}
                send_alert($file_csv,"FailedToOpenFile");
                return "FailedToOpenFile $file_csv";
        }
        if ($DEBUG)  { write_log("$today $remote_filename :: File Opened $file_csv  ");}
        $firstloop = 1;
        $header_index = array();
        $doctor_firstname_index ="";
        $doctor_lastname_index ="";
        $total=0;
        $new = 0;
        $error = 0;
        $merged = 0;


        while (($data = fgetcsv($fp, 3000, ",")) !== FALSE) {
                if($firstloop){
                        $firstloop = 0;
                        foreach ($header_arr as $key => $value) {
                                foreach ($data as $d => $v) {
                                        if ("$v" == "ReportDt"){
                                                $header_index["ReportDt"] = $d;
                                        }
                                        if ("$v" == "PayorKey"){
                                                $header_index["PayorKey"] = $d;
                                        }
		
                                        if ("$key" == "$v"){
                                                $header_index["$key"] = $d;
                                                if ($DEBUG)  { write_log("$today $remote_filename :: $key = $d ");}
                                        }
                                }
                        }
                        continue;
                }

                $total++;
                $err = 0;
                $pt_key = $header_index["PtKey"];
                $pt_key = $data["$pt_key"];
                $pt_key = trim($pt_key);
                $table_field = $header_arr["PtKey"];
                $last_update = $header_index["ReportDt"];
                $last_update = $data["$last_update"];
                $start_date = $header_index["StartDt"];
                $start_date = $data["$start_date"];

                $payor_key = $header_index["PayorKey"];
                $payor_key = $data["$payor_key"];
                $payor_key = trim($payor_key);


                $lastupdate_datetime = $last_update;
		if (!$first_import_patient_insurance){
                $status = check_import_required($lastupdate_datetime);
                if($status){
                        if ($DEBUG)  { write_log("$today $remote_filename :: Skiping record $pt_key Last Update  older 15 days $lastupdate_datetime  ");}
                        continue;
                }
		}


		if($pt_key == '' or $pt_key == ' '){
			continue;
		}

                $aquery = "select a.crmid,b.$import_field_name,b.$insurance_start_date_field from vtiger_crmentity as a inner join vtiger_contactscf as b on a.crmid=b.contactid where a.deleted='0' and b.$table_field ='$pt_key'";
                if ($DEBUG)  { write_log("$today $remote_filename :: Search Patient $pt_key === $aquery  ");}
                $aresult = mysql_query($aquery,$link);
                $qstatus = mysql_affected_rows($link);
                if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
                if ($qstatus < 0) {
                        $err = 1;
                        $reason = mysql_error($link);
                        write_log("$today $remote_filename :: Query Error $reason ");
                }

                $modulename = 'Contacts';
                if (!mysql_num_rows($aresult)){
                        if ($DEBUG)  { write_log("$today $remote_filename :: Patient Not Found  $pt_key  ");}
                        $error++;
                } else {
                        $row = mysql_fetch_row($aresult);
                        $crm_id = $row[0];
                        $modified_time = $row[1];
			$crm_start_date = $row[2];
                        if ($DEBUG)  { write_log("$today $remote_filename :: Record Found CRMID $crm_id  $modified_time CRM: $crm_start_date CSV Start Date: $start_date");}

                        $status = update_required($crm_start_date,$start_date);
                        if($status){
                        	$status = update_required($modified_time,$lastupdate_datetime);
	                        if($status){
        	                        if ($DEBUG)  { write_log("$today $remote_filename :: Skiping record $crm_id Modifed $modified_time csv $lastupdate_datetime  ");}
                	                continue;
                        	}
			}

                        if ($DEBUG)  { write_log("$today $remote_filename :: Record Found CRMID $crm_id  ");}



	                $fquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_insurance as b on a.crmid=b.insuranceid where b.payorkey='$payor_key' and a.deleted='0'";
        	        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
                	$fresult = mysql_query($fquery,$link);
	                $qstatus = mysql_affected_rows($link);
        	        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
                	if(!mysql_num_rows($fresult)){
	                        if ($DEBUG)  { write_log("$today $remote_filename :: Payor $payor_key not found ");}
        	                send_alert($remote_filename,"$today $remote_filename  Payor Not Found $payor_key");
				$error++;
                	        continue;
	                }
			$frow = mysql_fetch_array($fresult);
		        $payor_crm_id = $frow["crmid"];
			$data_link = $crm_link."?module=Insurance&view=Detail&record=".$payor_crm_id;
	

                        $retrieveid = "12x".$crm_id;
                        $url = $webapiurl."?sessionName=$sessionid&operation=retrieve&id=$retrieveid";

                        list($status,$response) = get_data($remote_filename,$url);
                        if ($status != 1 ){
                                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id  ");}
                                $error++;
                                continue;
                        }
                        $ret_response = json_decode($response,true);
                        if ($ret_response["success"] != 1) {
                                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id");}
                                $error++;
                                continue;
                        }

                        $retrievedobject = $ret_response['result'];

                        foreach ($header_index as $key => $value) {
                                if ($key == 'PtKey') {
                                        continue;
                                }
                                $data_value = $data["$value"];
                                $crm_field = $header_arr["$key"];
                                $field_type = $field_arr["$crm_field"];
                                if ($field_type == '5' or $field_type == '23') {
                                        if ($data_value == ''){
                                                $data_value = '0000-00-00';
                                        } else {
                                                $new_value = format_date($data_value);
                                                if ($new_value == '1969-12-31') {
                                                        $data_value = '0000-00-00';
                                                } else {
                                                        $data_value = $new_value;
                                                }
                                        }
                                }

                                $retrievedobject["$crm_field"] = mysql_real_escape_string($data_value);
                        }
                        if ($lastupdate_datetime == '') {
                                $retrievedobject["$import_field_name"] = get_gmt();
                        } else {
                                $retrievedobject["$import_field_name"] = $lastupdate_datetime;
                        }
			$retrievedobject[$crm_link_field] = $data_link;	
                        $objectjson=json_encode($retrievedobject);
                        $params = array("sessionName"=>$sessionid, "operation"=>'update', "element"=>$objectjson, "elementType"=>$modulename);
                        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
                        if (!$status){
                                $error++;
                                continue;
                        }

                        $jsonresponse = json_decode($response,true);

                        if ($jsonresponse["success"] == 1){
                                $merged++;
	                        $fquery = "delete from vtiger_crmentityrel where crmid='$crm_id' and relmodule='Insurance'";
	                        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        	                $fresult = mysql_query($fquery,$link);
                	        $qstatus = mysql_affected_rows($link);
        	                if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
				$fquery = "insert into vtiger_crmentityrel values  ('$crm_id','Contacts','$payor_crm_id','Insurance')";
	                        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        	                $fresult = mysql_query($fquery,$link);
                	        $qstatus = mysql_affected_rows($link);
        	                if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}


                        } else {
                                $error++;
                        }
                }

        }
        if ($DEBUG)  { write_log("$today $remote_filename :: Import Total = $total , New = $new , Merged = $merged , Error = $error ");}
        $url = $webapiurl."?operation=logout&sessionName=$sessionid";
        list($status,$response) = get_data($remote_filename,$url);

        $status = "Imported";
        write_import_status($remote_filename,$total,$new,$merged,$error,$status);

        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }

        return "Success";

}

function process_insurance_details (){

        global $today;
        global $extractpath;
        global $link;
        global $insurnace_details_template;
        global $logfile;
        global $DEBUG;
        global $webapiurl;
        global $username;
        global $secretkey;
        global $first_import_patient_insurance_details;
	global $patient_pt_field;	

        $file_suffix = "PtPayor";
        $remote_filename = $file_suffix."_".$today.".zip";
        $local_filename = $extractpath."/".$remote_filename;
        $file_csv = $extractpath."/".$file_suffix.".csv";
/*
        if ($DEBUG)  { write_log("$today $remote_filename Getting File");}
        $response = sftp_get_file($remote_filename,$local_filename);
        if ($DEBUG)  { write_log("$today $remote_filename sftp $response ");}

        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }
        $response = unzip_file($local_filename,$extractpath);


        if ($DEBUG)  { write_log("$today $remote_filename unzip $response ");}
        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }
*/
        $mappingarr = explode('&',$insurnace_details_template);

        $field_arr = array();
        $header_arr = array();


        $fquery = "select tabid from vtiger_tab where name ='InsuranceDetails'";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: RetrieveId field not found $fquery");}
                send_alert($remote_filename,"RetrieveId Not Found in vtiger_ws_entity ");
                return "RetrieveId Not Found in vtiger_ws_entity";
        }
        $frow = mysql_fetch_array($fresult);
        $tabid = $frow["tabid"];



        foreach ($mappingarr as $value) {
                $fields = explode('=',$value);
                $header = $fields[0];
                $fieldname = $fields[1];
                $header_arr["$header"] = $fieldname;
                $fquery = "select tablename,uitype from vtiger_field where columnname='$fieldname' and tabid='$tabid'";
                if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
                $fresult = mysql_query($fquery,$link);
                $qstatus = mysql_affected_rows($link);
                if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
                if(!mysql_num_rows($fresult)){
                        if ($DEBUG)  { write_log("$today $remote_filename :: Template Error $ptpayor_template field Not found $query ");}
                        send_alert($remote_filename,"TemplateError $ptpayor_template Field Not found $fquery");
                        return "NoTemplate $ptpayor_template field Not Found";
                }
                $frow = mysql_fetch_array($fresult);
                $tablename = $frow["tablename"];
                $field_arr["$fieldname"] = $frow["uitype"];
                if ($header == 'StartDt') {
                        $insurance_start_date_field = $fieldname;
                }

        }

        $fquery = "select columnname ,uitype from vtiger_field where fieldlabel='ReportDt' and tablename='vtiger_insurancedetails' limit 1";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: Import Date field not found $fquery");}
                send_alert($remote_filename,"Import Date Field Not Found ");
                return "Import Date field Not Found";
        }
        $frow = mysql_fetch_array($fresult);
        $import_field_name = $frow["columnname"];

        $fquery = "select id from vtiger_ws_entity where name ='InsuranceDetails'";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: RetrieveId field not found $fquery");}
                send_alert($remote_filename,"RetrieveId Not Found in vtiger_ws_entity ");
                return "RetrieveId Not Found in vtiger_ws_entity";
        }
        $frow = mysql_fetch_array($fresult);
        $retrieve_id_prefix = $frow["id"];



        $fquery = "select columnname ,uitype from vtiger_field where fieldlabel='ReportDt' and tabid='$tabid' limit 1";
        if ($DEBUG)  { write_log("$today $remote_filename :: $fquery  ");}
        $fresult = mysql_query($fquery,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
        if(!mysql_num_rows($fresult)){
                if ($DEBUG)  { write_log("$today $remote_filename :: ReprotDt field not found $fquery");}
                send_alert($remote_filename,"Import Date Field Not Found ");
                return "Import Date field Not Found";
        }
        $frow = mysql_fetch_array($fresult);
        $import_field_name = $frow["columnname"];


        //login to server
        $loginurl = $webapiurl."?operation=getchallenge&username=$username";
        list($status,$response) = get_data($remote_filename,$loginurl);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $challengeToken = $loginresponse['result']['token'];

        $generatedKey = md5($challengeToken.$secretkey);
        $params = array('operation'=>'login', 'username'=>$username,'accessKey'=>$generatedKey);

        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$today $remote_filename :: unable to login user $username  ");}
                send_alert($remote_filename,"Unable to Login user $username ");
                return "UnableTOLogin $remote_filename";
        }

        $jsonResponse = json_decode($response,true);
        $sessionid = $jsonResponse['result']['sessionName'];
        $userid = $jsonResponse['result']['userId'];


        $fp = fopen("$file_csv", "r");
        if(!$fp) {
                if ($DEBUG)  { write_log("$today $remote_filename :: File Open Fail $file_csv  ");}
                send_alert($file_csv,"FailedToOpenFile");
                return "FailedToOpenFile $file_csv";
        }
        if ($DEBUG)  { write_log("$today $remote_filename :: File Opened $file_csv  ");}
        $firstloop = 1;
        $header_index = array();
        $doctor_firstname_index ="";
        $doctor_lastname_index ="";
        $total=0;
        $new = 0;
        $error = 0;
        $merged = 0;


        while (($data = fgetcsv($fp, 3000, ",")) !== FALSE) {
                if($firstloop){
                        $firstloop = 0;
                        foreach ($header_arr as $key => $value) {
                                foreach ($data as $d => $v) {
                                        if ("$v" == "ReportDt"){
                                                $header_index["ReportDt"] = $d;
                                        }
                                        if ("$v" == "PayorKey"){
                                                $header_index["PayorKey"] = $d;
                                        }
                                        if ("$key" == "$v"){
                                                $header_index["$key"] = $d;
                                                if ($DEBUG)  { write_log("$today $remote_filename :: $key = $d ");}
                                        }
                                }
                        }
                        continue;
                }

                $total++;
                $err = 0;
                $pt_key = $header_index["PtKey"];
                $pt_key = $data["$pt_key"];
                $pt_key = trim($pt_key);
                $last_update = $header_index["ReportDt"];
                $last_update = $data["$last_update"];
                $start_date = $header_index["StartDt"];
                $start_date = $data["$start_date"];

                $payor_key = $header_index["PayorKey"];
                $payor_key = $data["$payor_key"];
                $payor_key = trim($payor_key);

                $ptpayor_key = $header_index["PtPayorKey"];
                $ptpayor_key = $data["$ptpayor_key"];
                $ptpayor_key = trim($ptpayor_key);
                $table_field = $header_arr["PtPayorKey"];



                $lastupdate_datetime = $last_update;
                if (!$first_import_patient_insurance_details){
                $status = check_import_required($lastupdate_datetime);
                if($status){
                        if ($DEBUG)  { write_log("$today $remote_filename :: Skiping record $pt_key Last Update  older 15 days $lastupdate_datetime  ");}
                        continue;
                }
                }


                if($ptpayor_key == '' or $ptpayor_key == ' '){
                        continue;
                }
                $aquery = "select a.crmid,b.$import_field_name from vtiger_crmentity as a inner join vtiger_insurancedetails as b on a.crmid=b.insurancedetailsid where a.deleted='0' and b.$table_field ='$ptpayor_key'";
                if ($DEBUG)  { write_log("$today $remote_filename :: Search InsuranceDetails $ptpayor_key === $aquery  ");}
                $aresult = mysql_query($aquery,$link);
                $qstatus = mysql_affected_rows($link);
                if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $qstatus ");}
                if ($qstatus < 0) {
                        $err = 1;
                        $reason = mysql_error($link);
                        write_log("$today $remote_filename :: Query Error $reason ");
                }

                $modulename = 'InsuranceDetails';
                if (!mysql_num_rows($aresult)){
                        if ($DEBUG)  { write_log("$today $remote_filename :: InsuranceDetails Found Adding New $ptpayor_key  ");}

			$dquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_insurance as b on a.crmid=b.insuranceid where b.payorkey='$payor_key' and a.deleted='0'";
			if ($DEBUG)  { write_log("$today $remote_filename :: Search Payor $payor_key === $dquery  ");}	
			$dresult = mysql_query($dquery,$link);
			$dstatus = mysql_affected_rows($link);	
			if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $dstatus ");}
			if (!mysql_num_rows($dresult)){
				write_log("$today $remote_filename :: Payor Not found $dquery ");
                                send_alert($remote_filename,"$today $remote_filename  Payor Not Found $payor_key");
				$error++;	
				continue;
			}
			$drow = mysql_fetch_array($dresult);
			$payor_id = $drow['crmid'];


                        $dquery = "select a.crmid from vtiger_crmentity as a inner join vtiger_contactscf as b on a.crmid=b.contactid where b.$patient_pt_field='$pt_key' and a.deleted='0'";
                        if ($DEBUG)  { write_log("$today $remote_filename :: Search Patient $pt_key === $dquery  ");}
                        $dresult = mysql_query($dquery,$link);
                        $dstatus = mysql_affected_rows($link);
                        if ($DEBUG)  { write_log("$today $remote_filename :: Query Executed Result $dstatus ");}
                        if (!mysql_num_rows($dresult)){
                                write_log("$today $remote_filename :: Pt_key Not found $dquery ");
                                send_alert($remote_filename,"$today $remote_filename  Pt_key Not Found $pt_key");
                                $error++;
                                continue;
                        }
                        $drow = mysql_fetch_array($dresult);
                        $customer_id = $drow['crmid'];

                        $contactdata = array('ptpayorkey'=>$ptpayor_key,'assigned_user_id'=>$userid);
                        foreach ($header_index as $key => $value) {
                                $data_value = $data["$value"];
                                $crm_field = $header_arr["$key"];
                                $field_type = $field_arr["$crm_field"];
                                if ($field_type == '5' or $field_type == '23') {
                                        if ($data_value == ''){
                                                $data_value = '0000-00-00';
                                        } else {
                                                $new_value = format_date($data_value);
                                                if ($new_value == '1969-12-31') {
                                                        $data_value = '0000-00-00';
                                                } else {
                                                        $data_value = $new_value;
                                                }
                                        }
                                }

                                $set_field = $crm_field."='".mysql_real_escape_string($data_value)."'";
                                $contactdata["$crm_field"] = mysql_real_escape_string($data_value);
                        }
                        if ($lastupdate_datetime == '') {
                                $contactdata["$import_field_name"] = get_gmt();
                        } else {
                                $contactdata["$import_field_name"] = $lastupdate_datetime;
                        }
			$contactdata ['ptpayorkey'] = $ptpayor_key;
			$contactdata ['payor'] = $payor_id;
			$contactdata ['customer'] = $customer_id;
                        $objectjson=json_encode($contactdata);
			print_r($contactdata);
                        $params = array("sessionName"=>$sessionid, "operation"=>'create', "element"=>$objectjson, "elementType"=>$modulename);
			print_r($params);
                        list($status,$response) = post_data($remote_filename,$webapiurl,$params);

                        if (!$status){
                                $error++;
                                continue;
                        }

                        $jsonresponse = json_decode($response,true);
				print_r($jsonresponse);

                        if ($jsonresponse["success"] == 1){
                                $new++;

                        } else {
                                $error++;
                        }
                                         		

                } else {
                        $row = mysql_fetch_row($aresult);
                        $crm_id = $row[0];
                        $modified_time = $row[1];
                        if ($DEBUG)  { write_log("$today $remote_filename :: Record Found CRMID $crm_id  $modified_time ");}

                                $status = update_required($modified_time,$lastupdate_datetime);
                                if($status){
                                        if ($DEBUG)  { write_log("$today $remote_filename :: Skiping record $crm_id Modifed $modified_time csv $lastupdate_datetime  ");}
                                        continue;
                                }


			$retrieveid = $retrieve_id_prefix."x".$crm_id;	
                        $url = $webapiurl."?sessionName=$sessionid&operation=retrieve&id=$retrieveid";

                        list($status,$response) = get_data($remote_filename,$url);
                        if ($status != 1 ){
                                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id  ");}
                                $error++;
                                continue;
                        }
                        $ret_response = json_decode($response,true);
                        if ($ret_response["success"] != 1) {
                                if ($DEBUG)  { write_log("$today $remote_filename :: unable to retrieve data $crm_id");}
                                $error++;
                                continue;
                        }

                        $retrievedobject = $ret_response['result'];

                        foreach ($header_index as $key => $value) {
                                if ($key == 'PtKey' or $key == 'PtPayorKey' or $key == 'PayorKey') {
                                        continue;
                                }
                                $data_value = $data["$value"];
                                $crm_field = $header_arr["$key"];
                                $field_type = $field_arr["$crm_field"];
                                if ($field_type == '5' or $field_type == '23') {
                                        if ($data_value == ''){
                                                $data_value = '0000-00-00';
                                        } else {
                                                $new_value = format_date($data_value);
                                                if ($new_value == '1969-12-31') {
                                                        $data_value = '0000-00-00';
                                                } else {
                                                        $data_value = $new_value;
                                                }
                                        }
                                }

                                $retrievedobject["$crm_field"] = mysql_real_escape_string($data_value);
                        }
                        if ($lastupdate_datetime == '') {
                                $retrievedobject["$import_field_name"] = get_gmt();
                        } else {
                                $retrievedobject["$import_field_name"] = $lastupdate_datetime;
                        }
                        $retrievedobject[$crm_link_field] = $data_link;
                        $objectjson=json_encode($retrievedobject);
                        $params = array("sessionName"=>$sessionid, "operation"=>'update', "element"=>$objectjson, "elementType"=>$modulename);
                        list($status,$response) = post_data($remote_filename,$webapiurl,$params);
                        if (!$status){
                                $error++;
                                continue;
                        }

                        $jsonresponse = json_decode($response,true);

                        if ($jsonresponse["success"] == 1){
                                $merged++;
                        } else {
                                $error++;
                        }
                }

        }
        if ($DEBUG)  { write_log("$today $remote_filename :: Import Total = $total , New = $new , Merged = $merged , Error = $error ");}
        $url = $webapiurl."?operation=logout&sessionName=$sessionid";
        list($status,$response) = get_data($remote_filename,$url);

        $status = "Imported";
        write_import_status($remote_filename,$total,$new,$merged,$error,$status);

        if ($response != "Success"){
                send_alert($remote_filename,$response);
                return "$response";
        }

        return "Success";

}



function write_log ($log) {
	global $logfile;
	$fp = fopen($logfile,'a+');
	fwrite($fp,$log."\n");
	fclose($fp);
}

function post_data ($remote_filename,$endpointUrl,$params) {

	global $today;
	global $DEBUG;
	global $username;
	global $password;

	if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	$retValue = curl_exec($ch);
	$status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
	if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);	
	

}


function get_data ($remote_filename,$endpointUrl) {

        global $today;
        global $DEBUG;
	global $username;
	global $password;

        if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");	
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $retValue = curl_exec($ch);
        $status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
        if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);


}

function format_date ($old_date) {

	$newDate = date("Y-m-d", strtotime($old_date));	
	return ($newDate);

}

function get_gmt() {
	$newDate = gmdate("Y-m-d", time());
	return ($newDate);
}

function update_required ($modified_time,$lastupdate_datetime) {

	global $DEBUG;
	global $today;
	if ($lastupdate_datetime == '' or $lastupdate_datetime == null){
		return 0;
	}
	$modified_timestamp = strtotime($modified_time);
	$lastupdate_datetime_timestamp = strtotime($lastupdate_datetime);
	if ($DEBUG)  { write_log("$today  ::  Modified $modified_time $modified_timestamp csv $lastupdate_datetime $lastupdate_datetime_timestamp ");}
	$status = 0;
	if ($modified_timestamp >= $lastupdate_datetime_timestamp) {
		$status = 1;
	}
	return $status;	

}



function check_import_required($lastupdate_datetime) {

	global $DEBUG;
	global $today;
	if ($lastupdate_datetime == '' or $lastupdate_datetime == null){
		if ($DEBUG)  { write_log("$today  ::  LastUpdateDT is blank $lastupdate_datetime ");}
                return 0;
        }

	$lastupdate_date = date("Y-m-d",strtotime($lastupdate_datetime));
	if(strtotime($lastupdate_date) <= strtotime('-15 days')) {
       		return 1;
	} else {
        	return 0;
	}
	
}


include('import_config.php');
include($crmpath."conn.php");
ini_set('include_path', "$basepath");

require_once("Net/SFTP.php");
error_reporting(E_ALL & ~E_WARNING);
echo "Starting\n";

$today = date('m-d-y');
$today = "12-08-16";
//$today = "03-14-16";
$extractpath = $basepath."".$today;
if (!file_exists($extractpath)) {
	mkdir($extractpath,0777,true);
}

//$response = Insurance_import();
//$response = process_ptpayor();
$response = process_insurance_details();

echo "Finished\n";
?>
