<?php

include_once 'modules/Vtiger/CRMEntity.php';

class InsuranceDetails extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_insurancedetails';
        var $table_index= 'insurancedetailsid';

        var $customFieldTable = Array('vtiger_insurancedetailscf', 'insurancedetailsid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_insurancedetails', 'vtiger_insurancedetailscf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_insurancedetails' => 'insurancedetailsid',
                'vtiger_insurancedetailscf'=>'insurancedetailsid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'PtPayorKey' => Array('insurancedetails', 'ptpayorkey'),
//                'Payor' => Array('insurancedetails', 'payor'),
//                'Customer' => Array('insurancedetails', 'customer'),
                'PolicyNbr' => Array('insurancedetails', 'policynbr'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'PtPayorKey' => 'ptpayorkey',
//                'Payor' => 'payor',
//                'Customer' => 'customer',
                'PolicyNbr' => 'policynbr',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'ptpayorkey';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'PtPayorKey' => 'ptpayorkey',
//                'Payor' => 'payor',
//                'Customer' => 'customer',
                'PolicyNbr' => 'policynbr',
                'PayorName' => Array('insurance', 'payorname'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'PtPayorKey' => 'ptpayorkey',
//                'Payor' => 'payor',
//                'Customer' => 'customer',
                'PolicyNbr' => 'policynbr',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('ptpayorkey');

        // For Alphabetical search
        var $def_basicsearch_col = 'ptpayorkey';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'ptpayorkey';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('ptpayorkey','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
