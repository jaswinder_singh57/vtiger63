<?php

include_once 'modules/Vtiger/CRMEntity.php';

class CDR extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_cdr';
        var $table_index= 'cdrid';

        var $customFieldTable = Array('vtiger_cdrcf', 'cdrid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_cdr', 'vtiger_cdrcf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_cdr' => 'cdrid',
                'vtiger_cdrcf'=>'cdrid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'CallDate' => Array('cdr', 'calldate'),
                'Call From' => Array('cdr', 'callfrom'),
                'Destination' => Array('cdr', 'destination'),
                'Customer' => Array('cdr', 'customer'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'CallDate' => 'calldate',
                'CallFrom' => 'callfrom',
                'Destination' => 'destination',
                'Customer' => 'customer',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'calldate';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'CallDate' => 'calldate',
                'CallFrom' => 'callfrom',
                'Destination' => 'destination',
                'Customer' => 'customer',
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'CallDate' => 'calldate',
                'CallFrom' => 'callfrom',
                'Destination' => 'destination',
                'Customer' => 'customer',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('calldate');

        // For Alphabetical search
        var $def_basicsearch_col = 'calldate';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'calldate';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('calldate','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
