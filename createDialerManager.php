<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'DialerManager';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
if ($moduleInstance || file_exists('modules/'.$MODULENAME)) {
        echo "Module already present - choose a different name.";
} else {
        $moduleInstance = new Vtiger_Module();
        $moduleInstance->name = $MODULENAME;
        $moduleInstance->parent= 'Tools';
        $moduleInstance->save();

        // Schema Setup
        $moduleInstance->initTables();

        // Field Setup
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);

        $blockcf = new Vtiger_Block();
        $blockcf->label = 'LBL_CUSTOM_INFORMATION';
        $moduleInstance->addBlock($blockcf);

	$field1 = new Vtiger_Field();
	$field1->name = 'campaignid_selected';
	$field1->table = $module->basetable;
	$field1->column = 'campaignid_selected';
	$field1->label = 'Campaign Selected';
	$field1->columntype = 'VARCHAR(255)';
	$field1->uitype = 2;
	$field1->typeofdata = 'V~M';
	$block->addField($field1);
	$moduleInstance->setEntityIdentifier($field1);

	
	$field2 = new Vtiger_Field();
	$field2->name = 'customer';
	$field2->label= 'Customer';
	$field2->table = 'vtiger_dialermanager';
	$field2->column = 'customer';
	$field2->columntype = 'VARCHAR(100)';
	$field2->uitype = 10;
	$field2->typeofdata = 'V~O';
	$block->addField($field2);
	$field2->setRelatedModules(Array('Contacts','Accounts'));

        $field3  = new Vtiger_Field();
        $field3->name = 'status';
        $field3->label= 'Status';
        $field3->uitype= 2;
        $field3->column = $field3->name;
        $field3->columntype = 'VARCHAR(255)';
        $field3->typeofdata = 'V~M';
        $block->addField($field3);

        $field4  = new Vtiger_Field();
        $field4->name = 'leadid';
        $field4->label= 'VicidialLeadID';
        $field4->uitype= 2;
        $field4->column = $field4->name;
        $field4->columntype = 'VARCHAR(255)';
        $field4->typeofdata = 'V~O';
        $block->addField($field4);


        // Recommended common fields every Entity module should have (linked to core table)
        $mfield1 = new Vtiger_Field();
        $mfield1->name = 'assigned_user_id';
        $mfield1->label = 'Assigned To';
        $mfield1->table = 'vtiger_crmentity';
        $mfield1->column = 'smownerid';
        $mfield1->uitype = 53;
        $mfield1->typeofdata = 'V~M';
        $block->addField($mfield1);

        $mfield2 = new Vtiger_Field();
        $mfield2->name = 'CreatedTime';
        $mfield2->label= 'Created Time';
        $mfield2->table = 'vtiger_crmentity';
        $mfield2->column = 'createdtime';
        $mfield2->uitype = 70;
        $mfield2->typeofdata = 'T~O';
        $mfield2->displaytype= 2;
        $block->addField($mfield2);

        $mfield3 = new Vtiger_Field();
        $mfield3->name = 'ModifiedTime';
        $mfield3->label= 'Modified Time';
        $mfield3->table = 'vtiger_crmentity';
        $mfield3->column = 'modifiedtime';
        $mfield3->uitype = 70;
        $mfield3->typeofdata = 'T~O';
        $mfield3->displaytype= 2;
        $block->addField($mfield3);

        // Filter Setup
        $filter1 = new Vtiger_Filter();
        $filter1->name = 'All';
        $filter1->isdefault = true;
        $moduleInstance->addFilter($filter1);
        $filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($mfield1, 3)->addField($mfield2, 4);

        // Sharing Access Setup
        $moduleInstance->setDefaultSharing('Public');

        // Webservice Setup
        $moduleInstance->initWebservice();

        mkdir('modules/'.$MODULENAME);
        echo "OK\n";
        $moduleInstance = Vtiger_Module::getInstance('Contacts');
        $accountsModule = Vtiger_Module::getInstance('DialerManager');
        $account = Vtiger_Module::getInstance('Accounts');
        $relationLabel = 'DialerManager';
        $moduleInstance->setRelatedList(
                $accountsModule, $relationLabel, Array('ADD'),'get_dependents_list'
        );
        $account->setRelatedList(
                $accountsModule, $relationLabel, Array('ADD'),'get_dependents_list'
        );
	
	echo "Relation Done";
}
