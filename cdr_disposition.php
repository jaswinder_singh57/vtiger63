<?php


function get_fieldname($label){
    global $link;
    
    $query = "select columnname from vtiger_field where tablename = 'vtiger_cdrcf' and fieldlabel = '$label'";
    $result = mysql_query($query,$link);
    if (!mysql_num_rows($result)){
        echo "Please Set the field name $label";
        exit;
    }    
    $row = mysql_fetch_assoc($result);
    $fieldname = $row['columnname'];    
    return $fieldname;

}

function search_contact ($phone) {
	
	global $link;
	global $DEBUG;
	global $uniqueid;
        global $exclude_field;
	$module = 'Contacts';
		
	$i = 0;
	$contact_where = "";

	$query = "select tabid from vtiger_tab where name = '$module'";
	$result = mysql_query($query,$link);
	if (!mysql_num_rows($result)){
		return "";
	}
	$row = mysql_fetch_assoc($result);
	$tabid = $row['tabid'];
	

	$query1 = "select fieldname,tablename  from vtiger_field where tabid='$tabid' and uitype='11' order by tablename";
        if ($DEBUG)  { write_log("$uniqueid :: $query1  ");}

	$last_table = '';
	$j = 0;
	$query_array = array();
	$result1 = mysql_query($query1,$link);
	while ($row1 = mysql_fetch_assoc($result1)) {
		$field = $row1['fieldname'];
		$table = $row1['tablename'];
		if ($field == $exclude_field) {
			continue;
		}
		if (!$j){
			$j++;
			$last_table = $table;
		}
		if ($last_table == $table) {
			if (!$i) {
			$i++;
			$contact_where = "$table.$field = '$phone'";
			} else {
			$contact_where .= " or $table.$field = '$phone'";
			}
		} else {
			if ($last_table == 'vtiger_contactdetails') {
				$query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
			} else if ($last_table == 'vtiger_contactsubdetails') {
				$query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactsubdetails ON vtiger_contactsubdetails.contactsubscriptionid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
			} else if ($last_table == 'vtiger_contactscf') {
				$query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactscf ON vtiger_contactscf.contactid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
			} else {
				continue;		
			}
  			if ($DEBUG)  { write_log("$uniqueid :: $query  ");}

			$result = mysql_query($query,$link);

		        if (mysql_num_rows($result)){
		                $row = mysql_fetch_assoc($result);
		                $crmid = $row['crmid'];
		                $crmlink = $crmid;
				return $crmlink;
       			}	
			$last_table = $table;
                        $contact_where = "$table.$field = '$phone'";
			
		}	

	}


        if ($last_table == 'vtiger_contactdetails') {
	        $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_contactsubdetails') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactsubdetails ON vtiger_contactsubdetails.contactsubscriptionid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_contactscf') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactscf ON vtiger_contactscf.contactid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else {
                continue;
        }
        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}
        $result = mysql_query($query,$link);

        if (mysql_num_rows($result)){
        	$row = mysql_fetch_assoc($result);
                $crmid = $row['crmid'];
                $crmlink = $crmid;
                return $crmlink;
        } else {
		return "";
	}

}

function search_lead ($phone) {

        global $link;
        global $DEBUG;
        global $uniqueid;
	$module = 'Leads';
	
        $i = 0;
        $contact_where = "";

        $query = "select tabid from vtiger_tab where name = '$module'";
        $result = mysql_query($query,$link);
        if (!mysql_num_rows($result)){
                return "";
        }
        $row = mysql_fetch_assoc($result);
        $tabid = $row['tabid'];

        $query1 = "select fieldname,tablename  from vtiger_field where tabid='$tabid' and uitype='11' order by tablename";
        if ($DEBUG)  { write_log("$uniqueid :: $query1  ");}

        $last_table = '';
        $j = 0;
        $query_array = array();
        $result1 = mysql_query($query1,$link);
        while ($row1 = mysql_fetch_assoc($result1)) {
                $field = $row1['fieldname'];
                $table = $row1['tablename'];
                if ($field == $exclude_field) {
                        continue;
                }
                if (!$j){
                        $j++;
                        $last_table = $table;
                }
                if ($last_table == $table) {
                        if (!$i) {
                        $i++;
                        $contact_where = "$table.$field = '$phone'";
                        } else {
                        $contact_where .= " or $table.$field = '$phone'";
                        }
                } else {
                        if ($last_table == 'vtiger_leaddetails') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_leaddetails ON vtiger_leaddetails.leadid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else if ($last_table == 'vtiger_leadsubdetails') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_leadsubdetails ON vtiger_leadsubdetails.leadsubscriptionid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else if ($last_table == 'vtiger_leadscf') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_leadscf ON vtiger_leadscf.leadid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else {
                                continue;
                        }
                        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}

                        $result = mysql_query($query,$link);

                        if (mysql_num_rows($result)){
                                $row = mysql_fetch_assoc($result);
                                $crmid = $row['crmid'];
                                $crmlink = $crmid;
                                return $crmlink;
                        }
                        $last_table = $table;
                        $contact_where = "$table.$field = '$phone'";

                }

        }


        if ($last_table == 'vtiger_leaddetails') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_leaddetails ON vtiger_leaddetails.leadid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_leadsubdetails') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_leadsubdetails ON vtiger_leadsubdetails.leadsubscriptionid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_leadscf') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_leadscf ON vtiger_leadscf.leadid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else {
                continue;
        }
        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}
        $result = mysql_query($query,$link);

        if (mysql_num_rows($result)){
                $row = mysql_fetch_assoc($result);
                $crmid = $row['crmid'];
                $crmlink = $crmid;
                return $crmlink;
        } else {
		return "";
	}
		

}

function search_accounts ($phone) {

        global $link;
        global $DEBUG;
        global $uniqueid;
	$module = 'Accounts';

        $i = 0;
        $contact_where = "";

        $query = "select tabid from vtiger_tab where name = '$module'";
        $result = mysql_query($query,$link);
        if (!mysql_num_rows($result)){
                return "";
        }
        $row = mysql_fetch_assoc($result);
        $tabid = $row['tabid'];


        $query1 = "select fieldname,tablename  from vtiger_field where tabid='$tabid' and uitype='11' order by tablename";
        if ($DEBUG)  { write_log("$uniqueid :: $query1  ");}

        $last_table = '';
        $j = 0;
        $query_array = array();
        $result1 = mysql_query($query1,$link);
        while ($row1 = mysql_fetch_assoc($result1)) {
                $field = $row1['fieldname'];
                $table = $row1['tablename'];
                if ($field == $exclude_field) {
                        continue;
                }
                if (!$j){
                        $j++;
                        $last_table = $table;
                }
                if ($last_table == $table) {
                        if (!$i) {
                        $i++;
                        $contact_where = "$table.$field = '$phone'";
                        } else {
                        $contact_where .= " or $table.$field = '$phone'";
                        }
                } else {
                        if ($last_table == 'vtiger_account') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else if ($last_table == 'vtiger_accountscf') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else {
                                continue;
                        }
                        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}

                        $result = mysql_query($query,$link);

                        if (mysql_num_rows($result)){
                                $row = mysql_fetch_assoc($result);
                                $crmid = $row['crmid'];
                                $crmlink = $crmid;
                                return $crmlink;
                        }
                        $last_table = $table;
                        $contact_where = "$table.$field = '$phone'";

                }

        }


        if ($last_table == 'vtiger_account') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_accountscf') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else {
                continue;
        }
        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}
        $result = mysql_query($query,$link);

        if (mysql_num_rows($result)){
                $row = mysql_fetch_assoc($result);
                $crmid = $row['crmid'];
                $crmlink = $crmid;
                return $crmlink;
        } else {
                return "";
        }

}


function search_insurance ($phone) {

        global $link;
        global $DEBUG;
        global $uniqueid;
        $module = 'Insurance';

        $i = 0;
        $contact_where = "";

        $query = "select tabid from vtiger_tab where name = '$module'";
        $result = mysql_query($query,$link);
        if (!mysql_num_rows($result)){
                return "";
        }
        $row = mysql_fetch_assoc($result);
        $tabid = $row['tabid'];

        $query1 = "select fieldname,tablename  from vtiger_field where tabid='$tabid' and uitype='11' order by tablename";
        if ($DEBUG)  { write_log("$uniqueid :: $query1  ");}

        $last_table = '';
        $j = 0;
        $query_array = array();
        $result1 = mysql_query($query1,$link);
        while ($row1 = mysql_fetch_assoc($result1)) {
                $field = $row1['fieldname'];
                $table = $row1['tablename'];
                if ($field == $exclude_field) {
                        continue;
                }
                if (!$j){
                        $j++;
                        $last_table = $table;
                }
                if ($last_table == $table) {
                        if (!$i) {
                        $i++;
                        $contact_where = "$table.$field = '$phone'";
                        } else {
                        $contact_where .= " or $table.$field = '$phone'";
                        }
                } else {
                        if ($last_table == 'vtiger_insurance') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_insurance ON vtiger_insurance.insuranceid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else if ($last_table == 'vtiger_insurancecf') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_insurancecf ON vtiger_insurancecf.insuranceid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else {
                                continue;
                        }
                        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}

                        $result = mysql_query($query,$link);

                        if (mysql_num_rows($result)){
                                $row = mysql_fetch_assoc($result);
                                $crmid = $row['crmid'];
                                $crmlink = $crmid;
                                return $crmlink;
                        }
                        $last_table = $table;
                        $contact_where = "$table.$field = '$phone'";

                }

        }


        if ($last_table == 'vtiger_insurance') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_insurance ON vtiger_insurance.insuranceid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_insurancecf') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_insurancecf ON vtiger_insurancecf.insuranceid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else {
                continue;
        }
        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}
        $result = mysql_query($query,$link);

        if (mysql_num_rows($result)){
                $row = mysql_fetch_assoc($result);
                $crmid = $row['crmid'];
                $crmlink = $crmid;
                return $crmlink;
        } else {
                return "";
        }

}

function search_vendor ($phone) {

        global $link;
        global $DEBUG;
        global $uniqueid;
        $module = 'Vendors';

        $i = 0;
        $contact_where = "";

        $query = "select tabid from vtiger_tab where name = '$module'";
        $result = mysql_query($query,$link);
        if (!mysql_num_rows($result)){
                return "";
        }
        $row = mysql_fetch_assoc($result);
        $tabid = $row['tabid'];

        $query1 = "select fieldname,tablename  from vtiger_field where tabid='$tabid' and uitype='11' order by tablename";
        if ($DEBUG)  { write_log("$uniqueid :: $query1  ");}

        $last_table = '';
        $j = 0;
        $query_array = array();
        $result1 = mysql_query($query1,$link);
        while ($row1 = mysql_fetch_assoc($result1)) {
                $field = $row1['fieldname'];
                $table = $row1['tablename'];
                if ($field == $exclude_field) {
                        continue;
                }
                if (!$j){
                        $j++;
                        $last_table = $table;
                }
                if ($last_table == $table) {
                        if (!$i) {
                        $i++;
                        $contact_where = "$table.$field = '$phone'";
                        } else {
                        $contact_where .= " or $table.$field = '$phone'";
                        }
                } else {
                        if ($last_table == 'vtiger_vendor') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else if ($last_table == 'vtiger_vendorcf') {
                                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_vendorcf ON vtiger_vendorcf.vendorid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
                        } else {
                                continue;
                        }
                        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}

                        $result = mysql_query($query,$link);

                        if (mysql_num_rows($result)){
                                $row = mysql_fetch_assoc($result);
                                $crmid = $row['crmid'];
                                $crmlink = $crmid;
                                return $crmlink;
                        }
                        $last_table = $table;
                        $contact_where = "$table.$field = '$phone'";

                }

        }


        if ($last_table == 'vtiger_vendor') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else if ($last_table == 'vtiger_vendorcf') {
                $query = "SELECT vtiger_crmentity.crmid as crmid FROM  vtiger_crmentity LEFT JOIN vtiger_vendorcf ON vtiger_vendorcf.vendorid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ( $contact_where ) limit 1";
        } else {
                continue;
        }
        if ($DEBUG)  { write_log("$uniqueid :: $query  ");}
        $result = mysql_query($query,$link);

        if (mysql_num_rows($result)){
                $row = mysql_fetch_assoc($result);
                $crmid = $row['crmid'];
                $crmlink = $crmid;
                return $crmlink;
        } else {
                return "";
        }

}

function search_user ($phone) {

        global $link;
        global $DEBUG;
        global $uniqueid;

        $query = "select id from vtiger_users where phone_crm_extension='$phone' and deleted='0'";
        $result = mysql_query($query,$link);
        if (!mysql_num_rows($result)){
                return "1";
        }
        $row = mysql_fetch_assoc($result);
        $id = $row['id'];
	return $id;

}


function write_log ($log) {
        global $logfile;
        $fp = fopen($logfile,'a+');
        fwrite($fp,$log."\n");
        fclose($fp);
}




if (isset($_REQUEST["exten"])) {
        $callfrom = $_REQUEST["callfrom"];
        $callname = $_REQUEST["callname"];
        $uniqueid = $_REQUEST["uniqueid"];
        $exten = $_REQUEST["exten"];
}

require('conn.php');
require('config_cdr.php');
$phone = substr($callfrom,-10);
$direction = '';





if ($phone == '') {
        $crmlink = $vtiger_url."index.php?module=Contacts&view=List";
//Gurpreet: may10
} elseif ((strlen($callfrom) <= 4 ) and (strlen($exten) <=4 )) {
        $crmlink = $vtiger_url."index.php?module=Contacts&view=List";
//end
} else {
	$module = 'Contacts';
        $crmlink = search_contact($phone);
        if ($crmlink == '') {
		$module = 'Accounts';
                $crmlink = search_accounts($phone);
                if ($crmlink == '') {
			$module = 'Insurance';
                        $crmlink = search_insurance($phone);
                        if ($crmlink == '') {
				$module = 'Vendors';
                                $crmlink = search_vendor($phone);
                        }
                }
        }

	if ($crmlink != '') {
		
		$crmlink_new = $vtiger_url."index.php?module=$module&view=Detail&record=$crmlink";
	} else {
        	$crmlink_new = $vtiger_url."index.php?module=Contacts&view=List";

	}
	$user_id = search_user($exten);
	$query = "insert into cdr_queue(uniqueid,callfrom,exten,userid,create_at,crmid) values ('$uniqueid','$phone','$exten','$user_id',now(),'$crmlink')";
	$result = mysql_query($query,$link);	
	
	if ($DEBUG) {echo "$crmlink_new\n";}

	header("Location: $crmlink_new");			
	exit;

}

header("Location: $crmlink");			


exit;
?>
