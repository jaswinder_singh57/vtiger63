<?php

function check_accounts($sender) {

	global $link;
	global $DEBUG;
	global $today;

	$sql = "select a.crmid from vtiger_crmentity as a inner join vtiger_account as b on a.crmid=b.accountid where a.deleted='0' and (b.phone='$sender' or b.otherphone='$sender' or b.fax='$sender')";
        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
	$result = mysql_query($sql);
	$qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

	if (!mysql_num_rows($result)){
		return array(1,0);
	}
	$row = mysql_fetch_array($result);
	$crmid = '11x'.$row["crmid"];

	return array(0,$crmid);
}


function check_contacts($sender) {

        global $link;
        global $DEBUG;
        global $today;

        $sql = "select a.crmid from vtiger_crmentity as a inner join vtiger_contactdetails as b on a.crmid=b.contactid where a.deleted='0' and (b.phone='$sender' or b.mobile='$sender' or b.fax='$sender')";
        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

        if (!mysql_num_rows($result)){
                return array(1,0);
        }
        $row = mysql_fetch_array($result);
        $crmid = '12x'.$row["crmid"];

        return array(0,$crmid);
}

function check_leads($sender) {

        global $link;
        global $DEBUG;
        global $today;

        $sql = "select a.crmid from vtiger_crmentity as a inner join vtiger_leadaddress as b on a.crmid=b.leadaddressid where a.deleted='0' and (b.phone='$sender' or b.mobile='$sender' or b.fax='$sender')";
        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

        if (!mysql_num_rows($result)){
                return array(1,0);
        }
        $row = mysql_fetch_array($result);
        $crmid = '10x'.$row["crmid"];

        return array(0,$crmid);
}


function import_inboundsms ($relationid,$sender,$receiver,$receiver_name,$datetime,$message) {

        global $today;
        global $link;
        global $logfile;
        global $DEBUG;
        global $webapiurl;
        global $username;
        global $secretkey;
	global $inboundsms_status_field;
        //login to server
        $loginurl = $webapiurl."?operation=getchallenge&username=$username";
        list($status,$response) = get_data($remote_filename,$loginurl);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }
        $challengeToken = $loginresponse['result']['token'];

        $generatedKey = md5($challengeToken.$secretkey);
        $params = array('operation'=>'login', 'username'=>$username,'accessKey'=>$generatedKey);

        list($status,$response) = post_data($sender,$webapiurl,$params);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }

        $jsonResponse = json_decode($response,true);
        $sessionid = $jsonResponse['result']['sessionName'];
        $userid = $jsonResponse['result']['userId'];

	$modulename = "InboundSMS";
	
	$contactdata = array('sender'=>$sender,'receiver'=>$receiver,'receivername'=>$receiver_name,'smsdatetime'=>$datetime,'message'=>$message,'customer'=>$relationid,'assigned_user_id'=>$userid,$inboundsms_status_field => 'New');

        $objectjson=json_encode($contactdata);
        $params = array("sessionName"=>$sessionid, "operation"=>'create', "element"=>$objectjson, "elementType"=>$modulename);
        list($status,$response) = post_data($sender,$webapiurl,$params);
        if (!$status){
                if ($DEBUG)  { write_log("$sender :: unable to import to InboundSMS $response ");}
		return "UnableToImport";
	}
		
        $jsonresponse = json_decode($response,true);

        if ($jsonresponse["success"] == 1){
                if ($DEBUG)  { write_log("$sender :: Import Successfull to InboundSMS  ");}
		return "ImportSuccessfull";
        } else {
                if ($DEBUG)  { write_log("$sender :: Import Unsuccessfull to InboundSMS $response ");}
		return "ImportUnsuccessfull";
        }

}


function write_log ($log) {
	global $logfile;
	$fp = fopen($logfile,'a+');
	fwrite($fp,$log."\n");
	fclose($fp);
}


function post_data ($remote_filename,$endpointUrl,$params) {

	global $today;
	global $DEBUG;
	global $username;
	global $password;

	if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	$retValue = curl_exec($ch);
	$status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
	if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);	
	

}


function get_data ($remote_filename,$endpointUrl) {

        global $today;
        global $DEBUG;
	global $username;
	global $password;

        if ($DEBUG)  { write_log("$today $remote_filename :: URL $endpointUrl");}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");	
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $retValue = curl_exec($ch);
        $status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
        if ($DEBUG)  { write_log("$today $remote_filename :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);


}


require_once("sms_config.php");
require_once("conn.php");

error_reporting(E_ALL & ~E_WARNING);
$today = date('m-d-y');

if (!($_REQUEST["sender"] or $_REQUEST["receiver"] or $_REQUEST["datetime"])) {
	echo "Please share sender,reveiver and datetime details";
	exit;
}

$sender = $_REQUEST["sender"];
$receiver = $_REQUEST["receiver"];
$datetime = $_REQUEST["datetime"];
$message = $_REQUEST["message"];

$sender = substr($sender,-10);
$receiver = substr($receiver,-10);


$receiver_name = '';
if (array_key_exists($receiver, $receiver_array)) {
    $receiver_name = $receiver_array["$receiver"];
}


//check sender phone number in Organization/Doctor database
list($status,$crmid) = check_accounts($sender);

if (!$status){
	$response = import_inboundsms($crmid,$sender,$receiver,$receiver_name,$datetime,$message);
	echo "$response";
	exit;
}

//check sender phone number in Contacts/Patient database
list($status,$crmid) = check_contacts($sender);

if (!$status){
	$response = import_inboundsms($crmid,$sender,$receiver,$receiver_name,$datetime,$message);
	echo "$response";
	exit;
}

//check sender phone number in Leads database
list($status,$crmid) = check_leads($sender);

if (!$status){
	$response = import_inboundsms($crmid,$sender,$receiver,$receiver_name,$datetime,$message);
	echo "$response";
	exit;
}

$response = import_inboundsms('',$sender,$receiver,$receiver_name,$datetime,$message);
echo "$response";
exit;

?>
