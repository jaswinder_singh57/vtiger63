<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'Insurance';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
if ($moduleInstance || file_exists('modules/'.$MODULENAME)) {
        echo "Module already present - choose a different name.";
} else {
        $moduleInstance = new Vtiger_Module();
        $moduleInstance->name = $MODULENAME;
        $moduleInstance->parent= 'Tools';
        $moduleInstance->save();

        // Schema Setup
        $moduleInstance->initTables();

        // Field Setup
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);

        $blockcf = new Vtiger_Block();
        $blockcf->label = 'LBL_CUSTOM_INFORMATION';
        $moduleInstance->addBlock($blockcf);

	$field1 = new Vtiger_Field();
	$field1->name = 'payorkey';
	$field1->table = $module->basetable;
	$field1->column = 'payorkey';
	$field1->label = 'PayorKey';
	$field1->columntype = 'VARCHAR(255)';
	$field1->uitype = 2;
	$field1->typeofdata = 'V~M';
	$block->addField($field1);
	$moduleInstance->setEntityIdentifier($field1);

	
	$field2 = new Vtiger_Field();
	$field2->name = 'payorname';
	$field2->column = 'payorname';
	$field2->label= 'PayorName';
	$field2->columntype = 'VARCHAR(255)';
	$field2->uitype = 1;
	$field2->typeofdata = 'V~M';
	$block->addField($field2);

        $field3  = new Vtiger_Field();
        $field3->name = 'address1';
        $field3->label= 'Address1';
        $field3->uitype= 1;
        $field3->column = $field3->name;
        $field3->columntype = 'VARCHAR(255)';
        $field3->typeofdata = 'V~O';
        $block->addField($field3);

        $field4  = new Vtiger_Field();
        $field4->name = 'address2';
        $field4->label= 'Address2';
        $field4->uitype= 1;
        $field4->column = $field4->name;
        $field4->columntype = 'VARCHAR(255)';
        $field4->typeofdata = 'V~O';
        $block->addField($field4);


        $field5  = new Vtiger_Field();
        $field5->name = 'city';
        $field5->label= 'City';
        $field5->uitype= 1;
        $field5->column = $field5->name;
        $field5->columntype = 'VARCHAR(50)';
        $field5->typeofdata = 'V~O';
        $block->addField($field5);

        $field6  = new Vtiger_Field();
        $field6->name = 'state';
        $field6->label= 'State';
        $field6->uitype= 1;
        $field6->column = $field6->name;
        $field6->columntype = 'VARCHAR(50)';
        $field6->typeofdata = 'V~O';
        $block->addField($field6);

        $field7  = new Vtiger_Field();
        $field7->name = 'postalcode';
        $field7->label= 'PostalCode';
        $field7->uitype= 1;
        $field7->column = $field7->name;
        $field7->columntype = 'VARCHAR(20)';
        $field7->typeofdata = 'V~O';
        $block->addField($field7);

        $field8  = new Vtiger_Field();
        $field8->name = 'phonenumber';
        $field8->label= 'PhoneNumber';
        $field8->uitype= 11;
        $field8->column = $field8->name;
        $field8->columntype = 'VARCHAR(30)';
        $field8->typeofdata = 'V~O';
        $block->addField($field8);

        $field9  = new Vtiger_Field();
        $field9->name = 'faxnumber';
        $field9->label= 'FAXNumber';
        $field9->uitype= 11;
        $field9->column = $field9->name;
        $field9->columntype = 'VARCHAR(30)';
        $field9->typeofdata = 'V~O';
        $block->addField($field9);

        $field10  = new Vtiger_Field();
        $field10->name = 'emailaddress';
        $field10->label= 'EmailAddress';
        $field10->uitype= 12;
        $field10->column = $field10->name;
        $field10->columntype = 'VARCHAR(50)';
        $field10->typeofdata = 'V~O';
        $block->addField($field10);

        $field11 = new Vtiger_Field();
        $field11->name = 'contactfirstname';
        $field11->label= 'ContactFirstName';
        $field11->uitype= 1;
        $field11->column = $field11->name;
        $field11->columntype = 'VARCHAR(255)';
        $field11->typeofdata = 'V~O';
        $block->addField($field11);

        $field12 = new Vtiger_Field();
        $field12->name = 'contactmiddlename';
        $field12->label= 'ContactMiddleName';
        $field12->uitype= 1;
        $field12->column = $field12->name;
        $field12->columntype = 'VARCHAR(255)';
        $field12->typeofdata = 'V~O';
        $block->addField($field12);

        $field13 = new Vtiger_Field();
        $field13->name = 'contactlastname';
        $field13->label= 'ContactLastName';
        $field13->uitype= 1;
        $field13->column = $field13->name;
        $field13->columntype = 'VARCHAR(255)';
        $field13->typeofdata = 'V~O';
        $block->addField($field13);

        $field14 = new Vtiger_Field();
        $field14->name = 'paypercentage';
        $field14->label= 'PayPercentage';
        $field14->uitype= 1;
        $field14->column = $field14->name;
        $field14->columntype = 'VARCHAR(50)';
        $field14->typeofdata = 'V~O';
        $block->addField($field14);



        $field15  = new Vtiger_Field();
        $field15->name = 'acctonhold';
        $field15->label= 'AcctOnHold';
        $field15->uitype= 56;
        $field15->column = $field15->name;
        $field15->columntype = 'VARCHAR(10)';
        $field15->typeofdata = 'V~O';
        $block->addField($field15);

        $field16  = new Vtiger_Field();
        $field16->name = 'grpname';
        $field16->label= 'GrpName';
        $field16->uitype= 1;
        $field16->column = $field16->name;
        $field16->columntype = 'VARCHAR(100)';
        $field16->typeofdata = 'V~O';
        $block->addField($field16);

        $field17  = new Vtiger_Field();
        $field17->name = 'insconame';
        $field17->label= 'InsCoName';
        $field17->uitype= 1;
        $field17->column = $field17->name;
        $field17->columntype = 'VARCHAR(100)';
        $field17->typeofdata = 'V~O';
        $block->addField($field17);

        $field18  = new Vtiger_Field();
        $field18->name = 'insplantype';
        $field18->label= 'InsPlanType';
        $field18->uitype= 1;
        $field18->column = $field18->name;
        $field18->columntype = 'VARCHAR(100)';
        $field18->typeofdata = 'V~O';
        $block->addField($field18);

        $field19  = new Vtiger_Field();
        $field19->name = 'pricetable';
        $field19->label= 'PriceTable';
        $field19->uitype= 1;
        $field19->column = $field19->name;
        $field19->columntype = 'VARCHAR(30)';
        $field19->typeofdata = 'V~O';
        $block->addField($field19);

        $field20  = new Vtiger_Field();
        $field20->name = 'claimprg';
        $field20->label= 'ClaimPrg';
        $field20->uitype= 1;
        $field20->column = $field20->name;
        $field20->columntype = 'VARCHAR(50)';
        $field20->typeofdata = 'V~O';
        $block->addField($field20);

        $field21 = new Vtiger_Field();
        $field21->name = 'submission';
        $field21->label= 'Submission';
        $field21->uitype= 1;
        $field21->column = $field21->name;
        $field21->columntype = 'VARCHAR(100)';
        $field21->typeofdata = 'V~O';
        $block->addField($field21);

        $field22 = new Vtiger_Field();
        $field22->name = 'taxtype';
        $field22->label= 'TaxType';
        $field22->uitype= 1;
        $field22->column = $field22->name;
        $field22->columntype = 'VARCHAR(100)';
        $field22->typeofdata = 'V~O';
        $block->addField($field22);

        $field23 = new Vtiger_Field();
        $field23->name = 'reportdt';
        $field23->label= 'ReportDt';
        $field23->uitype= 1;
        $field23->column = $field23->name;
        $field23->columntype = 'VARCHAR(100)';
        $field23->typeofdata = 'V~O';
        $block->addField($field23);



        // Recommended common fields every Entity module should have (linked to core table)
        $mfield1 = new Vtiger_Field();
        $mfield1->name = 'assigned_user_id';
        $mfield1->label = 'Assigned To';
        $mfield1->table = 'vtiger_crmentity';
        $mfield1->column = 'smownerid';
        $mfield1->uitype = 53;
        $mfield1->typeofdata = 'V~M';
        $block->addField($mfield1);

        $mfield2 = new Vtiger_Field();
        $mfield2->name = 'CreatedTime';
        $mfield2->label= 'Created Time';
        $mfield2->table = 'vtiger_crmentity';
        $mfield2->column = 'createdtime';
        $mfield2->uitype = 70;
        $mfield2->typeofdata = 'T~O';
        $mfield2->displaytype= 2;
        $block->addField($mfield2);

        $mfield3 = new Vtiger_Field();
        $mfield3->name = 'ModifiedTime';
        $mfield3->label= 'Modified Time';
        $mfield3->table = 'vtiger_crmentity';
        $mfield3->column = 'modifiedtime';
        $mfield3->uitype = 70;
        $mfield3->typeofdata = 'T~O';
        $mfield3->displaytype= 2;
        $block->addField($mfield3);

        // Filter Setup
        $filter1 = new Vtiger_Filter();
        $filter1->name = 'All';
        $filter1->isdefault = true;
        $moduleInstance->addFilter($filter1);
        $filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($field4, 3)->addField($field5, 4)->addField($mfield2, 5);

        // Sharing Access Setup
        $moduleInstance->setDefaultSharing('Public');

        // Webservice Setup
        $moduleInstance->initWebservice();

        mkdir('modules/'.$MODULENAME);
        echo "OK\n";
}
