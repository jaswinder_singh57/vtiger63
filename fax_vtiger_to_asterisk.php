<?php



function get_datetime() {

        date_default_timezone_set('America/Los_Angeles');
        $date = date("Y-m-d H:i:s");
        return $date;
}



function upload_tif ($tif_file,$o_crmid,$userid) {


	global $tif_folder;
	global $link;
                require_once('vtlib/Vtiger/Functions.php');
                $location = Vtiger_Functions::initStorageFileDirectory();
                $timestamp = time();
                $filename = "faxfile-".$timestamp.".tif";
		$filesize = filesize($tif_file);	

		$query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
	        $result = mysql_query($query,$link);
                $query = "select id from vtiger_crmentity_seq";
                $result = mysql_query($query,$link);
                $row = mysql_fetch_row($result);
                $crmid = $row[0];
                $gmt_now = get_datetime();
                $query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($crmid,'$userid','$userid','Documents',NULL,'$userid','$gmt_now','$gmt_now')";
                $result = mysql_query($query,$link);
                $query = "select cur_id,prefix from vtiger_modentity_num where semodule='Documents' and active = 1";
                $result = mysql_query($query,$link);
                $row = mysql_fetch_row($result);
                $cur_id = $row[0];
                $prefix_cur_id = $row[1];
                $old_cur_id = $cur_id;
                $cur_id++;
                $query = "UPDATE vtiger_modentity_num SET cur_id='$cur_id' where cur_id='$old_cur_id' and active=1 AND semodule='Documents'";
                $result = mysql_query($query,$link);

		$query = "select folderid from vtiger_attachmentsfolder where foldername='$tif_folder'";
		$result = mysql_query($query,$link);
		if (mysql_num_rows($result)) {
			$row = mysql_fetch_row($result);
			$folderid = $row["0"];
		} else {
	                $folderid = '1';
		}
		$filetype= 'image/tiff';
                $query = "insert into vtiger_notes(notesid,title,filename,notecontent,filelocationtype,fileversion,filestatus,folderid,note_no,filesize,filetype) values($crmid,'FaxDoc$crmid','$filename','Document For Fax','I','','1','$folderid','$prefix_cur_id$cur_id','$filesize','$filetype')";
                $result = mysql_query($query,$link);
                $query = "insert into vtiger_notescf(notesid) values($crmid)";
                $result = mysql_query($query,$link);

                $query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
                $result = mysql_query($query,$link);
                $query = "select id from vtiger_crmentity_seq";
                $result = mysql_query($query,$link);
                $row = mysql_fetch_row($result);
                $attachmentid = $row[0];
                $gmt_now = get_datetime();
                $query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($attachmentid,'$userid','$userid','Documents Attachment',NULL,'$userid','$gmt_now','$gmt_now')";
                $result = mysql_query($query,$link);

                $query = "insert into vtiger_attachments(attachmentsid, name, description, type, path) values($attachmentid, '$filename', NULL, '$filetype', '$location')";
                $result = mysql_query($query,$link);
                $query = "delete from vtiger_seattachmentsrel where crmid = $crmid";
                $result = mysql_query($query,$link);
                $query = "insert into vtiger_seattachmentsrel values($crmid,$attachmentid)";
                $result = mysql_query($query,$link);
		$target_file = $location."".$attachmentid."_".$filename;
		shell_exec("cp -rf $tif_file $target_file");
		$query = "update vtiger_outboundfax set tif_filelink ='$crmid' where outboundfaxid='$o_crmid'";
		$result = mysql_query($query,$link);
		
}




function create_tif_file ($faxcover,$faxcoverfile_pdf,$docfile_full,$tif_file,$previewpdf) {
	if ($faxcover) {
		$status = System("/usr/local/bin/gs -q -dNOPAUSE -dBATCH -sDEVICE=tiffg4 -sPAPERSIZE=letter -sOutputFile=$tif_file $faxcoverfile_pdf $docfile_full");
	
	} else {
		$status = System("/usr/local/bin/gs -q -dNOPAUSE -dBATCH -sDEVICE=tiffg4 -sPAPERSIZE=letter -sOutputFile=$tif_file $docfile_full");
	}	
	return $status;
}

function convert_to_pdf ($docfile_full,$outdir){
	shell_exec("libreoffice --headless --convert-to pdf $docfile_full --outdir $outdir");


}

function write_to_file ($filename,$content) {
	
	$fp = fopen("$filename", "w");
	if (!$fp){
		return 1;
	}
	fwrite($fp, $content);
	fclose($fp);
	return 0;

}

function format_filename ($filename) {

	$filename = preg_replace('/\s/','\ ',$filename);
	$filename = preg_replace('/\(/','\\\(',$filename);
	$filename = preg_replace('/\)/','\\\)',$filename);	
	return $filename;

}

function get_page_count($docfile_full){
	$a = shell_exec("pdf2ps -sOutputFile=- $docfile_full | grep -c '%%Page: '");
	return $a;

}

function originate_call ($command) {

	global $faxserver;
	global $faxserver_port;
	global $faxserver_username;
	global $faxserver_secret;
	global $faxserver_secret;

        $oSocket = fsockopen ($faxserver, $faxserver_port, $errno, $errstr, 20);
        if (!$oSocket) {
                echo "$errstr ($errno)<br>\n";
		$status = 1;
        } else {

                fputs($oSocket, "Action: login\r\n");
                //fputs($oSocket, "Events: off\r\n");
                fputs($oSocket, "Username: $faxserver_username\r\n");
                fputs($oSocket, "Secret: $faxserver_secret\r\n");
                fputs($oSocket, "Events: off\r\n\r\n");
/*		while (!feof($oSocket)) {
		    	$msg =  fgets($oSocket, 1024);
			if(preg_match("/Authentication accepted|Authentication failed/",$msg)) {
				echo "Matchfound\n";
				break;
			}
		}
*/
                sleep(1);
                fputs($oSocket, "$command");
/*		while (!feof($oSocket)) {
		    	$msg =  fgets($oSocket, 1024);
			if(preg_match("/Originate successfully|Originate failed/",$msg)) {
				echo "Matchfound\n";
				break;
			}
		}
*/
                sleep(1);
                fputs($oSocket, "Action: Logoff\r\n\r\n");
                sleep(3);
                fclose($oSocket);
		$status = 0;
        }
	return $status;
}

function get_gmt() {
        $newDate = date("Y-m-d H:i:s", time());
        return ($newDate);
}


function checkprocess () {

	global $lock_file;
	$pid = file_get_contents($lock_file);
	if ($pid != '') {
		$running = posix_getpgid($pid);
		if ($running == $pid) {
		        echo "Process Running";
			return 1;	
		}		
	}	
	$pid = posix_getpid();
	file_put_contents($lock_file,$pid);	
	return 0;
}


$DB = 1;

require_once("conn.php");
require_once("fax_config.php");

$status = checkprocess();
if ($status) {
	echo "Exiting as previous not complete";
	exit;
}


echo "starting";
$csql = "select a.crmid,b.faxstatus,a.smcreatorid,b.faxphone,b.filelink,b.datatype,b.faxcover,b.faxsubject,b.templatename,b.customer,b.retrycount,b.tiffile from vtiger_crmentity as a inner join vtiger_outboundfax as b on a.crmid=b.outboundfaxid where a.deleted='0' and b.faxstatus = 'ScheduledToSend' and b.retrycount < '$max_retry' limit $max_limit";
$nresult = mysql_query($csql, $link);
while($nrow = mysql_fetch_array($nresult)) {

		
	$crmid = $nrow["crmid"];
	$faxstatus = $nrow["faxstatus"];
	$faxphone = $nrow["faxphone"];
	$docid = $nrow["filelink"];
	$datatype = $nrow["datatype"];
	$faxcover = $nrow["faxcover"];
	$faxsubject = $nrow["faxsubject"];
	$fax_user = $nrow["smcreatorid"];
	$rel_id = $nrow["customer"];
	$retrycount = $nrow["retrycount"];
	$template_name = $nrow["templatename"];
	$tif_file = $nrow["tiffile"];
	//Gurpreet: sep30 and oct03
	$coquery = "select $comment_field,$data_type2_field,$template2_field,$faxpriority_field,$faxcoverpatientname_field,$faxcoverpatientdob_field,$fax_coverfile_field from vtiger_outboundfaxcf where outboundfaxid='$crmid'";
	//end
	$coresult = mysql_query($coquery,$link);
	$corow = mysql_fetch_array($coresult);
	$faxcomment = $corow[$comment_field];
	//Gurpreet: sep30
	$datatype2 = $corow[$data_type2_field];
	$data_val2 = $corow[$template2_field];
	//end
//Gurpreet: oct03
	$faxpriority = $corow[$faxpriority_field];
	$faxpatientname = $corow[$faxcoverpatientname_field];
	$faxpatientdob = $corow[$faxcoverpatientdob_field];
//end
	$fax_cover_file = $corow[$fax_coverfile_field];	
	$retrycount++;	
	$accid =0;		
	//Gurpreet: sep30
	$final_pdf_array = array();	
	$fax_pages = 0;
	if ($datatype == 'Template' or $datatype2 == 'Template') {

		if ($datatype2 == 'Template' and $data_val2 != ''){
			$template_name = $data_val2;
		}
	//Gurpreet: end
		$tquery = "select setype from vtiger_crmentity where crmid='$rel_id'";	
		$result = mysql_query($tquery,$link);
		if (!mysql_num_rows($result)) {
                        $tempquery = "update vtiger_outboundfax set faxstatus='Failed',failurereason='FaxEntityDoestNotExist',retrycount='3' where outboundfaxid='$crmid'";
                        $result = mysql_query($tempquery,$link);
                        continue;
		}
		$row = mysql_fetch_array($result);
		$rel_module = $row["setype"];
		$rel_module = strtolower($rel_module);
		$templatequery = "select  body from vtiger_faxtemplates where templatename='$template_name' and deleted='0'";
		$result = mysql_query($templatequery,$link);
		if (!mysql_num_rows($result)){
			$tempquery = "update vtiger_outboundfax set faxstatus='Failed',failurereason='TempalteNotFound',retrycount='3' where outboundfaxid='$crmid'";
			$result = mysql_query($tempquery,$link);		
			continue;
		}
                $row = mysql_fetch_array($result);
                $templatebody = $row["body"];
		$accid = 0;	
		list ($a,$b) = explode('-',$template_name);
		if ($a == 'ToDr') {
                        $query = "select accountid from vtiger_contactdetails where contactid='$rel_id'";                                                                                       $result = mysql_query($query,$link);
			if (mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				$accid = $row["accountid"];	
				$query = "SELECT fax FROM  vtiger_crmentity LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountshipads ON vtiger_accountshipads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$accid'";
				$result = mysql_query($query,$link);
				if (mysql_num_rows($result)) {
					$row = mysql_fetch_array($result);
					$faxphone =$row["fax"];
					$query = "update vtiger_outboundfax set faxphone='$faxphone' where outboundfaxid='$crmid'";
					$result = mysql_query($query,$link);
				
				}		
			} else {
				$accid = $rel_id;	
			}		
				
		}		
		$result = preg_match_all("/\\$(?:[a-zA-Z0-9]+)-(?:[a-zA-Z0-9]+)(?:_[a-zA-Z0-9]+)?(?::[a-zA-Z0-9]+)?(?:_[a-zA-Z0-9]+)?\\$/", $templatebody, $matches);

	        if($result != 0){
        	    	$templateVariablePair = $matches[0];
            		for ($i = 0; $i < count($templateVariablePair); $i++) {
                		$templateVariablePair[$i] = str_replace('$', '', $templateVariablePair[$i]);
				$search = $templateVariablePair[$i];
		                list($module, $columnName) = explode('-', $templateVariablePair[$i]);
				if ($module == 'contacts') {
					$query = "SELECT $columnName FROM  vtiger_crmentity LEFT JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_crmentity.crmid LEFT JOIN vtiger_contactaddress ON vtiger_contactaddress.contactaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_contactsubdetails ON vtiger_contactsubdetails.contactsubscriptionid=vtiger_crmentity.crmid LEFT JOIN vtiger_contactscf ON vtiger_contactscf.contactid=vtiger_crmentity.crmid LEFT JOIN vtiger_customerdetails ON vtiger_customerdetails.customerid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$rel_id'";	
				} elseif ($module == 'accounts') {
					if ($accid) {
						$query = "SELECT $columnName FROM  vtiger_crmentity LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountshipads ON vtiger_accountshipads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$accid'";
					} else {
                                        $query = "SELECT $columnName FROM  vtiger_crmentity LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountshipads ON vtiger_accountshipads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$rel_id'";
					}
				} elseif ($module == 'leads') {
					$query = "SELECT $columnName FROM  vtiger_crmentity LEFT JOIN vtiger_leaddetails ON vtiger_leaddetails.leadid=vtiger_crmentity.crmid LEFT JOIN vtiger_leadsubdetails ON vtiger_leadsubdetails.leadsubscriptionid=vtiger_crmentity.crmid LEFT JOIN vtiger_leadaddress ON vtiger_leadaddress.leadaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_leadscf ON vtiger_leadscf.leadid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$rel_id'";	
				} elseif ($module == 'vendors'){
					$query = "SELECT $columnName FROM  vtiger_crmentity LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_crmentity.crmid LEFT JOIN vtiger_vendorcf ON vtiger_vendorcf.vendorid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$rel_id'"; 
//Nov 2				
				} elseif ($module == 'patientnotes'){
					$query = "SELECT $columnName FROM  vtiger_crmentity LEFT JOIN vtiger_patientnotes ON vtiger_patientnotes.patientnotesid=vtiger_crmentity.crmid LEFT JOIN vtiger_patientnotescf ON vtiger_patientnotescf.patientnotesid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.crmid='$rel_id'"; 
//end
				} elseif ($module == 'users') {
					$query = "select $columnName from vtiger_users where id='$fax_user'";

				} elseif ($module == 'custom') {
					if ($columnName == 'currentdate') {
						$query = "select date(now())";
					} else {
						$query = "select time(now())";
					}

				} else {
					$query == '';
				}	
				if ($query == '') {
					$columnvalue = '';
				} else {
					$result = mysql_query($query,$link);
	                                if (mysql_num_rows($result)) {
                	                	$row = mysql_fetch_row($result);
        	                        	$columnvalue = $row[0];
                        	        } else {
                                	        $columnvalue = '';              
                                	}
				}
				$templatebody = preg_replace("/\\$$search\\$/","$columnvalue",$templatebody);
        		}
			
		} else {
                	echo "Nothing";

	       	}

		$img_tag = strpos($templatebody,'<img ');
		if ($img_tag) {

			$pattern = '@(https?://([-\w\.]+)+(:\d+)?)@';
			$templatebody = preg_replace ($pattern,$webroot,$templatebody);

		}

		
		$timestamp = time();
		$tif_file = $asteriskpath."/faxfile-".$timestamp.".tif";	
		$docfile_html = $asteriskpath."/template-".$timestamp.".html";	
		$status = write_to_file($docfile_html,$templatebody);
	
                $docfile_full = $asteriskpath."/template-".$timestamp.".pdf";
                shell_exec("/usr/local/bin/wkhtmltopdf $docfile_html $docfile_full");
	//Gurpreet: sep30
		array_push($final_pdf_array,$docfile_full);
	        $fax_pages = get_page_count($docfile_full);
	        $fax_pages= trim($fax_pages);
	//end

	} 
	if ($datatype == 'File') {

		$nquery = "select attachmentsid,name,path,type from vtiger_attachments where attachmentsid = (select attachmentsid from vtiger_seattachmentsrel where crmid='$docid')";
		$nresult = mysql_query($nquery, $link);
		$nrow = mysql_fetch_array($nresult);
		$path = $nrow["path"];
		$docfile = $nrow["name"];
		$attachment_type = $nrow["type"];
		$attachimentsid = $nrow["attachmentsid"];
		$tif_file = '';
		$pdf_file = '';
		$docfile_name = $attachimentsid."_".$docfile;
		if ($attachment_type == 'application/pdf') {
			$tif_file = preg_replace('~pdf(?!.*pdf)~', 'tif', $docfile_name);
		} elseif ($attachment_type == 'application/msword' or $attachment_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
			$tif_file = preg_replace('~doc(?!.*doc)~', 'tif', $docfile_name);
			$pdf_file = preg_replace('~doc(?!.*doc)~', 'pdf', $docfile_name);
			
		} elseif ($attachment_type == 'image/tiff'){
			$tif_file = $docfile_name;
		} else {
			echo "Unknown File Format $attachment_type";
			continue;
		}
		$docfile_name = format_filename($docfile_name);
		$docfile_full = $crmroot."/".$path."".$docfile_name;
                if ($pdf_file != '') {
			$status = convert_to_pdf($docfile_full,$asteriskpath."/");
                        if ($status) {
                                echo "FiledToConvertToPDF";
                                continue;
                        }
			$pdf_file = format_filename($pdf_file);	
			$docfile_full = $asteriskpath."/".$pdf_file;
		}
		$timestamp = time();
		$tif_file = $asteriskpath."/faxfile-".$timestamp.".tif";
	//Gurpreet: sep30
		array_push($final_pdf_array,$docfile_full);
	        $fax_pages_tmp = get_page_count($docfile_full);
	        $fax_pages_tmp= trim($fax_pages_tmp);
		if ($fax_pages){
			$fax_pages = $fax_pages + $fax_pages_tmp;
		} else {
			$fax_pages = $fax_pages_tmp;
		}
	//end
			
	}
	//Gurpreet: sep30
	if ($datatype2 == 'File' ) {

		$docid = $data_val2;		
		$nquery = "select attachmentsid,name,path,type from vtiger_attachments where attachmentsid = (select attachmentsid from vtiger_seattachmentsrel where crmid='$docid')";
		$nresult = mysql_query($nquery, $link);
		$nrow = mysql_fetch_array($nresult);
		$path = $nrow["path"];
		$docfile = $nrow["name"];
		$attachment_type = $nrow["type"];
		$attachimentsid = $nrow["attachmentsid"];
		$tif_file = '';
		$pdf_file = '';
		$docfile_name = $attachimentsid."_".$docfile;
		if ($attachment_type == 'application/pdf') {
			$tif_file = preg_replace('~pdf(?!.*pdf)~', 'tif', $docfile_name);
		} elseif ($attachment_type == 'application/msword' or $attachment_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
			$tif_file = preg_replace('~doc(?!.*doc)~', 'tif', $docfile_name);
			$pdf_file = preg_replace('~doc(?!.*doc)~', 'pdf', $docfile_name);
			
		} elseif ($attachment_type == 'image/tiff'){
			$tif_file = $docfile_name;
		} else {
			echo "Unknown File Format $attachment_type";
			continue;
		}
		$docfile_name = format_filename($docfile_name);
		$docfile_full = $crmroot."/".$path."".$docfile_name;
                if ($pdf_file != '') {
			$status = convert_to_pdf($docfile_full,$asteriskpath."/");
                        if ($status) {
                                echo "FiledToConvertToPDF";
                                continue;
                        }
			$pdf_file = format_filename($pdf_file);	
			$docfile_full = $asteriskpath."/".$pdf_file;
		}
		$timestamp = time();
		$tif_file = $asteriskpath."/faxfile-".$timestamp.".tif";

		array_push($final_pdf_array,$docfile_full);			
                $fax_pages_tmp = get_page_count($docfile_full);
                $fax_pages_tmp= trim($fax_pages_tmp);
                if ($fax_pages){
                        $fax_pages = $fax_pages + $fax_pages_tmp;
                } else {
                        $fax_pages = $fax_pages_tmp;
                }

	}
	
	$docfile_full = implode('  ',$final_pdf_array);	
//end
	$fax_pages++;
	$faxcoverfile_pdf = '';

        if ($faxcover) {
                $filecontent = file_get_contents("$fax_cover_file");

                $companyquery = "select * from vtiger_organizationdetails";
                $result = mysql_query($companyquery,$link);
                $row = mysql_fetch_array($result);
                $company_name = $row["organizationname"];
                $company_address = $row["address"];
                $company_city = $row["city"];
                $company_state = $row["state"];
                $company_zip = $row["code"];
                $company_phone = $row["phone"];
                $company_fax = $row["fax"];
                $company_website = $row["website"];
                $company_logo = $row["logo"];

                $userquery = "select first_name,last_name from vtiger_users where id='$fax_user'";
                $result = mysql_query($userquery,$link);
                $row = mysql_fetch_array($result);
                $agent_name = $row["first_name"]." ".$row["last_name"];

	//Gurpreet: sep30
		$patientquery = "select firstname,lastname from vtiger_contactdetails where contactid='$rel_id'";
                $result = mysql_query($patientquery,$link);
                $row = mysql_fetch_array($result);
                $patient_firstname = $row["firstname"];
		$patient_lastname = $row["lastname"];
                $patientquery = "select $patient_dob_field from vtiger_contactscf where contactid='$rel_id'";
                $result = mysql_query($patientquery,$link);
                $row = mysql_fetch_array($result);
                $patient_dob = $row["$patient_dob_field"];
	//end

                $companyquery = "select label from vtiger_crmentity where crmid='$rel_id'";
                $result = mysql_query($companyquery,$link);
                $row = mysql_fetch_array($result);
                $receivername = $row["label"];
		if ($accid) {
			$companyquery = "select label from vtiger_crmentity where crmid='$accid'";
	                $result = mysql_query($companyquery,$link);
        	        $row = mysql_fetch_array($result);
                	$receivername = $row["label"];
		}

                $current_datetime = date("m/d/y : H:i:s",time());
                $current_date = date("m/d/y",time());
                $current_time = date("H:i:s",time());
                        
                $filecontent = preg_replace('/\$company-name\$/',"$company_name",$filecontent);
                $filecontent = preg_replace('/\$company-address\$/',"$company_address",$filecontent);
                $filecontent = preg_replace('/\$company-city\$/',"$company_city",$filecontent);
                $filecontent = preg_replace('/\$company-state\$/',"$company_state",$filecontent);
                $filecontent = preg_replace('/\$company-zip\$/',"$company_zip",$filecontent);
                $filecontent = preg_replace('/\$company-phone\$/',"$company_phone",$filecontent);
                $filecontent = preg_replace('/\$company-fax\$/',"$company_fax",$filecontent);
                $filecontent = preg_replace('/\$company-website\$/',"$company_website",$filecontent);
                $filecontent = preg_replace('/\$agent-name\$/',"$agent_name",$filecontent);
                $filecontent = preg_replace('/\$fax-subject\$/',"$faxsubject",$filecontent);
                $filecontent = preg_replace('/\$receiver-name\$/',"$receivername",$filecontent);
                $filecontent = preg_replace('/\$current-datetime\$/',"$current_datetime",$filecontent);
                $filecontent = preg_replace('/\$current-date\$/',"$current_date",$filecontent);
                $filecontent = preg_replace('/\$current-time\$/',"$current_time",$filecontent);
                $filecontent = preg_replace('/\$fax-pages\$/',"$fax_pages",$filecontent);
		//Gurpreet: spe20
                $filecontent = preg_replace('/\$fax-comment\$/',"$faxcomment",$filecontent);
		//end
	//Gurpreet: oct03
                $filecontent = preg_replace('/\$fax-priority\$/',"$faxpriority",$filecontent);
		if ($faxpatientname) {
			$patient_name_val = "Patient Name: ".$patient_firstname." ".$patient_lastname;
	                $filecontent = preg_replace('/\$patient-name\$/',"$patient_name_val",$filecontent);
		} else {
	
	                $filecontent = preg_replace('/\$patient-name\$/',"",$filecontent);
		}
		if ($faxpatientdob) {
			$patient_dob = date('m/d/Y',strtotime($patient_dob));
			$patient_dob_val = "Patient DOB: ".$patient_dob;
		 	$filecontent = preg_replace('/\$patient-dob\$/',"$patient_dob_val",$filecontent);
		} else {
			$filecontent = preg_replace('/\$patient-dob\$/',"",$filecontent);
		}

	//Gurpreet: end	
		$timestamp = time();
                $faxcoverfile_html = $asteriskpath."/faxcover".$timestamp.".html";
                $faxcoverfile_pdf = $asteriskpath."/faxcover".$timestamp.".pdf";
                $status = write_to_file ($faxcoverfile_html,$filecontent);

                shell_exec("/usr/local/bin/wkhtmltopdf $faxcoverfile_html $faxcoverfile_pdf");

         
        }

	exit;	
	$previewpdf = $crmroot."/preview/preview-".$timestamp.".pdf";
	$status = create_tif_file($faxcover,$faxcoverfile_pdf,$docfile_full,$tif_file,$previewpdf);	
	upload_tif($tif_file,$crmid,$fax_user);

	$sessionid = $timestamp."_".$crmid;
	
	$varset = "FAXFILE=$tif_file,FAXHEADER=$faxheader,TIMESTAMP=".date("d/m/y : H:i:s",time()).",DESTINATION=$faxphone,LOCALID=$faxlocalstationid,FAXCRMID=$crmid,CDR(userfield)=$sessionid"; 
	$command = "Action: Originate\r\nChannel: SIP/$dialout_prefix$faxphone$faxtrunk\r\nWaitTime: 60\r\nContext: $outboundfax_context\r\nExten: s\r\nPriority: 1\r\nCallerId: $faxcallerid\r\nVariable: $varset\r\n\r\n";
	$status = originate_call($command);

	if ($status) {
		$query = "update vtiger_outboundfax set faxstatus='Failed',retrycount='$retrycount',failurereason='Cant connect to Socket', tiffile='$tif_file' where outboundfaxid='$crmid'";
 
	} else {
		$query = "update vtiger_outboundfax set faxstatus='Dialing',retrycount='$retrycount',tiffile='$tif_file',callsessionid='$sessionid' where outboundfaxid='$crmid'";
	}	
	$fresult = mysql_query($query, $link);
        $current_date_gmt = get_datetime();
        $query = "update vtiger_crmentity set modifiedtime='$current_date_gmt' where crmid='$crmid'";
        $result = mysql_query($query,$link);

	
}

$current_date_gmt = get_datetime();
$csql = "select a.crmid,b.faxstatus,a.smcreatorid,b.faxphone,b.filelink,b.datatype,b.faxcover,b.faxsubject,b.templatename,b.customer,b.retrycount,b.tiffile,b.callsessionid from vtiger_crmentity as a inner join vtiger_outboundfax as b on a.crmid=b.outboundfaxid where a.deleted='0' and b.faxstatus in ('Dialing','Sending','ReDialing','Failed') and b.retrycount < '$max_retry' and TIME_TO_SEC(TIMEDIFF('$current_date_gmt',a.modifiedtime)) > $dialing_diff limit $max_limit";
echo "\nchecking old\n";
$nresult = mysql_query($csql, $link);
while($row = mysql_fetch_array($nresult)) {


        $crmid = $row["crmid"];
        $faxstatus = $row["faxstatus"];
        $faxphone = $row["faxphone"];
        $docid = $row["filelink"];
        $datatype = $row["datatype"];
        $faxcover = $row["faxcover"];
        $faxsubject = $row["faxsubject"];
        $fax_user = $row["smcreatorid"];
        $rel_id = $row["customer"];
        $retrycount = $row["retrycount"];
        $template_name = $row["templatename"];
        $tif_file = $row["tiffile"];
        $callsessionid = $row["callsessionid"];
	if ($callsessionid != '' and $faxstatus !='Failed') {
		$query = "select disposition from $cdr_table where userfield='$callsessionid'";
		$result = mysql_query($query,$faxlink);
		if(!mysql_num_rows($result)) {
			continue;	
		} else {
			$nrow = mysql_fetch_array($result);
			$reason = $nrow["disposition"];
			$query = "update vtiger_outboundfax set faxstatus='Failed',failurereason='$reason' where outboundfaxid='$crmid'";			
			$result = mysql_query($query,$link);

		}
	}
        $retrycount++;
	$timestamp = time();
	$sessionid = $timestamp."_".$crmid;
        $varset = "FAXFILE=$tif_file,FAXHEADER=$faxheader,TIMESTAMP=".date("d/m/y : H:i:s",time()).",DESTINATION=$faxphone,LOCALID=$faxlocalstationid,FAXCRMID=$crmid,CDR(userfield)=$sessionid";
        $command = "Action: Originate\r\nChannel: SIP/$dialout_prefix$faxphone$faxtrunk\r\nWaitTime: 60\r\nContext: $outboundfax_context\r\nExten: s\r\nPriority: 1\r\nCallerId: $faxcallerid\r\nVariable: $varset\r\n\r\n";

        $status = originate_call($command);

        if ($status) {
                $query = "update vtiger_outboundfax set faxstatus='Failed',retrycount='$retrycount',failurereason='Cant connect to Socket', tiffile='$tif_file' where outboundfaxid='$crmid'";

        } else {
                $query = "update vtiger_outboundfax set faxstatus='ReDialing',retrycount='$retrycount',tiffile='$tif_file',callsessionid='$sessionid' where outboundfaxid='$crmid'";
        }
        $fresult = mysql_query($query, $link);
        $current_date_gmt = get_datetime();
        $query = "update vtiger_crmentity set modifiedtime='$current_date_gmt' where crmid='$crmid'";
        $result = mysql_query($query,$link);
}
$current_date_gmt = get_datetime();
$csql = "select a.crmid,b.faxstatus,a.smcreatorid,b.faxphone,b.filelink,b.datatype,b.faxcover,b.faxsubject,b.templatename,b.customer,b.retrycount,b.tiffile,b.callsessionid from vtiger_crmentity as a inner join vtiger_outboundfax as b on a.crmid=b.outboundfaxid where a.deleted='0' and b.faxstatus in ('Dialing','Sending','ReDialing') and b.retrycount >= '$max_retry' and TIME_TO_SEC(TIMEDIFF('$current_date_gmt',a.modifiedtime)) > $dialing_diff limit $max_limit";
echo "ClosingFailed old\n";

$nresult = mysql_query($csql, $link);
while($row = mysql_fetch_array($nresult)) {

        $crmid = $row["crmid"];
        $faxstatus = $row["faxstatus"];
        $retrycount = $row["retrycount"];
        $template_name = $row["templatename"];
        $tif_file = $row["tiffile"];
        $callsessionid = $row["callsessionid"];

        if ($callsessionid != '') {
                $query = "select disposition from $cdr_table where userfield='$callsessionid'";
                $result = mysql_query($query,$faxlink);
                if(!mysql_num_rows($result)) {
                        continue;
                } else {
                        $nrow = mysql_fetch_array($result);
                        $reason = $nrow["disposition"];
                        $query = "update vtiger_outboundfax set faxstatus='Failed',failurereason='$reason' where outboundfaxid='$crmid'";
                        $result = mysql_query($query,$link);

                }
        }

}

//Resend Block

echo "CheckResending\n";
$csql = "select a.crmid,b.faxstatus,a.smcreatorid,b.faxphone,b.filelink,b.datatype,b.faxcover,b.faxsubject,b.templatename,b.customer,b.retrycount,b.tiffile,b.callsessionid from vtiger_crmentity as a inner join vtiger_outboundfax as b on a.crmid=b.outboundfaxid where a.deleted='0' and b.faxstatus in ('Resend')";
$nresult = mysql_query($csql, $link);
while($row = mysql_fetch_array($nresult)) {

        $crmid = $row["crmid"];
        $faxstatus = $row["faxstatus"];
        $faxphone = $row["faxphone"];
        $docid = $row["filelink"];
        $datatype = $row["datatype"];
        $faxcover = $row["faxcover"];
        $faxsubject = $row["faxsubject"];
        $fax_user = $row["smcreatorid"];
        $rel_id = $row["customer"];
        $retrycount = $row["retrycount"];
        $template_name = $row["templatename"];
        $tif_file = $row["tiffile"];
        $callsessionid = $row["callsessionid"];
        $retrycount = 1;
        $timestamp = time();
        $sessionid = $timestamp."_".$crmid;
        $varset = "FAXFILE=$tif_file,FAXHEADER=$faxheader,TIMESTAMP=".date("d/m/y : H:i:s",time()).",DESTINATION=$faxphone,LOCALID=$faxlocalstationid,FAXCRMID=$crmid,CDR(userfield)=$sessionid";
        $command = "Action: Originate\r\nChannel: SIP/$dialout_prefix$faxphone$faxtrunk\r\nWaitTime: 60\r\nContext: $outboundfax_context\r\nExten: s\r\nPriority: 1\r\nCallerId: $faxcallerid\r\nVariable: $varset\r\n\r\n";

        $status = originate_call($command);

        if ($status) {
                $query = "update vtiger_outboundfax set faxstatus='Failed',retrycount='$retrycount',failurereason='Cant connect to Socket', tiffile='$tif_file' where outboundfaxid='$crmid'";

        } else {
                $query = "update vtiger_outboundfax set faxstatus='ReDialing',retrycount='$retrycount',tiffile='$tif_file',callsessionid='$sessionid' where outboundfaxid='$crmid'";
        }
        $fresult = mysql_query($query, $link);
        $current_date_gmt = get_datetime();
        $query = "update vtiger_crmentity set modifiedtime='$current_date_gmt' where crmid='$crmid'";
        $result = mysql_query($query,$link);
}


$fsql = "select columnname from vtiger_field where tablename='vtiger_outboundfaxcf' and fieldlabel='NextDayRetry'";
$fresult = mysql_query($fsql, $link);
if (mysql_num_rows($fresult)) {

	$frow = mysql_fetch_array($fresult);
	$fieldname = $frow["columnname"];
		

echo "CheckNextDay\n";
$csql = "select a.createdtime,a.crmid,b.faxstatus,a.smcreatorid,b.faxphone,b.filelink,b.datatype,b.faxcover,b.faxsubject,b.templatename,b.customer,b.retrycount,b.tiffile,b.callsessionid from vtiger_crmentity as a inner join vtiger_outboundfax as b on a.crmid=b.outboundfaxid inner join vtiger_outboundfaxcf as c on b.outboundfaxid=c.outboundfaxid where a.deleted='0' and b.faxstatus in ('Failed') and c.$fieldname='0' and date(createdtime) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
$nresult = mysql_query($csql, $link);
while($row = mysql_fetch_array($nresult)) {

        $crmid = $row["crmid"];
        $faxstatus = $row["faxstatus"];
        $faxphone = $row["faxphone"];
        $docid = $row["filelink"];
        $datatype = $row["datatype"];
        $faxcover = $row["faxcover"];
        $faxsubject = $row["faxsubject"];
        $fax_user = $row["smcreatorid"];
        $rel_id = $row["customer"];
        $retrycount = $row["retrycount"];
        $template_name = $row["templatename"];
        $tif_file = $row["tiffile"];
        $callsessionid = $row["callsessionid"];
        $retrycount = 1; 
        $timestamp = time();
        $sessionid = $timestamp."_".$crmid;
        $varset = "FAXFILE=$tif_file,FAXHEADER=$faxheader,TIMESTAMP=".date("d/m/y : H:i:s",time()).",DESTINATION=$faxphone,LOCALID=$faxlocalstationid,FAXCRMID=$crmid,CDR(userfield)=$sessionid";
        $command = "Action: Originate\r\nChannel: SIP/$dialout_prefix$faxphone$faxtrunk\r\nWaitTime: 60\r\nContext: $outboundfax_context\r\nExten: s\r\nPriority: 1\r\nCallerId: $faxcallerid\r\nVariable: $varset\r\n\r\n";

        $status = originate_call($command);

        if ($status) {
                $query = "update vtiger_outboundfax set faxstatus='Failed',retrycount='$retrycount',failurereason='Cant connect to Socket', tiffile='$tif_file' where outboundfaxid='$crmid'";

        } else {
                $query = "update vtiger_outboundfax set faxstatus='ReDialing',retrycount='$retrycount',tiffile='$tif_file',callsessionid='$sessionid' where outboundfaxid='$crmid'";
        }
        $fresult = mysql_query($query, $link);
        $current_date_gmt = get_datetime();

        $query = "update vtiger_crmentity set modifiedtime='$current_date_gmt' where crmid='$crmid'";
        $result = mysql_query($query,$link);
        $query = "update vtiger_outboundfaxcf set $fieldname='1' where outboundfaxid='$crmid'";
        $result = mysql_query($query,$link);
}
}


exit;

?> 

