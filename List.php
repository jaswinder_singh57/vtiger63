<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Contacts_List_View extends Vtiger_List_View {

        function process (Vtiger_Request $request) {
                $viewer = $this->getViewer ($request);
                $moduleName = $request->getModule();
                $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
                $this->viewName = $request->get('viewname');

                $this->initializeListViewContents($request, $viewer);
                $viewer->assign('VIEW', $request->get('view'));
                $viewer->assign('MODULE_MODEL', $moduleModel);
                $viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
                $viewer->view('ListViewContentsNew.tpl', $moduleName);
        }

}
?>
