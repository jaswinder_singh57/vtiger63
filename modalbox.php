<script>
function validateForm(){
    var $val = document.getElementById('agentdispo').value;
    if ($val == 'select'){
        alert("Please select a Disposition")
        return false;
    }
    var $comment = document.getElementById('agentcomment').value;
    if ($comment.length < 1){
        alert("Please Enter the comments");
        return false;
    }
    return true;
}

data = {}
$('#save').click(function(e) {
	e.preventDefault();
	var valid = validateForm();	
	if (!valid) {
		return false;
	}

             var agentdispo= $('#agentdispo').val();
             var callfrom= $('#callfrom').val();
             var callname= $('#callname').val();
             var uniqueid= $('#uniqueid').val();
             var exten= $('#exten').val();
             var crmid= $('#crmid').val();
             var agentcomment= $('#agentcomment').val();
            $.ajax({
               type: "POST",
               url: "disposition.php",
               data: {
                       callfrom: callfrom,
                       callname: callname,
                       uniqueid: uniqueid,
                       exten: exten,
                       crmid: crmid,
                       agentdispo: agentdispo,
                       agentcomment: agentcomment,
               },
               success: function(data) {
			$('#myModal').modal('hide');
                       }
               });
});

$('#cancel').click(function(e) {
        e.preventDefault();
             var callfrom= $('#callfrom').val();
             var uniqueid= $('#uniqueid').val();
             var exten = $('#exten').val();
             var user_id= $('#user_id').val();
            $.ajax({
               type: "POST",
               url: "disposition.php",
               data: {
                       callfrom: callfrom,
                       uniqueid: uniqueid,
                       exten: exten,
                       user_id: user_id,
               },
               success: function(data) {
                        $('#myModal').modal('hide');
                       }
               });
});

    
</script>    
<?php

    
require('conn.php');
require('config_cdr.php');
    
    
    
if (isset($_REQUEST["exten"])) {
    //$calldate = $_REQUEST["calldate"];
	$callfrom = $_REQUEST["callfrom"];
	$callname = $_REQUEST["callname"];
	$uniqueid = $_REQUEST["uniqueid"];
	$exten = $_REQUEST["exten"];
	$crmid = $_REQUEST["crmid"];
	$record_found = $_REQUEST["record_found"];
	$create_at = $_REQUEST["create_at"];
	$user_id = $_REQUEST["user_id"];
	$query = "select label from vtiger_crmentity where crmid='$crmid'";
	$result = mysql_query($query,$link);
	$row = mysql_fetch_assoc($result);
	$recordlabel = $row['label'];
    
}

$query = "select columnname from vtiger_field where tablename = 'vtiger_cdrcf' and fieldlabel = '$agent_dispo_field'";
$result = mysql_query($query,$link);
if (!mysql_num_rows($result)){
    echo "Please Set the Agent disposition Field";
    exit;
}    
$row = mysql_fetch_assoc($result);
$fieldname = $row['columnname'];    
$dptablename = "vtiger_".$row['columnname'];   
    
    
?>    

<div id="myModal" class="modal fade" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h4 class="modal-title">Enter Remarks</h4>
            </div>
            <div class="modal-body">
	<p> Call Time: <?php echo $create_at; ?> Name: <?php echo $recordlabel; ?> | Phone Number : <?php echo $callfrom; ?> |  Exten: <?php echo $exten ?></p>
  <form  method="post" name="dispoform" onsubmit="return validateForm()">
      <input type="hidden" name="callfrom" id="callfrom" value='<?php echo $callfrom; ?>'>
      <input type="hidden" name="callname" id="callname" value='<?php echo $callname; ?>'>
      <input type="hidden" name="uniqueid" id="uniqueid" value='<?php echo $uniqueid; ?>'>
      <input type="hidden" name="exten" id="exten" value='<?php echo $exten; ?>'>
      <input type="hidden" name="crmid" id="crmid" value='<?php echo $crmid; ?>'>
      <input type="hidden" name="user_id" id="user_id" value='<?php echo $user_id; ?>'>
    
	<div class="form-group">
      		<label class="control-label" for="dispositoin">Disposition</label>
          	<div class="dropdown">
              		<select name="agentdispo" id="agentdispo" style="height: 25px;">
	                <option value="select">Select</option>

<?php
$query = "select $fieldname from $dptablename";
$result=mysql_query($query,$link);
while($row = mysql_fetch_array($result)) {
        print "<option value='".$row['0']."'>".$row['0']."</option>";
}

?>
			</select>
      		</div>
    	</div>

        <div class="form-group">
        	<label for="agentcomment" class="control-label">Comment:</label>
                <textarea class="form-control" id="agentcomment"></textarea>
       	</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id='save' >Save</button>
<?php 
if (!$record_found) { ?>
                <button type="button" class="btn btn-default" id='cancel' >Cancel</button>
<?php } ?>
            </div>
  </form>
</div>

</div>
</div>
</div>

