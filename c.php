<?php


if ($_REQUEST['file']) {

$filename = $_REQUEST['file'];

try
{
$images = new Imagick("$filename"); 
foreach($images as $i=>$image) {
    // Providing 0 forces thumbnail Image to maintain aspect ratio
    $image->thumbnailImage(768,0);
    $image->resizeImage(750,500,Imagick::FILTER_LANCZOS,1);

    $image->writeImage("page".$i.".jpg");
    $b = $i+1;	
    echo "<center><img src='page$i.jpg' alt='images' >Page $b</img></center>";
}
$images->clear();

}
catch(Exception $e){ 
        echo $e->getMessage();
}

} else {
	echo "Nothing To Do";
}
?>
