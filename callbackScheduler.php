<?php

include_once("/var/www/html/vtiger63/conn.php");


function write_log ($log) {
        global $logfile;
        $fp = fopen($logfile,'a+');
        fwrite($fp,$log."\n");
        fclose($fp);
}


$disposition = "'1day','2days','1hr','2hrs'";
$cquery = "select a.callbackmanagerid,a.callsubject,a.customer,a.callstatus,a.campaignname,a.callbackdatetime,b.smcreatorid from vtiger_callbackmanager as a inner join vtiger_crmentity as b on a.callbackmanagerid=b.crmid and b.deleted='0' and a.callstatus='ScheduledToCallBack' and a.callbackdatetime < now()"; 

echo "Starting\n";
$cresult = mysql_query($cquery, $link);
while($crow = mysql_fetch_assoc($cresult)) {

	$callbackmanagerid = $crow["callbackmanagerid"];
	$customer = $crow["customer"];
	$subject = $crow["callsubject"];
	$campaign = $crow["campaignname"];
	$calldate = $crow["callbackdatetime"];
	$userid = $crow["smcreatorid"];
	
        $query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
        $res = mysql_query($query,$link);
        $query = "select id from vtiger_crmentity_seq";
        $res = mysql_query($query,$link);
        $row = mysql_fetch_row($res);
        $crm_id = $row[0];
        $query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($crm_id,'$userid','$userid','DialerManager',NULL,'$userid',now(),now())";
        $res = mysql_query($query,$link);
        $query = "insert into vtiger_dialermanager(dialermanagerid,customer,campaignid_selected,status,leadid) values($crm_id,'$customer','$campaign','ScheduledForDialer','')";
        $res = mysql_query($query,$link);
        $query = "insert into vtiger_dialermanagercf(dialermanagerid) values($crm_id)";
        $res = mysql_query($query,$link);
        $query = "UPDATE vtiger_crmentity SET label='$customer' WHERE crmid='$crm_id'";
        $res = mysql_query($query,$link);

        $query = "INSERT INTO vtiger_crmentityrel(crmid, module, relcrmid, relmodule) VALUES('$customer','Contacts',$crm_id,'DialerManager')";
        $res = mysql_query($query,$link);

	$query = "update vtiger_callbackmanager set dialerrecord='$crm_id' , callstatus='ScheduledForDialer' where callbackmanagerid='$callbackmanagerid'";
	$res = mysql_query($query,$link);	

}

$cquery = "select a.callstatus,a.callbackmanagerid,a.callsubject,a.customer,a.callstatus,a.campaignname,a.callbackdatetime,b.smcreatorid from vtiger_callbackmanager as a inner join vtiger_crmentity as b on a.callbackmanagerid=b.crmid and b.deleted='0' and a.callstatus in ($disposition) and a.callbackrecord = ''";

$cresult = mysql_query($cquery, $link);

while($crow = mysql_fetch_assoc($cresult)) {

        $callbackmanagerid = $crow["callbackmanagerid"];
        $callstatus = $crow["callstatus"];
        $customer = $crow["customer"];
        $subject = "Cust-".$crow["callsubject"];
        $campaign = $crow["campaignname"];
        $calldate = $crow["callbackdatetime"];
        $userid = $crow["smcreatorid"];
	if(preg_match('/hr/',$callstatus)) {
		$seconds = 3600;
		if (preg_match('/2hr/',$callstatus)){
			$seconds = $seconds*2;
		}

	} else {
		$seconds = 86400;
		if (preg_match('/2day/',$callstatus)){
                        $seconds = $seconds*2;
                }

	} 

	$timestamp =  strtotime($calldate);
	$newtimestamp = $timestamp+$seconds;
	$newdatetime = date("Y-m-d H:i:s",$newtimestamp);
        $query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
        $res = mysql_query($query,$link);
        $query = "select id from vtiger_crmentity_seq";
        $res = mysql_query($query,$link);
        $row = mysql_fetch_row($res);
        $crm_id = $row[0];
        $query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($crm_id,'$userid','$userid','CallBackManager',NULL,'$userid',now(),now())";
        $res = mysql_query($query,$link);
        $query = "insert into vtiger_callbackmanager(callbackmanagerid,callsubject,customer,callbackdatetime,dialerrecord,callstatus,callbackrecord,campaignname) values($crm_id,'$subject','$customer','$newdatetime','','ScheduledToCallBack','','$campaign')";
        $res = mysql_query($query,$link);
        $query = "insert into vtiger_callbackmanagercf(callbackmanagerid) values($crm_id)";
        $res = mysql_query($query,$link);
        $query = "UPDATE vtiger_crmentity SET label='$subject' WHERE crmid='$crm_id'";
        $res = mysql_query($query,$link);

        $query = "INSERT INTO vtiger_crmentityrel(crmid, module, relcrmid, relmodule) VALUES('$customer','Contacts',$crm_id,'CallBackManager')";
        $res = mysql_query($query,$link);

        $query = "update vtiger_callbackmanager set callbackrecord='$crm_id' where callbackmanagerid='$callbackmanagerid'";
        $res = mysql_query($query,$link);
}

$cquery = "select a.callbackmanagerid,a.dialerrecord,a.customer,a.callstatus,a.campaignname,a.callbackdatetime,b.smcreatorid from vtiger_callbackmanager as a inner join vtiger_crmentity as b on a.callbackmanagerid=b.crmid and b.deleted='0' and a.callstatus='ScheduledForDialer'";

$cresult = mysql_query($cquery, $link);

while($crow = mysql_fetch_assoc($cresult)) {

	$dialermanagerid = $crow["dialerrecord"];	
	$callbackmanagerid = $crow["callbackmanagerid"];	
	$query = "select status from vtiger_dialermanager where dialermanagerid = '$dialermanagerid' and status != 'ScheduledForDialer' and status != 'SentToDialer' limit 1";
	$result = mysql_query($query, $link);
	if (!mysql_num_rows($result)) {
		continue;
	}	
        $row = mysql_fetch_assoc($result);
        $dialer_status = $row["status"];
	$query = "update vtiger_callbackmanager set callstatus='$dialer_status' where callbackmanagerid='$callbackmanagerid'";
        $res = mysql_query($query,$link);


}
?>
