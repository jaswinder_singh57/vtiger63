<?php

include_once 'modules/Vtiger/CRMEntity.php';

class PatientNotes extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_patientnotes';
        var $table_index= 'patientnotesid';

        var $customFieldTable = Array('vtiger_patientnotescf', 'patientnotesid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_patientnotes', 'vtiger_patientnotescf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_patientnotes' => 'patientnotesid',
                'vtiger_patientnotescf'=>'patientnotesid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'UserTaskKey' => Array('patientnotes', 'usertaskkey'),
                'Subject' => Array('patientnotes', 'subject'),
                'Customer' => Array('patientnotes', 'customer'),
                'Description' => Array('patientnotes', 'notedescription'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'UserTaskKey' => 'usertaskkey',
                'Subject' => 'subject',
                'Customer' => 'customer',
                'Description' => 'notedescription',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'usertaskkey';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'UserTaskKey' => 'usertaskkey',
                'Subject' => 'subject',
                'Customer' => 'customer',
                'Description' => 'notedescription',
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'UserTaskKey' => 'usertaskkey',
                'Subject' => 'subject',
                'Customer' => 'customer',
                'Description' => 'notedescription',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('usertaskkey');

        // For Alphabetical search
        var $def_basicsearch_col = 'usertaskkey';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'usertaskkey';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('usertaskkey','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
