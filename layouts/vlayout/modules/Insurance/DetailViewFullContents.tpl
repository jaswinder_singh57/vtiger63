{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
*
 ********************************************************************************/
-->*}
{strip}
{if $SHOW_CDR}
<div class="modal-container"></div>
<script type="text/javascript">
                var myfunction = function(url) {
                        $('.modal-container').load(url,function(result){
                                $('#myModal').modal({

                                });
                        });
                };
                var callfrom = "{$CALLFROM}";
                var uniqueid = "{$UNIQUEID}";
                var exten = "{$EXTEN}";
                var recordid = "{$RECORDID}";
                var create_at = "{$CREATE_AT}";
                var user_id = "{$USER_ID}";
                var record_found = "{$RECORD_FOUND}";
                var url = "modalbox.php?callfrom="+callfrom+"&uniqueid="+uniqueid+"&exten="+exten+"&crmid="+recordid+"&record_found="+record_found+"&user_id="+user_id+"&create_at="+create_at;
                var res = encodeURI(url);
                myfunction(res);

</script>
{/if}

	{include file='DetailViewBlockView.tpl'|@vtemplate_path:$MODULE_NAME RECORD_STRUCTURE=$RECORD_STRUCTURE MODULE_NAME=$MODULE_NAME}

{/strip}
