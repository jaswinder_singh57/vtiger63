function SendCallBackToQueueDetail(modulename,userid,recordid) {
        // Compute selected ids, excluded ids values, along with cvid value and pass as url parameters
        exportActionUrl = 'call_back.php?module='+modulename+'&view=detail';
        exportActionUrl += '&userid='+userid+'&selected_ids='+recordid;

        var w = 600;
        var h = 300;
        var leftOffset = (screen.width/2) - w/2;
        var topOffset = (screen.height/2) - h/2;

        window.open(exportActionUrl,"SendCallBackToQueue",'left=' + leftOffset + ',top=' + topOffset + ',width=' + w + ',height=' + h + ',titlebar=no,toolbar=no,location=no,resizable,scrollbars=yes');
}

