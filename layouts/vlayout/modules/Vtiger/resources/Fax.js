function SendFaxToQueue(modulename,userid) {
        var listInstance = Vtiger_List_Js.getInstance();
        // Compute selected ids, excluded ids values, along with cvid value and pass as url parameters
	var val=document.getElementById('query').value;
        var selectedIds = listInstance.readSelectedIds(true);
        var excludedIds = listInstance.readExcludedIds(true);
        var cvId = listInstance.getCurrentCvId();
        var pageNumber = jQuery('#pageNumber').val();
	exportActionUrl = 'fax_queue.php?module='+modulename+'&view=list';
        exportActionUrl += '&userid='+userid+'&selected_ids='+selectedIds+'&excluded_ids='+excludedIds+'&viewname='+cvId+'&page='+pageNumber+'&val='+val;

        var listViewInstance = Vtiger_List_Js.getInstance();
        var searchValue = listViewInstance.getAlphabetSearchValue();

        if((typeof searchValue != "undefined") && (searchValue.length > 0)) {
            exportActionUrl += '&search_key='+listViewInstance.getAlphabetSearchField()+'&search_value='+searchValue+'&operator=s';
        }
        exportActionUrl += '&search_params='+JSON.stringify(listInstance.getListSearchParams());
	var w = 600;
	var h = 300;
	var leftOffset = (screen.width/2) - w/2;
	var topOffset = (screen.height/2) - h/2;

	window.open(exportActionUrl,"SendFaxToQueue",'left=' + leftOffset + ',top=' + topOffset + ',width=' + w + ',height=' + h + ',titlebar=no,toolbar=no,location=no,resizable,scrollbars=yes');
}
function SendFaxToQueueDetail(modulename,userid,recordid) {
        // Compute selected ids, excluded ids values, along with cvid value and pass as url parameters
	alert ("hello"+recordid);
        exportActionUrl = 'fax_queue.php?module='+modulename+'&view=detail';
        exportActionUrl += '&userid='+userid+'&selected_ids='+recordid;

        var w = 600;
        var h = 300;
        var leftOffset = (screen.width/2) - w/2;
        var topOffset = (screen.height/2) - h/2;

        window.open(exportActionUrl,"SendFaxToQueue",'left=' + leftOffset + ',top=' + topOffset + ',width=' + w + ',height=' + h + ',titlebar=no,toolbar=no,location=no,resizable,scrollbars=yes');
}

function ReSendFaxToQueue(modulename,userid) {
        var listInstance = Vtiger_List_Js.getInstance();
        // Compute selected ids, excluded ids values, along with cvid value and pass as url parameters
        var val=document.getElementById('query').value;
        var selectedIds = listInstance.readSelectedIds(true);
        var excludedIds = listInstance.readExcludedIds(true);
        var cvId = listInstance.getCurrentCvId();
        var pageNumber = jQuery('#pageNumber').val();
        exportActionUrl = 'fax_resend.php?module='+modulename+'&view=list';
        exportActionUrl += '&userid='+userid+'&selected_ids='+selectedIds+'&excluded_ids='+excludedIds+'&viewname='+cvId+'&page='+pageNumber+'&val='+val;

        var listViewInstance = Vtiger_List_Js.getInstance();
        var searchValue = listViewInstance.getAlphabetSearchValue();

        if((typeof searchValue != "undefined") && (searchValue.length > 0)) {
            exportActionUrl += '&search_key='+listViewInstance.getAlphabetSearchField()+'&search_value='+searchValue+'&operator=s';
        }
        exportActionUrl += '&search_params='+JSON.stringify(listInstance.getListSearchParams());
        var w = 600;
        var h = 300;
        var leftOffset = (screen.width/2) - w/2;
        var topOffset = (screen.height/2) - h/2;

        window.open(exportActionUrl,"ReSendFaxToQueue",'left=' + leftOffset + ',top=' + topOffset + ',width=' + w + ',height=' + h + ',titlebar=no,toolbar=no,location=no,resizable,scrollbars=yes');
}

function sendreplysms(modulename,userid,recordid) {
        // Compute selected ids, excluded ids values, along with cvid value and pass as url parameters
	var xmlhttp=new XMLHttpRequest();
	
	xmlhttp.onreadystatechange=function() {
    		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var foundmodule =xmlhttp.responseText;
			//var url = "index.php?module="+foundmodule+"&view=MassActionAjax&mode=showSendSMSForm";
			var url = "index.php?module=Contacts&view=MassActionAjax&mode=showSendSMSForm";
			Vtiger_Detail_Js.triggerSendSms("index.php?module=Contacts&view=MassActionAjax&mode=showSendSMSForm","SMSNotifier");	
    		}
  	}
	
	xmlhttp.open("GET","getsmsmodule.php?id="+recordid,true);
  	xmlhttp.send();
}

