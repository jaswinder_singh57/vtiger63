function previewfax(modulename,userid,recordid) {
        // Compute selected ids, excluded ids values, along with cvid value and pass as url parameters
        exportActionUrl = 'inboundfaxpreview.php?module='+modulename+'&view=detail';
        exportActionUrl += '&userid='+userid+'&selected_ids='+recordid;

        var w = 800;
        var h = 400;
        var leftOffset = (screen.width/2) - w/2;
        var topOffset = (screen.height/2) - h/2;

        window.open(exportActionUrl,"PreviewFax",'left=' + leftOffset + ',top=' + topOffset + ',width=' + w + ',height=' + h + ',titlebar=no,toolbar=no,location=no,resizable,scrollbars=yes');
}

