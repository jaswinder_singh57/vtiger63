{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is:  vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<div class="dashboardWidgetHeader">
{foreach key=index item=cssModel from=$STYLES}
        <link rel="{$cssModel->getRel()}" href="{$cssModel->getHref()}" type="{$cssModel->getType()}" media="{$cssModel->getMedia()}" />
{/foreach}
{foreach key=index item=jsModel from=$SCRIPTS}
        <script type="{$jsModel->getType()}" src="{$jsModel->getSrc()}"></script>
{/foreach}

<table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
                <tr>
                        <td class="span5">
                                <div class="dashboardTitle textOverflowEllipsis" title="{vtranslate($WIDGET->getTitle(), $MODULE_NAME)}" style="width: 15em;"><b>&nbsp;&nbsp;{vtranslate($WIDGET->getTitle(), $MODULE_NAME)}</b></div>
                        </td>
                        <td class="refresh span2" align="right">
                                <span style="position:relative;">&nbsp;</span>
                        </td>
                        <td class="widgeticons span5" align="right">
                                <div class="box pull-right">
{if !$WIDGET->isDefault()}
        <a name="dclose" class="widget" data-url="{$WIDGET->getDeleteUrl()}">
                <i class="icon-remove" hspace="2" border="0" align="absmiddle" title="{vtranslate('LBL_REMOVE')}" alt="{vtranslate('LBL_REMOVE')}"></i>
        </a>
{/if}

                                </div>
                        </td>
                </tr>
        </tbody>
</table>

</div>
<div class="dashboardWidgetContent">
	{assign var="SPANSIZE" value=12/4}
	<div class="row-fluid" style="padding:5px">
                <div class="span{$SPANSIZE}"><strong>Title</strong></div>
                <div class="span6"><strong>Message</strong></div>
                <div class="span{$SPANSIZE}"><strong>AckStatus</strong></div>
        </div>
    {foreach key=INDEX item=LINE_ITEM_DETAIL from=$DATA}
        <div class="row-fluid" style="padding:5px">
                <div class="span{$SPANSIZE}"><a href="{$LINE_ITEM_DETAIL['entitylink']}">{$LINE_ITEM_DETAIL["annoucementtitle"]}</a></div>
                <div class="span6">{$LINE_ITEM_DETAIL["description"]}</div>
                <div class="span{$SPANSIZE}">{$LINE_ITEM_DETAIL["ackstatus"]}</div>
        </div>
 {/foreach}		
	<div class="row-fluid" style="padding:5px">
                <div class="span{$SPANSIZE}"><a href="{$LISTLINK}">More >> </a></div>
        </div>

</div>
