<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source in cooperation with a-g-c (Andreas Goebel)
 * The Initial Developer of the Original Code is vtiger and a-g-c (Andreas Goebel)
 * Portions created by vtiger are Copyright (C) vtiger, portions created by a-g-c are Copyright (C) a-g-c.
 * www.a-g-c.de
 * All Rights Reserved.
 ************************************************************************************/
$languageStrings = Array (
    'SQLReports' => 'SQL Reports',
    'LBL_SQLREPORTS_INFORMATION' => 'SQL Report Information',
    'LBL_SQL_CODE' => 'SQL Code',
    'Assigned To' => 'Assigned To',
    'Created Time' => 'Created Time',
    'Modified Time' => 'Modified Time',
    'reportnum' => 'Report ID',
    'reportname' => 'Report Name',
    'reportsql' => 'Report SQL Code',
    'LBL_RUN_REPORT' => 'Run Report',
    'LBL_BACK_TO_REPORTS' => 'Return to Detail View',
    'LBL_EXPORTPDF_BUTTON' => 'Create PDF',
    'LBL_EXPORTXL_BUTTON' => 'Export to Excel',
    'LBL_PRINT_REPORT' => 'Print Report',
    'SINGLE_SQLReports' => 'SQL Report',
    'Sqlreports Information' => 'SQL Report Information',
    'LBL_REPORT_GENERATION_FAILED' => 'Error during report execution',
    'LBL_NO_OF_RECORDS' => 'Number of records',
    'LBL_CUSTOMIZE' => 'Edit Report',
    'LBL_RECORDS' => 'Records',
    'LBL_RECORDS_SINGLE' => 'Record',
    'LBL_REPORT_EXPORT_EXCEL' => 'Excel Export',
    'LBL_REPORT_EXPORT_PDF' => 'PDF Export',
    'LBL_REPORT_CSV' => 'CSV Export',
    'LBL_REPORT_PRINT' => 'Print View',    

);

?>
