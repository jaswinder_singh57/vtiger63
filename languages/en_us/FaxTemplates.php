<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_ADD_RECORD' => 'Add Fax Template',
	'SINGLE_FaxTemplates' => 'Fax Template',
	'LBL_FAX_TEMPLATES'=> 'Fax Templates',
	'LBL_FAX_TEMPLATE' => 'Fax Template',
	
	'LBL_TEMPLATE_NAME' => 'Template name',
	'LBL_DESCRIPTION' => 'Description',
	'LBL_SUBJECT' => 'Subject',
	'LBL_GENERAL_FIELDS' => 'General Fields',
	'LBL_SELECT_FIELD_TYPE' => 'Select Field Type',
	
	'LBL_FAX_TEMPLATE_DESCRIPTION'=>'Manage templates for E-Mail module',
	
);
