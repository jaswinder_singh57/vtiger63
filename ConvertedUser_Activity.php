<?php

function asterisk_extension_modify($extension,$first_name,$last_name,$pbxserver,$pbxserver_user,$pbxserver_script_command) {

	global $today;
	global $scriptname;
	global $DEBUG;

	$name = $first_name.''.$last_name;
	$command = "ssh ".$pbxserver_user."@".$pbxserver." '$pbxserver_script_command $name $extension'";
	if ($DEBUG) { write_log("$today $scriptname :: Command For pbx $command");}
	echo $command."\n"; 
	$output = shell_exec("$command");
	write_log("$today $scriptname :: Command Output $output");
	return 0;
	

}

function email_server_adduser($emailname,$emaildomain,$email,$email_server,$emailserver_user,$emailserver_script_command) {
	global $today;
	global $scriptname;
	global $DEBUG;

	$command = "ssh ".$emailserver_user."@".$email_server." '$emailserver_script_command $emailname $emaildomain'";
	if ($DEBUG) { write_log("$today $scriptname :: Command For Email $command");}
	echo $command."\n";
	$output = shell_exec("$command");
	write_log("$today $scriptname :: Command Output $output");
	return 0;

}

function samba_server_adduser($user_name,$samba_server,$sambaserver_user,$sambaserver_script_command) {
        global $today;
        global $scriptname;
        global $DEBUG;

        $command = "ssh ".$sambaserver_user."@".$samba_server." '$sambaserver_script_command $user_name '";
        if ($DEBUG) { write_log("$today $scriptname :: Command For Email $command");}
        echo $command."\n";
        $output = shell_exec("$command");
        write_log("$today $scriptname :: Command Output $output");
        return 0;

}


function write_log ($log) {
	global $logfile;
	$fp = fopen($logfile,'a+');
	fwrite($fp,$log."\n");
	fclose($fp);
}

function checkprocess () {

        global $lock_file;
        $pid = file_get_contents($lock_file);
        if ($pid != '') {
                $running = posix_getpgid($pid);
                if ($running == $pid) {
                        echo "Process Running";
                        return 1;
                }
        }
        $pid = posix_getpid();
        file_put_contents($lock_file,$pid);
        return 0;
}


include("conn.php");
include("candidate_activity_config.php");

$scriptname = 'ConvertedUser_Activity';
error_reporting(E_ALL & ~E_WARNING);
echo "Checking Records\n";
$today = date('m-d-y');

$status = checkprocess();
if ($status) {
        echo "Exiting as previous not complete";
        exit;
}



$query = "select userid,employeeid from custom_converted_user where flag = '0'";
$result = mysql_query($query,$link);
$qstatus = mysql_affected_rows($link);
if ($qstatus < 0) {
	$failure = mysql_error();
	if ($DEBUG)  { write_log("$today $scriptname :: Query Failure $failure");}
	exit;
}

if (!mysql_num_rows($result)) {
	echo "Nothing To Do\n";
	exit;
}	


while ($row = mysql_fetch_assoc($result)) {

	$userid = $row['userid'];
	$employeeid = $row['employeeid'];

	$sql = "select id,first_name,last_name,email1,phone_crm_extension,user_name from vtiger_users where id='$userid' limit 1";
	$sresult = mysql_query($sql,$link);
	$qstatus = mysql_affected_rows($link);

	if ($qstatus < 0) {
	        $failure = mysql_error();
        	if ($DEBUG)  { write_log("$today Activity Log :: Query Failure $failure");}
		continue;
	}

	$srow = mysql_fetch_assoc($sresult);
	$first_name = $srow['first_name'];
	$last_name = $srow['last_name'];
	$email = $srow['email1'];
	$extension = $srow['phone_crm_extension'];
	$user_name = $srow['user_name'];

	$array = explode('@',$email);
	$emailname = $array[0];
	$domain_name = $array[1];
	
	$status = asterisk_extension_modify($extension,$first_name,$last_name,$pbxserver,$pbxserver_user,$pbxserver_script_command);

	$email_server = $emailserver_array["$domain_name"];
	$status = email_server_adduser($emailname,$domain_name,$email,$email_server,$emailserver_user,$emailserver_script_command);
	
	$samba_server = $sambaserver_array["$domain_name"];
	$status = samba_server_adduser($user_name,$samba_server,$sambaserver_user,$sambaserver_script_command);

	$sql = "update custom_converted_user set flag = '1' where userid='$userid'";
        $sresult = mysql_query($sql,$link);

}

echo "Finished\n";

?>
