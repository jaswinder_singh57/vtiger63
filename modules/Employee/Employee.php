<?php

include_once 'modules/Vtiger/CRMEntity.php';

class Employee extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_employee';
        var $table_index= 'employeeid';

        var $customFieldTable = Array('vtiger_employeecf', 'employeeid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_employee', 'vtiger_employeecf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_employee' => 'employeeid',
                'vtiger_employeecf'=>'employeeid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'EmployeeName' => Array('employee', 'employeename'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'EmployeeName' => 'employeename',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'payorkey';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'EmployeeName' => Array('employee', 'employeename'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'EmployeeName' => 'employeename',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('employeename');

        // For Alphabetical search
        var $def_basicsearch_col = 'employeename';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'employeename';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('employeename','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
