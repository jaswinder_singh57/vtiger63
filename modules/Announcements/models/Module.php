<?php

class Announcements_Module_Model extends Vtiger_Module_Model {

        /**
         * Function to get the Quick Links for the module
         * @param <Array> $linkParams
         * @return <Array> List of Vtiger_Link_Model instances
         */
        public function getSideBarLinks($linkParams) {
               
                $parentQuickLinks = parent::getSideBarLinks($linkParams);

                $quickLink = array(
                                'linktype' => 'SIDEBARLINK',
                                'linklabel' => 'LBL_DASHBOARD',
                                'linkurl' => $this->getDashBoardUrl(),
                                'linkicon' => '',
                );

                //Check profile permissions for Dashboards
                $moduleModel = Vtiger_Module_Model::getInstance('Dashboard');
                $userPrivilegesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
                $permission = $userPrivilegesModel->hasModulePermission($moduleModel->getId());
                if($permission) {
                        $parentQuickLinks['SIDEBARLINK'][] = Vtiger_Link_Model::getInstanceFromValues($quickLink);
                }

                return $parentQuickLinks;
        }

        public function getNewAnnouncements() {
                $db = PearDatabase::getInstance();
		require('widget_config.php');
                //TODO need to handle security
                $result = $db->pquery('SELECT * FROM vtiger_announcements
                                                INNER JOIN vtiger_crmentity ON vtiger_announcements.announcementsid = vtiger_crmentity.crmid
                                                 WHERE vtiger_crmentity.deleted="0" and vtiger_announcements.ackstatus = ?  Order by vtiger_crmentity.createdtime desc limit 5', array('New'));

                $data = array();
                for($i=0; $i<$db->num_rows($result); $i++) {
                        $row = $db->query_result_rowdata($result, $i);
			$linkid = $row['announcementsid'];
			$ilink = $crm_url."index.php?module=Announcements&view=Detail&record=$linkid";	
			$new = array(
				'annoucementtitle' => $row['annoucementtitle'],
				'description' => $row['description'],
				'ackstatus' => $row['ackstatus'],
				'entitylink' => $ilink 
			);
                        $data[] = $new;
                }


                return $data;
        }


}

?>
