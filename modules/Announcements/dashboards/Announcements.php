<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Announcements_Announcements_Dashboard extends Vtiger_IndexAjax_View {


	public function process(Vtiger_Request $request) {

			
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$userid = $currentUser->id;
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();

		$linkId = $request->get('linkid');
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$data = $moduleModel->getNewAnnouncements();
		$createdTime = $request->get('createdtime');
		
		//Date conversion from user to database format
		if(!empty($createdTime)) {
			$dates['start'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['start']);
			$dates['end'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['end']);
		}
		$widget = Vtiger_Widget_Model::getInstance($linkId, $currentUser->getId());
		//Include special script and css needed for this widget
		$listViewUrl = $moduleModel->getListViewUrl();
		//$listViewUrl = $crm_url.$listViewUrl;	
		$viewer->assign('WIDGET', $widget);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('DATA', $data);
		$viewer->assign('LISTLINK', $listViewUrl);
		$viewer->assign('CURRENTUSER', $currentUser);

		$accessibleUsers = $currentUser->getAccessibleUsersForModule($moduleName);
		$viewer->assign('ACCESSIBLE_USERS', $accessibleUsers);

		$viewer->view('dashboards/Announcements.tpl', $moduleName);
	}
}
