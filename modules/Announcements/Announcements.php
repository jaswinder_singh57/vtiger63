<?php

include_once 'modules/Vtiger/CRMEntity.php';

class Announcements extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_announcements';
        var $table_index= 'announcementsid';

        var $customFieldTable = Array('vtiger_announcementscf', 'announcementsid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_announcements', 'vtiger_announcementscf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_announcements' => 'announcementsid',
                'vtiger_announcementscf'=>'announcementsid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Title' => Array('announcements', 'annoucementtitle'),
                'Description' => Array('announcements', 'description'),
                'Status' => Array('announcements', 'ackstatus'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Title' => 'annoucementtitle',
                'Description' => 'description',
                'Status' => 'ackstatus',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'payorkey';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Title' => Array('announcements', 'annoucementtitle'),
                'Description' => Array('announcements', 'description'),
                'Status' => Array('announcements', 'ackstatus'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Title' => 'annoucementtitle',
                'Description' => 'description',
                'Status' => 'ackstatus',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('annoucementtitle','description','ackstatus');

        // For Alphabetical search
        var $def_basicsearch_col = 'annoucementtitle';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'annoucementtitle';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('annoucementtitle','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
