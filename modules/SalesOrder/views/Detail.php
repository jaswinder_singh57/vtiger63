<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class SalesOrder_Detail_View extends Inventory_Detail_View {

	        function showLineItemDetails(Vtiger_Request $request) {
                $record = $request->get('record');
                $moduleName = $request->getModule();

                $recordModel = Inventory_Record_Model::getInstanceById($record);
                $relatedProducts = $recordModel->getProducts();

                //##Final details convertion started
                $finalDetails = $relatedProducts[1]['final_details'];

                //Final tax details convertion started
                $taxtype = $finalDetails['taxtype'];
                if ($taxtype == 'group') {
                        $taxDetails = $finalDetails['taxes'];
                        $taxCount = count($taxDetails);
                        for($i=0; $i<$taxCount; $i++) {
                                $taxDetails[$i]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($taxDetails[$i]['amount'], null, true);
                        }
                        $finalDetails['taxes'] = $taxDetails;
                }
                //Final tax details convertion ended


                //Final shipping tax details convertion started
                $shippingTaxDetails = $finalDetails['sh_taxes'];
                $taxCount = count($shippingTaxDetails);
                for($i=0; $i<$taxCount; $i++) {
                        $shippingTaxDetails[$i]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($shippingTaxDetails[$i]['amount'], null, true);
                }
                $finalDetails['sh_taxes'] = $shippingTaxDetails;
                //Final shipping tax details convertion ended

                $currencyFieldsList = array('adjustment', 'grandTotal', 'hdnSubTotal', 'preTaxTotal', 'tax_totalamount',
                                                                        'shtax_totalamount', 'discountTotal_final', 'discount_amount_final', 'shipping_handling_charge', 'totalAfterDiscount');
                foreach ($currencyFieldsList as $fieldName) {
                        $finalDetails[$fieldName] = Vtiger_Currency_UIType::transformDisplayValue($finalDetails[$fieldName], null, true);
                }

                $relatedProducts[1]['final_details'] = $finalDetails;
                //##Final details convertion ended

                //##Product details convertion started
		$db = PearDatabase::getInstance();
		echo "+++++++++++++++++++++++++++++++++++";


		$pid_array = array();
                $productsCount = count($relatedProducts);
                for ($i=1; $i<=$productsCount; $i++) {
                        $product = $relatedProducts[$i];

                        //Product tax details convertion started
                        if ($taxtype == 'individual') {
                                $taxDetails = $product['taxes'];
                                $taxCount = count($taxDetails);
                                for($j=0; $j<$taxCount; $j++) {
                                        $taxDetails[$j]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($taxDetails[$j]['amount'], null, true);
                                }
                                $product['taxes'] = $taxDetails;
                        }
                        //Product tax details convertion ended

                        $currencyFieldsList = array('taxTotal', 'netPrice', 'listPrice', 'unitPrice', 'productTotal',
                                                                                'discountTotal', 'discount_amount', 'totalAfterDiscount');
                        foreach ($currencyFieldsList as $fieldName) {
                                $product[$fieldName.$i] = Vtiger_Currency_UIType::transformDisplayValue($product[$fieldName.$i], null, true);
                        }

                        $relatedProducts[$i] = $product;
			$productid = $relatedProducts[$i]['hdnProductId'.$i];

	                $query = "select vendorname from vtiger_vendor where vendorid = (select vendor_id from vtiger_products where productid='$productid') limit 1" ;
                	$result = $db->pquery($query);
                	$num_rows = $db->num_rows($result);
                	if($db->num_rows($result) > 0) {
                                $row = $db->query_result_rowdata($result, '0');
				$relatedProducts[$i]['vendorname'] = $row['vendorname'];
                	}

                        $query = "select cf_1293 as itemid from vtiger_productcf where productid='$productid'" ;
                        $result = $db->pquery($query);
                        $num_rows = $db->num_rows($result);
                        if($db->num_rows($result) > 0) {
                                $row = $db->query_result_rowdata($result, '0');
                                $relatedProducts[$i]['itemid'] = $row['itemid'];
                        }
			
                        $query = "select allowamt,extallowamt from vtiger_inventoryproductrel where id='$record'" ;
                        $result = $db->pquery($query);
                        $num_rows = $db->num_rows($result);
                        if($db->num_rows($result) > 0) {
                                $row = $db->query_result_rowdata($result, '0');
                                $relatedProducts[$i]['allowamt'] = Vtiger_Currency_UIType::transformDisplayValue($row['allowamt'], null, true);
                                $relatedProducts[$i]['extallowamt'] = Vtiger_Currency_UIType::transformDisplayValue($row['extallowamt'], null, true);
                        }

                }
	print_r($relatedProducts);
                //##Product details convertion ended

                $viewer = $this->getViewer($request);
                $viewer->assign('RELATED_PRODUCTS', $relatedProducts);
                $viewer->assign('RECORD', $recordModel);
                $viewer->assign('MODULE_NAME',$moduleName);

                $viewer->view('LineItemsDetail.tpl', 'SalesOrder');
        }


}
