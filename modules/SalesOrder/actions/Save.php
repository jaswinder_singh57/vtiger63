<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
class SalesOrder_Save_Action extends Inventory_Save_Action {

        public function saveRecord($request) {

                $db = PearDatabase::getInstance();
		$t = $request->get('totalProductCount');
		$record = $request->get('record');
		$p_array = array();
//Gurpreet: may24
		for ($i=1;$i<=$t;$i++) {
			$pid = $request->get("hdnProductId$i");
		   	$query = "select allowamt,extallowamt from vtiger_inventoryproductrel where id='$record' and productid='$pid'" ;
                        $result = $db->pquery($query);
                        $num_rows = $db->num_rows($result);
                        if($db->num_rows($result) > 0) {
                        	$row = $db->query_result_rowdata($result, '0');
                                $p_array[$i]["allowamt"] = Vtiger_Currency_UIType::transformDisplayValue($row['allowamt'], null, true);
				$p_array[$i]["extallowamt"] = Vtiger_Currency_UIType::transformDisplayValue($row['extallowamt'], null, true);
				$p_array[$i]["pid"] = $pid;
			}	
		}
//end
                $recordModel = $this->getRecordModelFromRequest($request);
                $recordModel->save();
                if($request->get('relationOperation')) {
                        $parentModuleName = $request->get('sourceModule');
                        $parentModuleModel = Vtiger_Module_Model::getInstance($parentModuleName);
                        $parentRecordId = $request->get('sourceRecord');
                        $relatedModule = $recordModel->getModule();
                        $relatedRecordId = $recordModel->getId();

                        $relationModel = Vtiger_Relation_Model::getInstance($parentModuleModel, $relatedModule);
                        $relationModel->addRelation($parentRecordId, $relatedRecordId);
                }
        if($request->get('imgDeleted')) {
            $imageIds = $request->get('imageid');
            foreach($imageIds as $imageId) {
                $status = $recordModel->deleteImage($imageId);
            }
        }
//Gurpreet: may24
	foreach ($p_array as $p) {
		$allowamt = $p['allowamt'];	
		$extallowamt = $p['extallowamt'];	
		$pid = $p['pid'];	
		$query = "update vtiger_inventoryproductrel set allowamt='$allowamt',extallowamt='$extallowamt' where id='$record' and productid='$pid'";	
                $result = $db->pquery($query);
	}
//end
                return $recordModel;
        }


}
