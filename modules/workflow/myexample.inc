<?php
/*
  workflows invoke custom function example method function file (myexample.inc)

  put this file in the modules/workflow/ directory. if the directory does
   not exist, then just create it.
  
  see the myexample_register.php file for more information.
*/

function myexample($entity){

//$entity is global object. uncomment the code below to see everything it contains.
// ex: after adding a new contact, you can retrieve the contact number id with:
//     $contact_no = $entity->data&#91;contact_no&#93;;
//debug only - use this to display $entity
/*
  $entityarray = get_object_vars($entity);
  echo '<pre>';
  print_r($entityarray);
 echo '</pre>';
*/

  //this example just sends you an email. yes, i know that feature is
  // already available, but this is just an example to show you how
  // to set it all up!
  mail('me@mydomain.com','test subject','test message','from: me@mydomain.com');
}
?>
