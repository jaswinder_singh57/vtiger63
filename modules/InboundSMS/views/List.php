<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class InboundSMS_List_View extends Vtiger_List_View {

        function process (Vtiger_Request $request) {
                $viewer = $this->getViewer ($request);
                $moduleName = $request->getModule();
                $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
                $this->viewName = $request->get('viewname');

	        $db = PearDatabase::getInstance();

                require('widget_config.php');

		$status_array = array();
		$query = "select a.crmid,b.$inboundsms_status_field from vtiger_crmentity as a inner join vtiger_inboundsmscf as b on a.crmid=b.inboundsmsid where a.deleted='0'  ";
                $result = $db->pquery($query);
                $num_rows = $db->num_rows($result);
                if($db->num_rows($result) > 0) {
                        $noOfRows = $db->num_rows($result);
                        for($i=0; $i<$noOfRows; ++$i) {
                                $row = $db->query_result_rowdata($result, $i);
                                $crmid = $row['crmid'];
                                $status = $row[$inboundsms_status_field];
                                $status_array[$crmid] = $status;
                        }
                }
		

                $this->initializeListViewContents($request, $viewer);
                $viewer->assign('VIEW', $request->get('view'));
                $viewer->assign('MODULE_MODEL', $moduleModel);
                $viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
                $viewer->assign('STATUS_ARRAY', $status_array);
                $viewer->view('ListViewContents.tpl', $moduleName);
        }

}
?>
