<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

class InboundSMS_Detail_View extends Vtiger_Detail_View {
        function showModuleSummaryView($request) {
                $recordId = $request->get('record');
                $moduleName = $request->getModule();

                if(!$this->record){
                        $this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
                }
                $recordModel = $this->record->getRecord();
                $recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_SUMMARY);

	        $moduleModel = $recordModel->getModule();
                $viewer = $this->getViewer($request);
                $viewer->assign('RECORD', $recordModel);
        	$viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
                $viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());

                $viewer->assign('MODULE_NAME', $moduleName);
                $viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
                $viewer->assign('SUMMARY_RECORD_STRUCTURE', $recordStrucure->getStructure());
                $viewer->assign('RELATED_ACTIVITIES', $this->getActivities($request));

		$final_array = array();
		
                $db = PearDatabase::getInstance();

                $query = "select c.first_name,c.last_name,a.createdtime,a.crmid,b.message from vtiger_crmentity as a inner join vtiger_inboundsms as b on a.crmid=b.inboundsmsid inner join vtiger_users as c on a.smownerid=c.id where a.deleted='0' and customer in (select customer from vtiger_inboundsms  where inboundsmsid='$recordId') order by createdtime;";
                $result = $db->pquery($query,$selectedIds);
                $num_rows = $db->num_rows($result);
		$j = 0;
                if($db->num_rows($result) > 0) {
                        $noOfRows = $db->num_rows($result);
                        $matchingRecords = array();
                        for($i=0; $i<$noOfRows; ++$i) {
                                $row = $db->query_result_rowdata($result, $i);
				$crmid = $row['crmid'];
				$firstname = $row['first_name'];
				$lastname = $row['last_name'];
				$createdtime = $row['createdtime'];
                                $message = $row['message'];
				$timestamp = strtotime($createdtime);
				$final_array[$j] = array('name'=>$firstname." ".$lastname,'createdtime'=>$createdtime,'crmid'=>$crmid,'message'=>$message,'type'=>'Inbound','timestamp'=>$timestamp);
				$j++;	
                        }
                }


                $query = "select c.first_name,c.last_name,a.createdtime,a.crmid,b.message  from vtiger_crmentity as a inner join vtiger_smsnotifier as b on a.crmid=b.smsnotifierid inner join vtiger_users as c on a.smownerid=c.id where a.deleted='0' and  b.smsnotifierid in (select crmid from vtiger_crmentityrel where relcrmid=(select customer from vtiger_inboundsms where inboundsmsid='$recordId')) order by createdtime";
                $result = $db->pquery($query,$selectedIds);
                $num_rows = $db->num_rows($result);
                if($db->num_rows($result) > 0) {
                        $noOfRows = $db->num_rows($result);
                        $matchingRecords = array();
                        for($i=0; $i<$noOfRows; ++$i) {
                                $row = $db->query_result_rowdata($result, $i);
                                $crmid = $row['crmid'];
                                $firstname = $row['first_name'];
                                $lastname = $row['last_name'];
                                $createdtime = $row['createdtime'];
                                $message = $row['message'];
                                $timestamp = strtotime($createdtime);
                                $final_array[$j] = array('name'=>$firstname." ".$lastname,'createdtime'=>$createdtime,'crmid'=>$crmid,'message'=>$message,'type'=>'Outbound','timestamp'=>$timestamp);
				$j++;	
                        }
                }

		foreach ($final_array as $key => $row) {
			$new_published[$key] = $row['timestamp'];
 		}	
		array_multisort($new_published, SORT_DESC,$final_array);








		$viewer->assign('HISTORY_FLAG', '1');
		$viewer->assign('HISTORY', $final_array);

                return $viewer->view('ModuleSummaryView.tpl', $moduleName, true);
        }




}
