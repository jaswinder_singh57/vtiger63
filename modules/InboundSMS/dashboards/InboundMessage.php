<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class InboundSMS_InboundMessage_Dashboard extends Vtiger_IndexAjax_View {


	public function process(Vtiger_Request $request) {

			
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$userid = $currentUser->id;
	
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();

		$linkId = $request->get('linkid');
		$data = $request->get('data');
		
		$createdTime = $request->get('createdtime');
		
		//Date conversion from user to database format
		if(!empty($createdTime)) {
			$dates['start'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['start']);
			$dates['end'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['end']);
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		
                $db = PearDatabase::getInstance();

		require('widget_config.php');

		$grouparray = array($userid,$inbounduser);
		$query = "SELECT  vtiger_users2group.groupid FROM vtiger_users INNER JOIN vtiger_users2group ON vtiger_users2group.userid = vtiger_users.id WHERE vtiger_users.id = '$userid'";
                $result = $db->pquery($query);
                $num_rows = $db->num_rows($result);
                if($db->num_rows($result) > 0) {
                        $noOfRows = $db->num_rows($result);
                        for($i=0; $i<$noOfRows; ++$i) {
                                $row = $db->query_result_rowdata($result, $i);
				$group_id = $row['groupid'];
				array_push($grouparray,$group_id);
			}
		}
		$group_str = implode(',',$grouparray);
	

                $query = "select a.crmid,b.message,b.customer,c.$inboundsms_status_field from vtiger_crmentity as a inner join vtiger_inboundsms as b on a.crmid=b.inboundsmsid inner join vtiger_inboundsmscf as c on a.crmid=c.inboundsmsid where a.deleted='0' and c.$inboundsms_status_field != 'Completed' and smownerid in ($group_str) order by modifiedtime desc  LIMIT 0,10";
                $result = $db->pquery($query);
                $num_rows = $db->num_rows($result);
                $j = 0;
                if($db->num_rows($result) > 0) {
                        $noOfRows = $db->num_rows($result);
                        $matchingRecords = array();
                        for($i=0; $i<$noOfRows; ++$i) {
                                $row = $db->query_result_rowdata($result, $i);
                                $crmid = $row['crmid'];
                                $message = $row['message'];
                                $customerid = $row['customer'];
                                $status = $row[$inboundsms_status_field];
				$link = '';
				$label = '';
				if ($customerid != 0 and $customerid != '') {
					$aquery = "select setype,label from vtiger_crmentity where crmid='$customerid'";			
                			$aresult = $db->pquery($aquery);
					$nrow = $db->query_result_rowdata($aresult, 0);
					$type = $nrow['setype'];
					$label = $nrow['label'];
					$link = $crm_url."index.php?module=$type&view=Detail&record=$customerid";
				}
				$ilink = $crm_url."index.php?module=InboundSMS&view=Detail&record=$crmid";
                                $final_array[$j] = array('crmid'=>$crmid,'message'=>$message,'customer'=>$customer,'status'=>$status,'clink'=>$link,'link'=>$ilink,'custname'=>$label);
                                $j++;
                        }
                }

			
		$data = $final_array;
		$widget = Vtiger_Widget_Model::getInstance($linkId, $currentUser->getId());

		//Include special script and css needed for this widget
		$listViewUrl = $moduleModel->getListViewUrl();
		$listViewUrl = $crm_url.$listViewUrl;	
		$viewer->assign('WIDGET', $widget);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('DATA', $data);
		$viewer->assign('LISTLINK', $listViewUrl);
		$viewer->assign('CURRENTUSER', $currentUser);

		$accessibleUsers = $currentUser->getAccessibleUsersForModule($moduleName);
		$viewer->assign('ACCESSIBLE_USERS', $accessibleUsers);

		$viewer->view('dashboards/InboundMessage.tpl', $moduleName);
	}
}
