<?php

include_once 'modules/Vtiger/CRMEntity.php';

class InboundSMS extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_inboundsms';
        var $table_index= 'inboundsmsid';

        var $customFieldTable = Array('vtiger_inboundsmscf', 'inboundsmsid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_inboundsms', 'vtiger_inboundsmscf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_inboundsms' => 'inboundsmsid',
                'vtiger_inboundsmscf'=>'inboundsmsid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'CUSTOMER' => Array('inboundsms', 'customer'),
                'SENDER' => Array('inboundsms', 'sender'),
                'SMS DATETIME' => Array('inboundsms', 'smsdatetime'),
                'MESSAGE' => Array('inboundsms', 'message'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'CUSTOMER' => 'customer',
                'SENDER' => 'sender',
                'SMS DATETIME' => 'smsdatetime',
                'MESSAGE' => 'message',
                'Assigned To' => 'assigned_user_id',
        );

        // Make the field link to detail view
        var $list_link_field = 'smsdatetime';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'CUSTOMER' => Array('inboundsms', 'customer'),
                'SENDER' => Array('inboundsms', 'sender'),
                'SMS DATETIME' => Array('inboundsms', 'smsdatetime'),
                'MESSAGE' => Array('inboundsms', 'message'),
                'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'CUSTOMER' => 'customer',
                'SENDER' => 'sender',
                'SMS DATETIME' => 'smsdatetime',
                'MESSAGE' => 'message',
                'Assigned To' => 'assigned_user_id',
        );

        // For Popup window record selection
        var $popup_fields = Array ('customer','sender','smsdatetime','message');

        // For Alphabetical search
        var $def_basicsearch_col = 'smsdatetime';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'smsdatetime';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('smsdatetime','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
