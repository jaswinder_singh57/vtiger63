<?php

class InboundSMS_Module_Model extends Vtiger_Module_Model {

        /**
         * Function to get the Quick Links for the module
         * @param <Array> $linkParams
         * @return <Array> List of Vtiger_Link_Model instances
         */
        public function getSideBarLinks($linkParams) {
               
                $parentQuickLinks = parent::getSideBarLinks($linkParams);

                $quickLink = array(
                                'linktype' => 'SIDEBARLINK',
                                'linklabel' => 'LBL_DASHBOARD',
                                'linkurl' => $this->getDashBoardUrl(),
                                'linkicon' => '',
                );

                //Check profile permissions for Dashboards
                $moduleModel = Vtiger_Module_Model::getInstance('Dashboard');
                $userPrivilegesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
                $permission = $userPrivilegesModel->hasModulePermission($moduleModel->getId());
                if($permission) {
                        $parentQuickLinks['SIDEBARLINK'][] = Vtiger_Link_Model::getInstanceFromValues($quickLink);
                }

                return $parentQuickLinks;
        }


}

?>
