<?php

include_once 'modules/Vtiger/CRMEntity.php';

class CallBackManager extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_callbackmanager';
        var $table_index= 'callbackmanagerid';

        var $customFieldTable = Array('vtiger_callbackmanagercf', 'callbackmanagerid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_callbackmanager', 'vtiger_callbackmanagercf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_callbackmanager' => 'callbackmanagerid',
                'vtiger_callbackmanagercf'=>'callbackmanagerid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Customer' => Array('callbackmanager', 'customer'),
                'CallBackDateTime' => Array('callbackmanager', 'callbackdatetime'),
                'Call Status' => Array('callbackmanager', 'callstatus'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Customer' => 'customer',
                'CallBackDateTime' => 'callbackdatetime',
                'Call Status' => 'callstatus',
                'Assigned To' => 'assigned_user_id',
        );

        // Make the field link to detail view
        var $list_link_field = 'customer';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Customer' => Array('callbackmanager', 'customer'),
                'CallBackDateTime' => Array('callbackmanager', 'callbackdatetime'),
                'Call Status' => Array('callbackmanager', 'callstatus'),
                'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Customer' => 'customer',
                'CallBackDateTime' => 'callbackdatetime',
                'Call Status' => 'callstatus',
                'Assigned To' => 'assigned_user_id',
        );

        // For Popup window record selection
        var $popup_fields = Array ('customer','status');

        // For Alphabetical search
        var $def_basicsearch_col = 'customer';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'customer';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('customer','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
