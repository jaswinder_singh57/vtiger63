<?php

include_once 'modules/Vtiger/CRMEntity.php';

class OutboundFax extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_outboundfax';
        var $table_index= 'outboundfaxid';

        var $customFieldTable = Array('vtiger_outboundfaxcf', 'outboundfaxid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_outboundfax', 'vtiger_outboundfaxcf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_outboundfax' => 'outboundfaxid',
                'vtiger_outboundfaxcf'=>'outboundfaxid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Title' => Array('outboundfax', 'faxtitle'),
                'Customer' => Array('outboundfax', 'customer'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'DataType' => Array('outboundfax', 'datatype'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Title' => 'faxtitle',
                'Customer' => 'customer',
                'CreatedTime' => 'CreatedTime',
                'DataType' => 'datatype',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'faxtitle';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Title' => Array('outboundfax', 'faxtitle'),
                'Customer' => Array('outboundfax', 'customer'),
                'FileLink' => Array('outboundfax', 'filelink'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Title' => 'faxtitle',
                'Customer' => 'customer',
                'FileLink' => 'filelink',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('faxtitle','customer');

        // For Alphabetical search
        var $def_basicsearch_col = 'faxtitle';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'faxtitle';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('faxtitle','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
