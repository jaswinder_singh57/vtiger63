<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class OutboundFax_ListView_Model extends Vtiger_ListView_Model {

	        public function getAdvancedLinks(){
                $moduleModel = $this->getModule();
                $createPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'EditView');
                $advancedLinks = array();
                $importPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'Import');
                if($importPermission && $createPermission) {
                        $advancedLinks[] = array(
                                                        'linktype' => 'LISTVIEW',
                                                        'linklabel' => 'LBL_IMPORT',
                                                        'linkurl' => $moduleModel->getImportUrl(),
                                                        'linkicon' => ''
                        );
                }

                $exportPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'Export');
                if($exportPermission) {
                        $advancedLinks[] = array(
                                        'linktype' => 'LISTVIEW',
                                        'linklabel' => 'LBL_EXPORT',
                                        'linkurl' => 'javascript:Vtiger_List_Js.triggerExportAction("'.$this->getModule()->getExportUrl().'")',
                                        'linkicon' => ''
                                );
                }

                $duplicatePermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'DuplicatesHandling');
                if($duplicatePermission) {
                        $advancedLinks[] = array(
                                'linktype' => 'LISTVIEWMASSACTION',
                                'linklabel' => 'LBL_FIND_DUPLICATES',
                                'linkurl' => 'Javascript:Vtiger_List_Js.showDuplicateSearchForm("index.php?module='.$moduleModel->getName().
                                                                '&view=MassActionAjax&mode=showDuplicatesSearchForm")',
                                'linkicon' => ''
                        );
                }
                $exportdailPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'SendFax');
                $moduleName = $this->getModule()->get('name');
                if($exportdailPermission) {
                        $advancedLinks[] = array(
                                        'linktype' => 'LISTVIEW',
                                        'linklabel' => 'ReSend Fax',
                                        'linkurl' => "javascript:ReSendFaxToQueue('".$moduleName."','".$_SESSION['AUTHUSERID']."');",
                                        'linkicon' => ''
                                );
                }



                return $advancedLinks;
        }



}
