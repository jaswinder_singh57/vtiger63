<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */
/**
 * Fax Template Model Class
 */
class Settings_FaxTemplates_Module_Model extends Settings_Vtiger_Module_Model {

	/**
	 * Function retruns List of Fax Templates
	 * @return string
	 */
	function getListViewUrl() {
		return 'module=FaxTemplates&parent=Settings&view=List';
	}

	/**
	 * Function returns all the Fax Template Models
	 * @return <Array of FaxTemplates_Record_Model>
	 */
	function getAll() {
		$db = PearDatabase::getInstance();
		$result = $db->pquery('SELECT * FROM vtiger_faxtemplates WHERE deleted = 0', array());

		$faxTemplateModels = array();
		for($i=0; $i<$db->num_rows($result); $i++) {
			$faxTemplateModel = Settings_FaxTemplates_Record_Model::getInstance();
			$faxTemplateModel->setData($db->query_result_rowdata($result, $i));
			$faxTemplateModels[] = $faxTemplateModel;
		}

		return $faxTemplateModels;
	}
}
