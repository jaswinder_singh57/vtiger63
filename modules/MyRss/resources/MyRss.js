jQuery.Class('MyRss_List_Js', {}, {

        registerEvents: function() {
                this.registerFeedClick();
                this.registerFeedAdd();
        },

        registerFeedClick: function() {
                jQuery('[data-feedurl]').click(function(e){
                        e.preventDefault();
                        var feedAnchor = jQuery(this);
                        var feedurl = feedAnchor.data('feedurl');
                        jQuery('#RssFeedTitle').text(feedAnchor.html() + ' - ' + feedurl);
                        var options = {
                                limit: 50
                        }
                        jQuery('#RssFeedContainer').empty().rss(feedurl, options);
                });
        },

        registerFeedAdd: function() {
                jQuery('#RssFeedAdd').click(function(e){
                        var formTplClone = jQuery('#RssFeedAddFormTpl').clone().css({display: 'block'});
                        app.showModalWindow(formTplClone, function(){
                                var targetForm = jQuery('.RssFeedAddForm', formTplClone);
                                targetForm.validationEngine(app.validationEngineOptions);
                                targetForm.submit(function(){
                                        if (!targetForm.validationEngine('validate')) {
                                                return;
                                        }
                                        var params = 'module=MyRss&action=Save&';
                                        params += jQuery(targetForm).serialize();
                                        AppConnector.request(params).then(function(response){
                                                window.location.reload();
                                                //app.hideModalWindow();
                                        });
                                });
                        });
                });
        }

});
