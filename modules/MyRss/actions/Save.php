<?php
class MyRss_Save_Action extends Vtiger_Action_Controller {
        public function checkPermission() {
                return true;
        }
        public function process(Vtiger_Request $request) {
                $feedurl = $request->get('url');
                if (empty($feedurl)) {
                        throw new Exception('URL cannot be empty');
                }

                $data = array('url' => $feedurl, 'title' => $feedurl);
                MyRss_Record_Model::create($data);

                $response = new Vtiger_Response();
                $response->setResult(array('feed' => $data));
                return $response;
        }
}
?>
