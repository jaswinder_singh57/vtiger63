<div class="listViewPageDiv">
        <div class="listViewTopMenuDiv">
                <div class="listViewActionsDiv">
                        <div id="RssFeedTitle">
                             Learn more about RSS
                             <a href="http://en.wikipedia.org/wiki/RSS" target="_blank">here</a>.
                        </div>
                </div>
        </div>
        <div id="RssFeedContainer" class="listViewEntriesDiv">
                &leftarrow; Selected feed content will be displayed here.
        </div>
</div>
<div id="RssFeedAddFormTpl" style="display: none;">
        <div class="modelContainer">
                <form method="GET" action="javascript:;" class="RssFeedAddForm">
                        <div class="modal-header">
                                <a class="close" data-dismiss="modal">x</a>
                                <h3>New RSS Feed</h3>
                        </div>
                        <div class="modal-body">
                                RSS Feed: <input type="text" name="url" class="validate[required]">
                        </div>
                        <div class="modal-footer">
                                <a class="cancelLink cancelLinkContainer pull-right" data-dismiss="modal">
                                 {vtranslate('LBL_CANCEL')}</a>
                                <button class="btn btn-success" type="submit">Add</button>
                        </div>
                </form>
        </div>
</div>

