<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Contacts_List_View extends Vtiger_List_View {

        function process (Vtiger_Request $request) {
                $viewer = $this->getViewer ($request);
                $moduleName = $request->getModule();
                $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
                $this->viewName = $request->get('viewname');

                $db = PearDatabase::getInstance();
		$user = Users_Record_Model::getCurrentUserModel();
                
		$status_array = array();
                $query = "Select * from cdr_queue where userid='".$user->id."' order by create_at desc";
                $result = $db->pquery($query);
                $num_rows = $db->num_rows($result);
		
		$noOfRows = 0;
                if($db->num_rows($result) > 0) {
			
                        $noOfRows = $db->num_rows($result);
			$i = 0;
                                $row = $db->query_result_rowdata($result, $i);
                                $uniqueid = $row['uniqueid'];
                                $callfrom = $row['callfrom'];
                                $exten = $row['exten'];
                                $create_at = $row['create_at'];
                }


                $this->initializeListViewContents($request, $viewer);
                $viewer->assign('VIEW', $request->get('view'));
                $viewer->assign('MODULE_MODEL', $moduleModel);
                $viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
		if ($noOfRows) {
			$viewer->assign('CDR_SHOW',1);
                        $viewer->assign('CALLFROM',$callfrom);
                        $viewer->assign('CREATE_AT',$create_at);
                        $viewer->assign('UNIQUEID',$uniqueid);
                        $viewer->assign('EXTEN',$exten);

		}
                $viewer->view('ListViewContents.tpl', $moduleName);
        }


}
?>
