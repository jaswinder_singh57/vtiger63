<?php

include_once 'modules/Vtiger/CRMEntity.php';

class DialerManager extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_dialermanager';
        var $table_index= 'dialermanagerid';

        var $customFieldTable = Array('vtiger_dialermanagercf', 'dialermanagerid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_dialermanager', 'vtiger_dialermanagercf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_dialermanager' => 'dialermanagerid',
                'vtiger_dialermanagercf'=>'dialermanagerid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Customer' => Array('dialermanager', 'customer'),
                'Campaign Selected' => Array('dialermanager', 'campaignid_selected'),
                'Status' => Array('dialermanager', 'status'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Customer' => 'customer',
                'Campaign Selected' => 'campaignid_selected',
                'Status' => 'status',
                'Assigned To' => 'assigned_user_id',
        );

        // Make the field link to detail view
        var $list_link_field = 'customer';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Customer' => Array('dialermanager', 'customer'),
                'Campaign Selected' => Array('dialermanager', 'campaignid_selected'),
                'Status' => Array('dialermanager', 'status'),
                'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Customer' => 'customer',
                'Campaign Selected' => 'campaignid_selected',
                'Status' => 'status',
                'Assigned To' => 'assigned_user_id',
        );

        // For Popup window record selection
        var $popup_fields = Array ('customer','campaignid_selected','status');

        // For Alphabetical search
        var $def_basicsearch_col = 'customer';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'customer';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('customer','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
