<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
include_once 'vtlib/Vtiger/Field.php';

/**
 * Vtiger Field Model Class
 */
class CDR_Field_model extends Vtiger_Field_Model {

        public static function getAdvancedFilterOpsByFieldType() {
                return array(
//Gurpreet: Jun 15
                        'V' => array('e','n','s','ew','c','k','y','ny','l','g'),
//end
                        'N' => array('e','n','l','g','m','h', 'y','ny'),
                        'T' => array('e','n','l','g','m','h','bw','b','a','y','ny'),
                        'I' => array('e','n','l','g','m','h','y','ny'),
                        'C' => array('e','n','y','ny'),
                        'D' => array('e','n','bw','b','a','y','ny'),
                        'DT' => array('e','n','bw','b','a','y','ny'),
                        'NN' => array('e','n','l','g','m','h','y','ny'),
                        'E' => array('e','n','s','ew','c','k','y','ny')
                );
        }

}
