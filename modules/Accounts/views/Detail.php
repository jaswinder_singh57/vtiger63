<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

class Accounts_Detail_View extends Vtiger_Detail_View {

	/**
	 * Function to get activities
	 * @param Vtiger_Request $request
	 * @return <List of activity models>
	 */
	public function getActivities(Vtiger_Request $request) {
		$moduleName = 'Calendar';
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if($currentUserPriviligesModel->hasModulePermission($moduleModel->getId())) {
			$moduleName = $request->getModule();
			$recordId = $request->get('record');

			$pageNumber = $request->get('page');
			if(empty ($pageNumber)) {
				$pageNumber = 1;
			}
			$pagingModel = new Vtiger_Paging_Model();
			$pagingModel->set('page', $pageNumber);
			$pagingModel->set('limit', 10);

			if(!$this->record) {
				$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
			}
			$recordModel = $this->record->getRecord();
			$moduleModel = $recordModel->getModule();

			$relatedActivities = $moduleModel->getCalendarActivities('', $pagingModel, 'all', $recordId);

			$viewer = $this->getViewer($request);
			$viewer->assign('RECORD', $recordModel);
			$viewer->assign('MODULE_NAME', $moduleName);
			$viewer->assign('PAGING_MODEL', $pagingModel);
			$viewer->assign('PAGE_NUMBER', $pageNumber);
			$viewer->assign('ACTIVITIES', $relatedActivities);

			return $viewer->view('RelatedActivities.tpl', $moduleName, true);
		}
	}

        function showModuleBasicView($request) {

                $recordId = $request->get('record');
                $moduleName = $request->getModule();

                if(!$this->record){
                        $this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
                }
                $recordModel = $this->record->getRecord();
                $detailViewLinkParams = array('MODULE'=>$moduleName,'RECORD'=>$recordId);
                $detailViewLinks = $this->record->getDetailViewLinks($detailViewLinkParams);

                $viewer = $this->getViewer($request);
                $viewer->assign('RECORD', $recordModel);
                $viewer->assign('MODULE_SUMMARY', $this->showModuleSummaryView($request));

                $viewer->assign('DETAILVIEW_LINKS', $detailViewLinks);
                $viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
                $viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
                $viewer->assign('MODULE_NAME', $moduleName);

                $recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_DETAIL);
                $structuredValues = $recordStrucure->getStructure();

                $moduleModel = $recordModel->getModule();

                $viewer->assign('RECORD_STRUCTURE', $structuredValues);
                $viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());

                $db = PearDatabase::getInstance();
                $user = Users_Record_Model::getCurrentUserModel();

                $status_array = array();
                $query = "Select * from cdr_queue where userid='".$user->id."' and (crmid ='$recordId' or crmid = '') order by create_at desc";
                $result = $db->pquery($query);
                $num_rows = $db->num_rows($result);

                $noOfRows = 0;
                if($db->num_rows($result) > 0) {

                        $noOfRows = $db->num_rows($result);
                        $i = 0;
                        $row = $db->query_result_rowdata($result, $i);
                        $uniqueid = $row['uniqueid'];
                        $callfrom = $row['callfrom'];
                        $exten = $row['exten'];
                        $crmid = $row['crmid'];
                        $create_at = $row['create_at'];
                        $user_id = $row['userid'];
                        $viewer->assign('SHOW_CDR', 1);
                        $viewer->assign('CALLFROM',$callfrom);
                        $viewer->assign('RECORDID',$recordId);
                        $viewer->assign('UNIQUEID',$uniqueid);
                        $viewer->assign('CREATE_AT',$create_at);
                        $viewer->assign('EXTEN',$exten);
                        $viewer->assign('USER_ID',$user_id);
			if ($crmid == $recordId) {
                        	$viewer->assign('RECORD_FOUND',1);
			} else {
                        	$viewer->assign('RECORD_FOUND',0);
			}
                }


                echo $viewer->view('DetailViewSummaryContents.tpl', $moduleName, true);
        }
        function showModuleDetailView(Vtiger_Request $request) {
                $recordId = $request->get('record');
                $moduleName = $request->getModule();

                if(!$this->record){
                $this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
                }
                $recordModel = $this->record->getRecord();
                $recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_DETAIL);
                $structuredValues = $recordStrucure->getStructure();

	        $moduleModel = $recordModel->getModule();

                $viewer = $this->getViewer($request);
                $viewer->assign('RECORD', $recordModel);
                $viewer->assign('RECORD_STRUCTURE', $structuredValues);
        	$viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
                $viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
                $viewer->assign('MODULE_NAME', $moduleName);
                $viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));


                $db = PearDatabase::getInstance();
                $user = Users_Record_Model::getCurrentUserModel();

                $status_array = array();
                $query = "Select * from cdr_queue where userid='".$user->id."' and (crmid ='$recordId' or crmid = '') order by create_at desc";
                $result = $db->pquery($query);
                $num_rows = $db->num_rows($result);

                $noOfRows = 0;
                if($db->num_rows($result) > 0) {

                        $noOfRows = $db->num_rows($result);
                        $i = 0;
                        $row = $db->query_result_rowdata($result, $i);
                        $uniqueid = $row['uniqueid'];
                        $callfrom = $row['callfrom'];
                        $exten = $row['exten'];
                        $crmid = $row['crmid'];
                        $create_at = $row['create_at'];
                        $user_id = $row['userid'];
                        $viewer->assign('SHOW_CDR', 1);
                        $viewer->assign('CALLFROM',$callfrom);
                        $viewer->assign('RECORDID',$recordId);
                        $viewer->assign('UNIQUEID',$uniqueid);
                        $viewer->assign('CREATE_AT',$create_at);
                        $viewer->assign('EXTEN',$exten);
                        $viewer->assign('USER_ID',$user_id);
                        if ($crmid == $recordId) {
                                $viewer->assign('RECORD_FOUND',1);
                        } else {
                                $viewer->assign('RECORD_FOUND',0);
                        }
                }

                return $viewer->view('DetailViewFullContents.tpl',$moduleName,true);
        }



}
