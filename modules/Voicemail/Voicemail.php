<?php

include_once 'modules/Vtiger/CRMEntity.php';

class Voicemail extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_voicemail';
        var $table_index= 'voicemailid';

        var $customFieldTable = Array('vtiger_voicemailcf', 'voicemailid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_voicemail', 'vtiger_voicemailcf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_voicemail' => 'voicemailid',
                'vtiger_voicemailcf'=>'voicemailid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Call Date' => Array('voicemail', 'vmdate'),
                'Status' => Array('voicemail', 'vmstatus'),
                'Customer' => Array('voicemail', 'customer'),
		'Recording' => Array('voicemail', 'recording'),
                'VM Box' => Array('voicemail', 'vmbox'),
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Call Date' => 'vmdate',
                'Status' => 'vmstatus',
                'Customer' => 'customer',
		'Recording' => 'recording',
                'VM Box' => 'vmbox',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // Make the field link to detail view
        var $list_link_field = 'vmdate';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Call Date' => 'vmdate',
                'Status' => 'vmstatus',
                'Customer' => 'customer',
                'VM Box' => 'vmbox',
                'CreatedTime' => Array('crmentity', 'createdtime'),
                'Assigned To' => Array('crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Call Date' => 'vmdate',
                'Status' => 'vmstatus',
                'Customer' => 'customer',
                'VM Box' => 'vmbox',
                'CreatedTime' => 'CreatedTime',
                'Assigned To' => 'assigned_user_id'
        );

        // For Popup window record selection
        var $popup_fields = Array ('vmdate');

        // For Alphabetical search
        var $def_basicsearch_col = 'vmdate';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'vmdate';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('vmdate','assigned_user_id');

        var $default_order_by = 'create_date';
        var $default_sort_order='ASC';
}
