<?php


function padit($old_count) {

	switch(strlen($old_count)) {
		case 1: 	
			$msg = "msg000$old_count";
			break;
		case 2: 	
			$msg = "msg00$old_count";
			break;
		case 3: 	
			$msg = "msg00$old_count";
			break;
		default: 	
			$msg = "msg$old_count";
			break;
	}
	
	return $msg;
}


function copy_files($file_txt,$old_path,$old_count) {
	global $DEBUG;
	
	$msg = padit($old_count);

        if (!file_exists($old_path)) {
		if ($DEBUG) { write_log("Path don't exists $old_path"); }
		shell_exec('mkdir -p $old_path');
        }

	
	$file_wav = str_replace (".txt",".wav",$file_txt);
	$file_WAV = str_replace (".txt",".WAV",$file_txt);
	$file_gsm = str_replace (".txt",".gsm",$file_txt);
	$old_txt = $old_path."".$msg.".txt";
	$old_wav = $old_path."".$msg.".wav";
	$old_WAV = $old_path."".$msg.".WAV";
	$old_gsm = $old_path."".$msg.".gsm";
	if (file_exists( $file_wav)){
		$status = shell_exec("cp -rf $file_wav $old_wav");
		write_log("cp -rf $file_wav $old_wav");	
	}
	if (file_exists( $file_WAV)){
		$status = shell_exec("cp -rf $file_WAV $old_WAV");
		write_log("cp -rf $file_WAV $old_WAV");	
	}
	if (file_exists( $file_gsm)){
		$status = shell_exec("cp -rf $file_gsm $old_gsm");
		write_log("cp -rf $file_gsm $old_gsm");	
	}
	if (file_exists( $file_txt)){
		$status = shell_exec("cp -rf $file_txt $old_txt");
		write_log("cp -rf $file_txt $old_txt");	
	}
	
}

function move_files($file_txt,$old_path,$old_count) {
        global $DEBUG;

        $msg = padit($old_count);

        $file_wav = str_replace (".txt",".wav",$file_txt);
        $file_WAV = str_replace (".txt",".WAV",$file_txt);
        $file_gsm = str_replace (".txt",".gsm",$file_txt);
        $old_txt = $old_path."".$msg.".txt";
        $old_wav = $old_path."".$msg.".wav";
        $old_WAV = $old_path."".$msg.".WAV";
        $old_gsm = $old_path."".$msg.".gsm";
        if (file_exists( $file_wav)){
                $status = shell_exec("mv -f $file_wav $old_wav");
                write_log("mv -f $file_wav $old_wav");
        }
        if (file_exists( $file_WAV)){
                $status = shell_exec("mv -f $file_WAV $old_WAV");
                write_log("mv -f $file_WAV $old_WAV");
        }
        if (file_exists( $file_gsm)){
                $status = shell_exec("mv -f $file_gsm $old_gsm");
                write_log("mv -f $file_gsm $old_gsm");
        }
        if (file_exists( $file_txt)){
                $status = shell_exec("mv -f $file_txt $old_txt");
                write_log(" Status for mv -f $file_txt $old_txt is $status");
        }
	return 0;	
}



function write_log ($log) {
        global $logfile;
	$now = date("Y-m-d h:i:s", time());
        $fp = fopen($logfile,'a+');
        fwrite($fp,$now." :: ".$log."\n");
        fclose($fp);
}


require_once('conn_db.php');
require_once('config_vm.php');

$management_array = array();
$query = "select users from userman_groups where groupname = '$management_group'";
if ($DEBUG) { write_log("$query"); }
$result = mysql_query($query,$link);
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)){
		$user_str = $row['users'];
		$user_str = str_replace( array("'", "[", "&quot;","]"), "", htmlspecialchars($user_str));	
		$uquery = "select work from userman_users where id in ($user_str)";
		if ($DEBUG) { write_log("$uquery"); }
		$uresult = mysql_query($uquery,$link);
		if(mysql_num_rows($uresult)){
			while($urow = mysql_fetch_assoc($uresult)){
				$username = $urow["username"];
				$management_array["$username"] = 1;
				if ($DEBUG) { write_log("managerment user = $username"); }
			}	
		}
	}	
}


$fw_path = $vmhome."/".$fw_context."/".$fw_box."/INBOX/";
$context_discard = array(
	'general' => 1,
	'zonemessages' => 1
);
$vm_lines = parse_ini_file($filename,true,true);

foreach ($vm_lines as $context => $val_array) {
	if (array_key_exists($context,$context_discard)) {	
		if ($DEBUG) { write_log("Skipping $context"); }
		continue;
	}
	foreach ($val_array as $vm_box => $vm_value){
		$vm_v = explode(',',$vm_value);
		$vm_name = $vm_v[1];
			if ($DEBUG) { write_log("CHeck Vm $vm_box"); }
		if (array_key_exists($vm_box,$management_array)){
			if ($DEBUG) { write_log("Skipping Management VM $vm_box"); }
			continue;
		}		
		$inbox_path = $vmhome."/".$context."/".$vm_box."/INBOX/";
		$old_path = $vmhome."/".$context."/".$vm_box."/Old/";
		if (!file_exists($inbox_path)) {
			if ($DEBUG) { write_log("Path don't exists $inbox_path"); }
			continue;
		}	
//		$count = trim(shell_exec("find $inbox_path -type f -name '*.txt' |grep -c msg"));
		
		$count = shell_exec("find $inbox_path -type f -name '*.txt' -daystart -mtime +$max_days|grep -c msg");		
		$count = trim($count);
		if ($DEBUG) { write_log("Files Found $count "); }
		if ($count == '0') {
			if ($DEBUG) { write_log("No message to process for User $vm_box in $context"); }
			continue;
					
		}
		if ($vm_box == $fw_box) {
			continue;
		}
		$messages = trim(shell_exec("find $inbox_path -type f -name '*.txt'|sort"));
		$mail_to_forward = explode(PHP_EOL, $messages);		

	        if (!file_exists($old_path)) {
        	        if ($DEBUG) { write_log("Path don't exists $old_path"); }
                	shell_exec("mkdir -p $old_path");
	        }

	
		$old_count = trim(shell_exec("find $old_path -type f -name '*.txt' |sort|grep -c msg"));
		$fw_inbox_count = trim(shell_exec("find $fw_path -type f -name '*.txt'|sort |grep -c msg"));
		foreach ($mail_to_forward as $file_txt) {
			$status = copy_files($file_txt,$fw_path,$fw_inbox_count);							
			$fw_inbox_count++;
			$status = move_files($file_txt,$old_path,$old_count);							
			$old_count++;
			$query = "insert into voicemail_forward(forward_time,extension,extension_name,forward_to) values (now(),'$vm_box','$vm_name','$fw_box')";
			if ($DEBUG) { write_log("$query"); }
			$result = mysql_query($query,$link);

		}	
		exit;		
		
	}
}
?>
