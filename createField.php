<?php
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$MODULENAME = 'CallBackManager';

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);


	
	$field2 = new Vtiger_Field();
	$field2->name = 'cbtoextension';
	$field2->label= 'CB To Extension';
	$field2->table = $module->basetable;
	$field2->column = 'cbtoextension';
	$field2->columntype = 'VARCHAR(1)';
	$field2->uitype = 56;
	$field2->typeofdata = 'V~O';
	$block->addField($field2);

