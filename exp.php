<?php
$title='coreBOS packaging tool';
// Turn on debugging level
$Vtiger_Utils_Log = false;
include_once('vtlib/Vtiger/Module.php');
global $current_user,$adb;
set_time_limit(0);
ini_set('memory_limit','1024M');
if (empty($_REQUEST['modulename'])) {
	echo '<br><br><b>Necessary Parameter {modulename} not present</b><br>';
} else {
	$modulename = vtlib_purify($_REQUEST['modulename']);
	Vtiger_Package::packageFromFilesystem($modulename,false,$dl);
	if ($dl) die();
	echo "<b>Package should be exported to the packages/optional directory of your install.</b><br>";
}
?>
