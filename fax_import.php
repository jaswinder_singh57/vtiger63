<?php



function write_log ($log) {
        global $logfile;
        $fp = fopen($logfile,'a+');
        fwrite($fp,$log."\n");
        fclose($fp);
}

function post_data ($endpointUrl,$params) {

        global $today;
        global $DEBUG;
        global $username;
        global $password;

        if ($DEBUG)  { write_log("$today  :: URL $endpointUrl");}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
//        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $retValue = curl_exec($ch);
        $status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
        if ($DEBUG)  { write_log("$today  :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);


}

function get_data ($endpointUrl) {

        global $today;
        global $DEBUG;
        global $username;
        global $password;

        if ($DEBUG)  { write_log("$today  :: URL $endpointUrl");}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$endpointUrl");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
//        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $retValue = curl_exec($ch);
        $status = 1;
        if(empty($retValue)) {
                $status = 0;
        }
        if ($DEBUG)  { write_log("$today  :: URLResponse $retValue");}
        curl_close($ch);
        return array($status,$retValue);


}






function check_accounts($sender) {

        global $link;
        global $DEBUG;
        global $today;

        $sql = "select columnname from vtiger_field where tabid='6' and uitype='11'";

        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql,$link);
        $qstatus = mysql_affected_rows($link);

        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}
        $array = array();

        while ($row = mysql_fetch_assoc($result)) {
                $columnname = $row['columnname'];
                $str = $columnname." = '".$sender."'";
                array_push($array,$str);

        }
        $where = implode(' or ',$array);

        $sql = "select crmid FROM  vtiger_crmentity LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountshipads ON vtiger_accountshipads.accountaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ($where)";
        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

        if (!mysql_num_rows($result)){
                return array(1,0);
        }
        $row = mysql_fetch_array($result);
        $crmid = '11x'.$row["crmid"];

        return array(0,$crmid);
}


function check_contacts($sender) {

        global $link;
        global $DEBUG;
        global $today;

	$sql = "select columnname from vtiger_field where tabid='4' and uitype='11'";

        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql,$link);
        $qstatus = mysql_affected_rows($link);

        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

	$array = array();
	 
	while ($row = mysql_fetch_assoc($result)) {
		$columnname = $row['columnname'];
		$str = $columnname." = '".$sender."'";
		array_push($array,$str);	

	}	
  
	$where = implode(' or ',$array);

        $sql = "select crmid FROM  vtiger_crmentity LEFT JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_crmentity.crmid LEFT JOIN vtiger_contactaddress ON vtiger_contactaddress.contactaddressid=vtiger_crmentity.crmid LEFT JOIN vtiger_contactsubdetails ON vtiger_contactsubdetails.contactsubscriptionid=vtiger_crmentity.crmid LEFT JOIN vtiger_contactscf ON vtiger_contactscf.contactid=vtiger_crmentity.crmid LEFT JOIN vtiger_customerdetails ON vtiger_customerdetails.customerid=vtiger_crmentity.crmid WHERE  vtiger_crmentity.deleted='0' and ($where)";

        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

        if (!mysql_num_rows($result)){
                return array(1,0);
        }
        $row = mysql_fetch_array($result);
        $crmid = '12x'.$row["crmid"];

        return array(0,$crmid);
}


function check_vendors($sender) {

        global $link;
        global $DEBUG;
        global $today;
        $sql = "select columnname from vtiger_field where tabid='18' and uitype='11'";

        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql,$link);
        $qstatus = mysql_affected_rows($link);

        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

        $array = array();

        while ($row = mysql_fetch_assoc($result)) {
                $columnname = $row['columnname'];
                $str = $columnname." = '".$sender."'";
                array_push($array,$str);

        }

        $where = implode(' or ',$array);

        $sql = "select crmid  FROM  vtiger_crmentity LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_crmentity.crmid LEFT JOIN vtiger_vendorcf ON vtiger_vendorcf.vendorid=vtiger_crmentity.crmid WHERE vtiger_crmentity.deleted='0' and ($where)";
        if ($DEBUG)  { write_log("$today $sender :: $sql  ");}
        $result = mysql_query($sql,$link);
        $qstatus = mysql_affected_rows($link);
        if ($DEBUG)  { write_log("$today $sender :: Query Executed Result $sql ");}

        if (!mysql_num_rows($result)){
                return array(1,0);
        }
        $row = mysql_fetch_array($result);
        $crmid = '2x'.$row["crmid"];

        return array(0,$crmid);
}

function import_inboundfax ($sender,$receiver,$patient,$doctor,$vendor,$faxstatus,$pages) {

        global $today;
        global $link;
        global $logfile;
        global $DEBUG;
        global $webapiurl;
        global $username;
        global $secretkey;

        //login to server
        $loginurl = $webapiurl."?operation=getchallenge&username=$username";
        list($status,$response) = get_data($loginurl);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }
        $challengeToken = $loginresponse['result']['token'];

        $generatedKey = md5($challengeToken.$secretkey);
        $params = array('operation'=>'login', 'username'=>$username,'accessKey'=>$generatedKey);

        list($status,$response) = post_data($webapiurl,$params);
        if ($status != 1 ){
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }
        $loginresponse = json_decode($response,true);
        if ($loginresponse["success"] != 1) {
                if ($DEBUG)  { write_log("$sender :: unable to login user $username  ");}
                return "UnableTOLogin";
        }

        $jsonResponse = json_decode($response,true);
        $sessionid = $jsonResponse['result']['sessionName'];
        $userid = $jsonResponse['result']['userId'];

	$title = "Fax-$sender-$receiver";
        $modulename = "InboundFax";
	$contactdata = array('faxtitle'=>$title,'faxstatus'=>$faxstatus,'caller'=>$sender,'called'=>$receiver,'patient'=>$patient,'doctor'=>$doctor,'vendor'=>$vendor,'pages'=>$pages,'assigned_user_id'=>$userid);

        $objectjson=json_encode($contactdata);
        $params = array("sessionName"=>$sessionid, "operation"=>'create', "element"=>$objectjson, "elementType"=>$modulename);
        list($status,$response) = post_data($webapiurl,$params);
        if (!$status){
                if ($DEBUG)  { write_log("$sender :: unable to import to InboundFax $response ");}
                return "UnableToImport";
        }

        $jsonresponse = json_decode($response,true);

        if ($jsonresponse["success"] == 1){
		$id = $jsonresponse["result"]["id"];
                if ($DEBUG)  { write_log("$sender :: Import Successfull to InboundFax  ");}
                return array("ImportSuccessfull",$id);
        } else {
                if ($DEBUG)  { write_log("$sender :: Import Unsuccessfull to InboundFax $response ");}
                return array("ImportUnsuccessfull","0");
        }

}




function upload_tif ($tif_file,$o_crmid,$userid) {


	global $tif_folder;
	global $link;
        
        require_once('vtlib/Vtiger/Functions.php');
        $location = Vtiger_Functions::initStorageFileDirectory();
                $timestamp = time();
                $filename = "faxfile-".$timestamp.".tif";
		$filesize = filesize($tif_file);	
		print_r($filesize);

		$query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
	        $result = mysql_query($query,$link);
                $query = "select id from vtiger_crmentity_seq";
                $result = mysql_query($query,$link);
                $row = mysql_fetch_row($result);
                $crmid = $row[0];
                $gmt_now = get_time();
                $query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($crmid,'$userid','$userid','Documents',NULL,'$userid','$gmt_now','$gmt_now')";
                $result = mysql_query($query,$link);
                $query = "select cur_id,prefix from vtiger_modentity_num where semodule='Documents' and active = 1";
                $result = mysql_query($query,$link);
                $row = mysql_fetch_row($result);
                $cur_id = $row[0];
                $prefix_cur_id = $row[1];
                $old_cur_id = $cur_id;
                $cur_id++;
                $query = "UPDATE vtiger_modentity_num SET cur_id='$cur_id' where cur_id='$old_cur_id' and active=1 AND semodule='Documents'";
                $result = mysql_query($query,$link);

		$query = "select folderid from vtiger_attachmentsfolder where foldername='$tif_folder'";
		$result = mysql_query($query,$link);
		if (mysql_num_rows($result)) {
			$row = mysql_fetch_row($result);
			$folderid = $row["0"];
		} else {
	                $folderid = '1';
		}
		$filetype= 'image/tiff';



                $query = "insert into vtiger_notes(notesid,title,filename,notecontent,filelocationtype,fileversion,filestatus,folderid,note_no,filesize,filetype) values($crmid,'InFaxDoc$crmid','$filename','Document For InFax','I','','1','$folderid','$prefix_cur_id$cur_id','$filesize','$filetype')";
		echo "$query\n";
                $result = mysql_query($query,$link);
                $query = "insert into vtiger_notescf(notesid) values($crmid)";
                $result = mysql_query($query,$link);

                $query = "update vtiger_crmentity_seq set id=LAST_INSERT_ID(id+1)";
                $result = mysql_query($query,$link);
                $query = "select id from vtiger_crmentity_seq";
                $result = mysql_query($query,$link);
                $row = mysql_fetch_row($result);
                $attachmentid = $row[0];
                $gmt_now = get_time();
                $query = "insert into vtiger_crmentity (crmid,smcreatorid,smownerid,setype,description,modifiedby,createdtime,modifiedtime) values($attachmentid,'$userid','$userid','Documents Attachment',NULL,'$userid','$gmt_now','$gmt_now')";
                $result = mysql_query($query,$link);

                $query = "insert into vtiger_attachments(attachmentsid, name, description, type, path) values($attachmentid, '$filename', NULL, '$filetype', '$location')";
                $result = mysql_query($query,$link);
                $query = "delete from vtiger_seattachmentsrel where crmid = $crmid";
                $result = mysql_query($query,$link);
                $query = "insert into vtiger_seattachmentsrel values($crmid,$attachmentid)";
                $result = mysql_query($query,$link);
		$target_file = $location."".$attachmentid."_".$filename;
		shell_exec("cp -rf $tif_file $target_file");
		$query = "update vtiger_inboundfax set filelink ='$crmid' where inboundfaxid='$o_crmid'";
		echo "$query";
		$result = mysql_query($query,$link);
		
}



function get_time() {
        $newDate = date("Y-m-d H:i:s", time());
        return ($newDate);
}

function get_userid($receiver) {
	
	global $receiver_array;
        global $link;
        global $DEBUG;
        global $today;

	print_r($receiver_array);
	echo "$receiver";
		
	if (array_key_exists($receiver, $receiver_array)) {
	     	$receiver_name = $receiver_array["$receiver"];
	} else {
		$receiver_name = $receiver_array["default"];
	}	

	$sql = "select groupid from vtiger_groups where groupname = '$receiver_name'";
        $result = mysql_query($sql,$link);
	if (mysql_num_rows($result)){
        	$row = mysql_fetch_row($result);	
		$userid = $row[0];
		return $userid;
	} 
        $sql = "select id from vtiger_users where user_name = '$receiver_name'";
        $result = mysql_query($sql,$link);
        if (mysql_num_rows($result)){
                $row = mysql_fetch_row($result);
                $userid = $row[0];
                return $userid;
        } 

}

$DB = 1;

require_once("conn.php");
require_once("faxinbound_config.php");
$today = get_time();
$csql = "select id,src,dst,uniqueid,filename,status,pages,calldate,flag from fax_received where flag='0' order by calldate desc limit 10;";
$nresult = mysql_query($csql, $alink);
while($nrow = mysql_fetch_array($nresult)) {

		
	$id = $nrow["id"];
	$sender = $nrow["src"];
	$dst = $nrow["dst"];
	$uniqueid = $nrow["uniqueid"];
	$filename = $nrow["filename"];
	$faxstatus = $nrow["status"];
	$pages = $nrow["pages"];
	$calldate = $nrow["calldate"];
	$filename_path = $file_path."/".$filename;
	$receiver = substr($dst,-10);
	$receiver_name = '';
	$userid = get_userid($receiver); 

	list($astatus,$doctor) = check_accounts($sender);
	list($pstatus,$patient) = check_contacts($sender);
	list($vstatus,$vendor) = check_vendors($sender);
	
	list($fstatus,$crmid) = import_inboundfax ($sender,$receiver,$patient,$doctor,$vendor,$faxstatus,$pages);	

	if (!$crmid) {
		echo "import failed";	
	}
	$temp = explode('x',$crmid);
	$crmid = $temp[1];
	if ($faxstatus == 'SUCCESS'){
		upload_tif($filename_path,$crmid,$userid);
	}	
	$sql = "update fax_received set flag='1' where id='$id'";
	$result = mysql_query($sql, $alink);
	
}


exit;

?> 
