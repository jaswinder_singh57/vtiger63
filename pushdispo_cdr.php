<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Prevent closing bootstrap modal window on click outside</title>
<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" ></script>
</head>
<body>
<form id="form1">
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
Show Modal
</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Prevent Model Close</h4>
</div>
<div class="modal-body">
Welcome to Aspdotnet-Suresh.com
Welcome to Aspdotnet-Suresh.com
Welcome to Aspdotnet-Suresh.com
Welcome to Aspdotnet-Suresh.com
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-primary">Save changes</button>
</div>
</div>
</div>
</div>
</form>
</body>
</html>
