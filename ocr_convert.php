<?php

function create_tif_file ($tif_file,$docfile_full) {

        $status = System("gs -q -dNOPAUSE -dBATCH -sDEVICE=tiffg4 -sPAPERSIZE=letter -sOutputFile=$tif_file $docfile_full");
        return $status;
}


function convert_file ($filelink) {


	global $link;
       	global $crm_home;
	global $tmp_folder;

	$query = "select attachmentsid,path,name,type from vtiger_attachments where attachmentsid = (select attachmentsid from vtiger_seattachmentsrel where crmid='$filelink')";
        $result = mysql_query($query,$link);
	if (!mysql_num_rows($result)){
		return array('2','File Not found');;
	}
	
        $row = mysql_fetch_row($result);
        $attachmentsid = $row[0];
        $path = $row[1];
        $filename = $row[2];
        $file_type = $row[3];
	$timestamp = time();
	$fullpath = $crm_home."/".$path."".$attachmentsid."_".$filename;
	
	$tmp_file = $tmp_folder."/".$attachmentsid."_".$timestamp;
	if ($file_type == 'application/pdf') {
		$tmp_file_tif = $tmp_folder."/".$attachmentsid."_".$timestamp.".tif";
		$status = create_tif_file($tmp_file_tif,$fullpath);
	} elseif ($file_type == 'image/tiff') {
		$tmp_file_tif = $fullpath;
	} else {
		return array('2','File Not PDF/tiff');
	}
	$result = shell_exec("/usr/bin/tesseract $tmp_file_tif $tmp_file");
	$content = file_get_contents($tmp_file.".txt");
	return array('1',$content);
	
}


$DB = 1;

require_once("conn.php");
require_once("ocr_config.php");

$csql = "select a.crmid,b.filelink from vtiger_crmentity as a inner join vtiger_inboundfax as b on a.crmid=b.inboundfaxid inner join vtiger_inboundfaxcf as c on a.crmid=c.inboundfaxid where a.deleted='0' and date(a.createdtime) > '$ocr_start_date' and (c.$ocr_flag_field= '' or c.$ocr_flag_field='NOTDONE') order by a.createdtime desc";
$nresult = mysql_query($csql, $link);
while($nrow = mysql_fetch_array($nresult)) {

		
	$crmid = $nrow["crmid"];
	$filelink = $nrow["filelink"];
	
	list($status,$content) = convert_file($filelink);

	if ($status == 1){
		$status = 'DONE';		
	} else if ($status == 2) {
		$status = 'FAILED';
	} else {
		$status = 'NOTDONE';
		$content = '';
	}		

	$query = "update vtiger_inboundfaxcf set $ocr_flag_field='$status',$ocr_data_field='$content' where inboundfaxid='$crmid'";	
	$sresult = mysql_query($query, $link);

	
}


exit;

?> 
